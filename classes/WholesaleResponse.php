<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate};
use GuzzleHttp\Client;

class WholesaleResponse {

    function __construct() {
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        if ( 'wholesale' !== $user->u_role ) {
            Response::instance()->sendMessage( 'You are not a wholesale.' );
        }
    }

    function orders() {
        $user = User::getUser( Auth::id() );
        $pauseStatus = $user->getMeta( 'pauseStatus' );
        $pause_prev = $user->getMeta( 'pause_prev' );
        $pause_latest = $user->getMeta( 'pause_latest' );

        $status = isset( $_GET['status'] ) ? $_GET['status'] : '';
        $page = isset( $_GET['page'] ) ? (int)$_GET['page'] : '';
        $per_page = 10;
        $limit    = $per_page * ( $page - 1 );

        $db = new DB;
        $db->add( 'SELECT tom.*, tm.m_name, tm.m_form, tm.m_strength, tm.m_unit, tr.o_ph_id, SUM(tom.m_qty) as total_qty FROM t_o_medicines tom INNER JOIN t_medicines tm ON tom.m_id = tm.m_id INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE 1=1' );
        $db->add( ' AND tom.s_id = ? AND tom.ws_available = ?', Auth::id(), 1 );

        if( \in_array( $status, [ 'feedback_requested', 'feedback_given'] ) ) {
            $db->add( ' AND tr.o_status IN (?,?)', 'processing', 'confirmed' );
            if ( 'feedback_requested' == $status ) {
                $db->add( ' AND tr.o_i_status = ? AND tom.s_price = ?', 'ws_fb', 0 );
                if( 'paused' == $pauseStatus ) {
                    $db->add( ' AND tr.o_created > ? AND tr.o_created <= ?', $pause_prev, $pause_latest );
                } else {
                    $db->add( ' AND tr.o_created > ? AND tr.o_created <= ?', $pause_latest, \date( 'Y-m-d H:i:s' ) );
                }
            } elseif ( 'feedback_given' == $status ) {
                $db->add( ' AND s_price != ? AND tr.o_created > ? AND tr.o_created <= ?', 0, $pause_prev, $pause_latest );
            }
        } elseif ( $status ) {
            $db->add( ' AND tr.o_status = ?', $status );
            $db->add( ' AND tr.o_i_status = ?', 'confirmed' );
        }
        if ( 'confirmed' == $status ) {
            //$db->add( ' AND tr.o_created > ? AND tr.o_created <= ?', $pause_prev, $pause_latest );
            $db->add( ' GROUP BY tr.o_ph_id, tom.m_id' );
        } else {
            $db->add( ' GROUP BY tom.m_id' );
        }

        $db->add( ' LIMIT ?, ?', $limit, $per_page );
        
        $query = $db->execute();
        $data = $query->fetchAll();
        if ( 'confirmed' == $status ) {
            $ph_ids = \array_column( $data, 'o_ph_id' );
            CacheUpdate::instance()->update_cache( $ph_ids, 'user' );

            foreach ( $data as &$om ) {
                $om['ph_name'] = User::getName( $om['o_ph_id'] );
            }
            unset( $om );
        }

        Response::instance()->setData( $data );

        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Orders Found' );
        } else {
            Response::instance()->setResponse( 'pauseStatus', $pauseStatus );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function notAvailable( $o_id, $m_id ){
        if ( !$m_id ) {
            Response::instance()->sendMessage( 'No medicines found.' );
        }
        $user = User::getUser( Auth::id() );
        $pause_prev = $user->getMeta( 'pause_prev' );
        $pause_latest = $user->getMeta( 'pause_latest' );

        $query = DB::db()->prepare( 'UPDATE t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id SET tom.s_price = ?, tom.ws_available = ? WHERE tom.s_id = ? AND tom.m_id = ? AND tom.ws_available = ? AND tr.o_status IN (?,?) AND tr.o_i_status = ? AND tr.o_created > ? AND tr.o_created <= ?' );
        $query->execute( [0, 0, Auth::id(), $m_id, 1, 'processing', 'confirmed', 'ws_fb', $pause_prev, $pause_latest ]);

        Response::instance()->sendMessage( 'Success', 'success' );
    }

    function supplierPrice() {
        $medicines = isset( $_POST['medicines'] ) ?  $_POST['medicines'] : '';

        $user = User::getUser( Auth::id() );
        $pause_prev = $user->getMeta( 'pause_prev' );
        $pause_latest = $user->getMeta( 'pause_latest' );

        if ( ! $medicines ){
            Response::instance()->sendMessage( 'medicines are required.');
        }
        if ( ! is_array( $medicines ) ){
            Response::instance()->sendMessage( 'medicines are malformed.');
        }

        $user_cash_change = 0;

        foreach ( $medicines as $m_id => $qtyPrice ) {
            if( ! \is_array( $qtyPrice ) || ! isset( $qtyPrice['amount'] ) || ! isset( $qtyPrice['qty'] ) ){
                continue;
            }
            $m_id = (int) $m_id;
            $price = is_numeric($qtyPrice['amount']) ? \round( $qtyPrice['amount'], 2 ) : 0.00;
            $user_cash_change += $price;
            $perPrice = \round( $price/$qtyPrice['qty'], 2 );

            $query = DB::db()->prepare( 'UPDATE t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id SET tom.s_price = ? WHERE tom.s_id = ? AND tom.s_price = ? AND tom.m_id = ? AND tom.ws_available = ? AND tr.o_status IN (?,?) AND tr.o_i_status = ? AND tr.o_created > ? AND tr.o_created <= ?' );
            $query->execute( [$perPrice, Auth::id(), 0.00, $m_id, 1, 'processing', 'confirmed', 'ws_fb', $pause_prev, $pause_latest]);
        }
        if( $user_cash_change ) {
            $user->pCashUpdate( $user_cash_change );
        }

        $query2 = DB::db()->prepare( 'SELECT DISTINCT tom.o_id FROM t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE tom.s_id = ? AND tom.s_price = ? AND tr.o_status IN (?,?) AND tr.o_i_status = ? AND tr.o_created > ? AND tr.o_created <= ?' );
        $query2->execute( [ Auth::id(), 0.00, 'processing', 'confirmed', 'ws_fb', $pause_prev, $pause_latest]);

        $not_completed = $query2->fetchAll( \PDO::FETCH_COLUMN );

        $query3 = DB::db()->prepare( 'SELECT DISTINCT tom.o_id FROM t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE tom.s_id = ? AND tr.o_status IN (?,?) AND tr.o_i_status = ? AND tr.o_created > ? AND tr.o_created <= ?' );
        $query3->execute( [ Auth::id(), 'processing', 'confirmed', 'ws_fb', $pause_prev, $pause_latest]);

        $all_orders = $query3->fetchAll( \PDO::FETCH_COLUMN );

        $completed_orders = \array_diff( $all_orders, $not_completed );

        if( $completed_orders ) {
            $in  = str_repeat('?,', count($completed_orders) - 1) . '?';
            $query4 = DB::db()->prepare( "UPDATE t_orders SET o_status = ?, o_i_status = ? WHERE o_id IN ($in)" );
            array_unshift( $completed_orders, 'confirmed', 'confirmed');
            $query4->execute( $completed_orders );
        }

        $data = [
            'u_cash' => \round( $user->u_cash, 2 ),
            'u_p_cash' => \round( $user->u_p_cash, 2 ),
        ];

        Response::instance()->sendData( [ 'user' => $data ], 'success' );
    }

    function pause() {
        $user = User::getUser( Auth::id() );
        $pauseStatus = $user->getMeta( 'pauseStatus' );
        $pause_prev = $user->getMeta( 'pause_prev' );
        $pause_latest = $user->getMeta( 'pause_latest' );

        $newStatus = ('paused' == $pauseStatus ) ? 'released' : 'paused';

        if( 'paused' == $newStatus ) {
            if( $pause_latest && ( \strtotime( \date( 'Y-m-d H:i:s' ) ) - \strtotime( $pause_latest ) < 60  ) ) {
                Response::instance()->sendMessage( 'You cannot pause too often.');
            }
        } else {
            $query = DB::db()->prepare( 'SELECT tom.om_id FROM t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE tom.s_id = ? AND tom.s_price = ? AND tom.ws_available = ? AND tr.o_status IN (?,?) AND tr.o_i_status = ? AND tr.o_created > ? AND tr.o_created <= ?' );
            $query->execute( [ Auth::id(), 0.00, 1, 'processing', 'confirmed', 'ws_fb', $pause_prev, $pause_latest]);
            if( $query->fetch() ){
                Response::instance()->sendMessage( 'You cannot release without giving all medicines feedback.');
            }
        }
        $user->setMeta( 'pauseStatus', $newStatus );

        if( 'paused' == $newStatus ) {
            $user->setMeta( 'pause_prev', $pause_latest );
            $user->setMeta( 'pause_latest', \date( 'Y-m-d H:i:s' ) );
        }

        Response::instance()->sendData( [ 'pauseStatus' => $user->getMeta( 'pauseStatus' ) ], 'success' );
    }

}