<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate, Inventory, Meta};
use GuzzleHttp\Client;

class Functions {

    function __construct() {
    }

    public static function randToken( $type, $length ){
        switch ( $type ) {
            case 'alpha':
                $string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'hexdec':
                $string = '0123456789abcdef';
                break;
            case 'numeric':
                $string = '0123456789';
                break;
            case 'distinct':
                $string = '2345679ACDEFHJKLMNPRSTUVWXYZ';
                break;
            case 'alnumlc':
                $string = '0123456789abcdefghijklmnopqrstuvwxyz';
                break;
            case 'alnum':
            default:
                $string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
        }
        $max = strlen($string);
        $token = '';
        for ($i=0; $i < $length; $i++) {
            $token .= $string[random_int(0, $max-1)];
        }
    
        return $token;
    }

    public static function sendSMS( $url, $data ){
        if( ! $url || ! $data || ! \is_array( $data ) ){
            return false;
        }
        if( !MAIN ) {
            return false;
        }
        try {
            $client = new Client(['verify' => false, 'http_errors' => false]);
            $client->post( $url, [
                'form_params' => $data,
            ]);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function sendOTPSMS( $mobile, $otp ) {
        if( !$mobile || !$otp ) {
            return false;
        }
        if( 0 === \strpos( $mobile, '+880100000000') ) {
            return false;
        }
        /*
        $data = [
            'u' => 'arogga',
            'h' => ALPHA_SMS_KEY,
            'op' => 'pv',
            'to' => $mobile,
            'msg' => "Your Arogga OTP is: {$otp}\nUID:7UvkiTFw3Ha"
        ];
        static::sendSMS( 'https://alphasms.biz/index.php?app=ws', $data );
        */
        
        $data = [
            'token' => GREENWEB_SMS_KEY,
            'to' => $mobile,
            'message' => "Your Arogga OTP is: {$otp}\nUID:7UvkiTFw3Ha"
        ];
        static::sendSMS( 'http://api.greenweb.com.bd/api.php', $data );
        
        /*
        $data = [
            'api_key' => '16601202823928832020/03/0305:41:15amShamimHasan',
            'mobile_no' => $mobile,
            'message' => "Your 2 Arogga OTP is: {$otp}\nUID:7UvkiTFw3Ha",
            'User_Email' => 'testshamimhasan@gmail.com',
            'sender_id' => '47',

        ];
        static::sendSMS( 'https://71bulksms.com/sms_api/bulk_sms_sender.php', $data );
        */
    }

    public static function sendNotification( $fcm_token, $title, $message, $extraData = [] ) {
        if( !$fcm_token || ! $title || ! $message ) {
            return false;
        }
        if( !MAIN ) {
            return false;
        }
        try {
            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'key='. FCM_SERVER_KEY,
                ],
                'http_errors' => false,
            ]);
            $client->post('https://fcm.googleapis.com/fcm/send',
                ['body' => json_encode([
                    'notification' => [
                        'title' => $title,
                        'body' => $message
                    ],
                    'data' => [
                        'title' => $title,
                        'body' => $message,
                        'extraData' => $extraData,
                    ],
                    'to' => $fcm_token
                ])]
            );
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public static function sendOrderStatusChangeNotification( $prev_order, $order ) {
        if( ! $prev_order || !$order || $prev_order->o_status == $order->o_status ) {
            return false;
        }
        $user = User::getUser( $order->u_id );
        if( $user && $user->fcm_token ) {
            $title = '';
            $message = '';
            if( 'confirmed' == $order->o_status && 'online' !== $order->o_payment_method ) {
                $title = 'Order Confirmed';
                $message = "Hello {$order->u_name}\nYour order #{$order->o_id} is confirmed.";
            } elseif('delivering' == $order->o_status ) {
                //$title = 'Order Delivering';
                //$message = "Hello {$order->u_name}\nWe have handed over your order #{$order->o_id} to delivery man. Please be ready to receive it shortly. Delivery man will call you once he reaches your address.";
            } elseif('delivered' == $order->o_status ) {
                $title = 'Order Delivered';
                $message = "Hello {$order->u_name}\nYour order #{$order->o_id} is delivered. Thank you for your purchase.";
            } elseif('cancelled' == $order->o_status ) {
                $title = 'Order Cancelled';
                $message = "Hello {$order->u_name}\nYour order #{$order->o_id} is cancelled.";
            }
            /*
             elseif('cancelled' == $order->o_status ) {
                $title = 'Order Cancelled';
                $message = "Hello {$order->u_name}\nDue to coronavirus outbreak we found unexpected spike in order volume and shortage of delivery personnel both at the same time.\nFor that reason, we are unable to fulfil your order #{$order->o_id}.We had to cancel the order and reorganize our delivery channel for this pandemic situation.\nWe are reshaping our delivery channel so that we can serve you in future with faster delivery service.\nThanks for your understanding.";
            }
            */
            if( $title && $message ) {
                return static::sendNotification( $user->fcm_token, $title, $message, ['screen' => 'Orders', 'btnScreen' => 'SingleOrder', 'btnScreenParams' => ['o_id' => $order->o_id], 'btnLabel' => 'View Order'] );
            }
        }
        return false;
    }

    public static function sendInternalOrderStatusChangeNotification( $prev_order, $order ) {
        if( ! $prev_order || !$order || $prev_order->o_i_status == $order->o_i_status ) {
            return false;
        }
        $user = User::getUser( $order->u_id );
        if( $user && $user->fcm_token ) {
            $title = '';
            $message = '';
            if( 'later' == $order->o_i_status ) {
                //$title2 = "Your order #{$order->o_id} may delay";
                //$message2 = "Hello {$user2->u_name}\nOne or more medicines of your order #{$order->o_id} is not available in our stock right now. We need extra few hours to make this availabe for you.\nYour estimated delivery time is 48 hours from now.\nIf you want to cancel this order for this extra delay, please call customer care 09606999688, we will cancel it for you.";
            }
            if( 'confirmed' == $order->o_status && 'confirmed' == $order->o_i_status && 'online' == $order->o_payment_method ){
                $title = 'Order Confirmed';
                $message = "Hello {$order->u_name}\nYour order #{$order->o_id} is confirmed.\nYou can now pay using your bKash/Nagad/Debit/Credit card.";
                return static::sendNotification( $user->fcm_token, $title, $message, ['screen' => 'Orders', 'btnScreen' => 'AroggaWebView', 'btnScreenParams' => ['uri' => \sprintf( SITE_URL . '/payment/v1/%d/%s/', $order->o_id, $order->getMeta( 'o_secret' ) )], 'btnLabel' => 'Pay Online'] );
            }
            if( $title && $message ) {
                return static::sendNotification( $user->fcm_token, $title, $message );
            }
        }
        return false;
    }

    public static function calculateCash( $prev_order, $order ) {
        if( ! $prev_order || ! $order ) {
            return false;
        }
        
        $ph_user = User::getUser( $order->o_ph_id );

        if( $ph_user && $prev_order->o_total != $order->o_total && 'delivering' == $prev_order->o_status && 'delivering' == $order->o_status ) {
            $ph_user->pCashUpdate( $order->o_total - $prev_order->o_total );
        }

        if( $prev_order->o_status == $order->o_status ) {
            return false;
        }

        if( ! \in_array( $order->o_status, [ 'delivering', 'delivered', 'cancelled', 'damaged', 'returned' ] ) ) {
            return false;
        }

        if( $order->o_total && $ph_user ){
            if ( 'delivering' == $order->o_status ) {
                $ph_user->u_p_cash = $ph_user->u_p_cash + $order->o_total;
            } elseif ( \in_array( $order->o_status, [ 'delivered', 'damaged' ] ) && 'delivering' == $prev_order->o_status ) {
                $ph_user->u_p_cash = $ph_user->u_p_cash - $order->o_total;
                $ph_user->u_cash = $ph_user->u_cash + $order->o_total;
            } elseif ( 'delivering' == $prev_order->o_status && 'cancelled' == $order->o_status ) {
                $ph_user->u_p_cash = $ph_user->u_p_cash - $order->o_total;
            } elseif( 'delivered' == $prev_order->o_status && 'returned' == $order->o_status ) {
                $ph_user->u_cash = $ph_user->u_cash - $order->o_total;
            }
            $ph_user->update();
        }

        $cash_back = $order->cashBackAmount();

        if( $cash_back && ( $user = User::getUser( $order->u_id ) ) ) {
            if( in_array( $order->o_status, [ 'delivered', 'cancelled', 'damaged' ] ) ){
                $user->u_p_cash = $user->u_p_cash - $cash_back;
            }
            if ( \in_array( $order->o_status, [ 'delivered' ] ) ) {
                $user->u_cash = $user->u_cash + $cash_back;
            }
            if ( 'delivered' == $prev_order->o_status && 'returned' == $order->o_status ) {
                $user->u_cash = $user->u_cash - $cash_back;
            }
            $user->update();
        }
        return true;
    }

    public static function referralCash( $prev_order, $order ) {
        if( ! $prev_order || !$order || $prev_order->o_status == $order->o_status ) {
            return false;
        }

        if( ! \in_array( $order->o_status, [ 'delivered' ] ) ) {
            return false;
        }
        $user = User::getUser( $order->u_id );
        if( ! $user || ! $user->u_r_uid ) {
            return false;
        }

        $query = DB::db()->prepare( 'SELECT o_id FROM t_orders WHERE u_id = ? AND o_status = ? LIMIT 2' );
        $query->execute( [ $user->u_id, 'delivered' ] );
        $result = $query->fetchAll();

        if( \count( $result ) === 1 ){
            $r_user = User::getUser( $user->u_r_uid );
            $cashback_amount = 20;
            if( $r_user && $r_user->cashUpdate( $cashback_amount ) ) {
                if( $r_user->fcm_token ){
                    $title = "Referral Cashback Received.";
                    $message = "Hello {$r_user->u_name}\nYou have received {$cashback_amount}Tk cashback as user registered and ordered with your referral code.";
                    static::sendNotification( $r_user->fcm_token, $title, $message );
                }
                return true;
            }
        }
        return false;
    }

    public static function miscOrderUpdate( $prev_order, $order ) {
        if( !$prev_order || !$order ) {
            return false;
        }

        if( ( $prev_order->o_status != $order->o_status && \in_array( $order->o_status, ['confirmed', 'delivering', 'delivered'] ) ) || ( $prev_order->o_i_status != $order->o_i_status && \in_array( $order->o_i_status, ['ph_fb', 'confirmed'] ) ) ) {
            foreach ( $order->medicineQty as $id_qty ) {
                $m_id = isset($id_qty['m_id']) ? (int)$id_qty['m_id'] : 0;
                $quantity = isset($id_qty['qty']) ? (int)$id_qty['qty'] : 0;
                $om_status = isset( $id_qty['om_status'] ) ? $id_qty['om_status']: '';
    
                if( \in_array( $om_status, ['pending', 'later'] ) ){
                    $inventory = Inventory::getByPhMid( $order->o_ph_id, $m_id );

                    if( $inventory && $inventory->i_qty >= $quantity ){
                        $inventory->i_qty = $inventory->i_qty - $quantity;
                        $inventory->update();
                        DB::instance()->update( 't_o_medicines', ['om_status' => 'available', 's_price' => $inventory->i_price ], [ 'o_id' => $order->o_id, 'm_id' => $m_id ] );
                    } else {
                        if( 'pending' == $om_status ){
                            DB::instance()->update( 't_o_medicines', ['om_status' => 'later' ], [ 'o_id' => $order->o_id, 'm_id' => $m_id ] );
                        }
                    }
                }
            }
        }

        if( $prev_order->o_status != $order->o_status && 'delivering' == $order->o_status ) {
            DB::instance()->update( 't_o_medicines', ['om_status' => 'available'], [ 'o_id' => $order->o_id, 'om_status' => 'packed' ] );
        }
        if( $prev_order->o_status != $order->o_status && in_array( $order->o_status, ['cancelled', 'returned'] ) ) {
            $ph_m_ids = [];
            $iv_insert = [];
            foreach ( $order->medicineQty as $id_qty ) {
                $m_id = isset($id_qty['m_id']) ? (int)$id_qty['m_id'] : 0;
                $quantity = isset($id_qty['qty']) ? (int)$id_qty['qty'] : 0;
                $om_status = isset( $id_qty['om_status'] ) ? $id_qty['om_status']: '';
                $s_price = isset( $id_qty['s_price'] ) ? $id_qty['s_price']: '';
    
                if( \in_array( $om_status, ['packed', 'available'] ) ){
                    $ph_m_ids[ $order->o_ph_id ][] = $m_id;
                    if( $inventory = Inventory::getByPhMid( $order->o_ph_id, $m_id ) ){
                        if(($inventory->i_qty + $quantity)){
                            $inventory->i_price = ( ($inventory->i_price * $inventory->i_qty ) + ($s_price * $quantity ) ) / ($inventory->i_qty + $quantity);
                        } else {
                            $inventory->i_price = '0.00';
                        }
                        $inventory->i_qty = $inventory->i_qty + $quantity;
                        $inventory->update();
                    } else {
                        $iv_insert[] = [
                            'i_ph_id' => $order->o_ph_id,
                            'i_m_id' => $m_id,
                            'i_price' => $s_price,
                            'i_qty' => $quantity,
                        ];
                    }
                }
            }
            if( $iv_insert ){
                DB::instance()->insertMultiple( 't_inventory', $iv_insert );
            }
            DB::instance()->update( 't_o_medicines', ['om_status' => 'pending'], ['o_id' => $order->o_id] );

            if( $ph_m_ids ){
                Functions::checkOrdersForInventory( $ph_m_ids );
                Functions::checkOrdersForPacking();
            }
        }
        /*
        //Done after modify order medicines
        if( $prev_order->o_i_status != $order->o_i_status && 'ph_fb' == $order->o_i_status ){
            $query = DB::db()->prepare( 'SELECT DISTINCT om_status FROM t_o_medicines WHERE o_id = ? AND m_qty > ?' );
            $query->execute( [ $order->o_id, 0 ] );

            $i_status_confirmed = true;
            while( $om = $query->fetch() ){
                if( 'available' !== $om['om_status'] ){
                    $i_status_confirmed = false;
                    break;
                }
            }
            if( $i_status_confirmed ){
                $order->update( [ 'o_i_status' => 'packing' ] );
            }
        }
        */
        if( $prev_order->o_status != $order->o_status && 'delivered' == $order->o_status ) {
            $query = DB::db()->prepare( "SELECT SUM(s_price*m_qty) FROM t_o_medicines WHERE o_id = ? AND om_status = ?" );
            $query->execute( [ $order->o_id, 'available' ] );
            $order->setMeta( 'supplierPrice', round( $query->fetchColumn(), 2 ) );

            if( 'paid' === $order->getMeta( 'paymentStatus' ) ){
                $order->update( [ 'o_i_status' => 'paid' ] );
            }
        }
    }

    public static function checkOrdersForInventory( $ph_m_ids ){
        DB::db()->beginTransaction();
        try {
            if( $ph_m_ids ){
                foreach ( $ph_m_ids as $ph_id => $m_ids ) {
                    $db = new DB;
                    $db->add( 'SELECT tom.om_id, tom.m_id, tom.m_qty, tr.o_id FROM t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE 1=1' );
                    $in  = str_repeat('?,', count($m_ids) - 1) . '?';
                    $db->add( " AND tom.m_id IN ($in)", ...$m_ids );
                    $db->add( ' AND tom.om_status IN (?,?)', 'pending', 'later' );
                    $db->add( ' AND tr.o_ph_id = ?', $ph_id );
                    $db->add( ' AND tr.o_status IN (?,?)', 'confirmed', 'delivering' );
                    $db->add( ' AND tr.o_i_status IN (?,?)', 'ph_fb', 'confirmed' );
                    $db->add( ' ORDER BY tr.o_id ASC' );
                    $query = $db->execute();
        
                    while( $om = $query->fetch() ) {
                        if( ( $inventory = Inventory::getByPhMid( $ph_id, $om['m_id'] ) ) && $inventory->i_qty >= $om['m_qty'] ){
                            $inventory->i_qty = $inventory->i_qty - $om['m_qty'];
                            $inventory->update();
                            DB::instance()->update( 't_o_medicines', ['om_status' => 'available', 's_price' => $inventory->i_price ], [ 'om_id' => $om['om_id'] ] );
                        }
                    }
                }
            }

            DB::db()->commit(); 
        } catch(\PDOException $e) {
            DB::db()->rollBack();
            \error_log( $e->getMessage() );
            //Response::instance()->sendMessage( 'Something wrong, Please try again.' );
        }
    }

    public static function checkOrdersForPacking(){
        $query = DB::db()->prepare( 'SELECT DISTINCT tom.o_id, tom.om_status FROM t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE tr.o_status = ? AND tr.o_i_status = ? AND tom.m_qty > ?' );
        $query->execute( [ 'confirmed', 'ph_fb', 0 ]);
        $oid_statuses = [];
        while( $om = $query->fetch() ){
            $oid_statuses[ $om['o_id'] ][] = $om['om_status'];
        }
        $confirmed_orders = [];
        foreach ( $oid_statuses as $o_id => $statuses ) {
            if( count( $statuses ) === 1 && 'available' == $statuses[0] ){
                $confirmed_orders[] = $o_id;
            }
        }
        if( $confirmed_orders ){
            $in  = str_repeat('?,', count($confirmed_orders) - 1) . '?';
            $query = DB::db()->prepare( "UPDATE t_orders SET o_i_status = ? WHERE o_id IN ($in)" );
            $new_array = ['packing'];
            $new_array = array_merge( $new_array, $confirmed_orders );
            $query->execute( $new_array );
            foreach ( $confirmed_orders as $o_id ) {
                Cache::instance()->delete( $o_id, 'order' );
            }
        }
    }

    public static function setInternalOrderStatus( $order ){
        if( ! $order || 'confirmed' != $order->o_status ||  ! in_array( $order->o_i_status, [ 'ph_fb', 'packing' ] ) ){
            return false;
        }
        $query = DB::db()->prepare( 'SELECT DISTINCT om_status FROM t_o_medicines WHERE o_id = ? AND m_qty > ?' );
        $query->execute( [ $order->o_id, 0 ] );
        $statuses = $query->fetchAll( \PDO::FETCH_COLUMN );

        if( 'ph_fb' == $order->o_i_status ){
            if( count( $statuses ) === 1 && 'available' == $statuses[0] ){
                $order->update( [ 'o_i_status' => 'packing' ] );
            }
        } elseif( 'packing' == $order->o_i_status ){
            if( count( $statuses ) > 1 || 'available' != $statuses[0] ){
                $order->update( [ 'o_i_status' => 'ph_fb' ] );
            }
        }
    }

    public static function orderCreated( $order ){
        if( ! $order || ! $order->exist() ){
            return false;
        }

        if( $order->u_id && ( $user = User::getUser( $order->u_id ) ) ){
            $user->update( [ 'u_o_count' => $user->u_o_count + 1 ] );
        }
    }

    public static function orderDeleted( $order ){
        if( ! $order || ! $order->exist() ){
            return false;
        }

        if( $order->u_id && ( $user = User::getUser( $order->u_id ) ) ){
            $user->update( [ 'u_o_count' => $user->u_o_count - 1 ] );
        }
    }

    public static function cartData( $user, $medicines, $d_code = '', $prev_applied_cash = 0, $offline = false, $args = [] ) {
        $subtotal = '0.00';
        $total = '0.00';
        $saving = 0;
        $d_amount = '0.00';
        $a_amount = '0.00';
        $rx_req = false;
        $m_return = [];
        $additions = [];
        $deductions = [];
        $free_delivery = false;

        $u_id = $user ? $user->u_id : 0;

        foreach ( $medicines as $key => $value ) {
            if( is_array( $value ) ) {
                $m_id = isset($value['m_id']) ? (int)$value['m_id'] : 0;
                $quantity = isset($value['qty']) ? (int)$value['qty'] : 0;
            } else {
                $m_id = (int) $key;
                $quantity = (int) $value;
            }

            if( ! ( $medicine = Medicine::getMedicine( $m_id ) ) ) {
                continue;
            }
            if( ! $medicine->m_rob ) {
                continue;
            }
            if ( (bool)$medicine->m_rx_req ) {
                $rx_req = true;
            }
            $quantity = (int) $quantity;
            $price = $medicine->m_price ? \round( $medicine->m_price * $quantity, 2 ) : 0;
            $d_price = $medicine->m_d_price ? \round( $medicine->m_d_price * $quantity, 2 ) : 0;
            $d_price = $offline ? $price : $d_price;

            $m_return[ $m_id ] = [
                'qty' => $quantity,
                'm_id' => $m_id,
                'name' => $medicine->m_name,
                'strength' => $medicine->m_strength,
                'form' => $medicine->m_form,
                'unit' => $medicine->m_unit,
                'price' => $price,
                'd_price' => $d_price,
                'rx_req' => $medicine->m_rx_req,
                'pic_url' => $medicine->m_pic_url,
            ];
            $subtotal += $price;
            $saving   += ( $price - $d_price );
        }
        if( $saving ) {
            $d_amount    += $saving;
            $deductions['saving'] = [
                'amount' => \round( $saving, 2 ),
                'text' => 'Discount applied',
                'info' => '',
            ];
        }
        $discount = Discount::getDiscount( $d_code );

        if( $discount && $discount->canUserUse( $u_id ) ) {
            if ( 'percent' === $discount->d_type || 'firstPercent' === $discount->d_type ) {
                $amount = ( ( $subtotal - $d_amount ) / 100 ) * $discount->d_amount;
                if ( ! empty( $discount->d_max ) ) {
                    $amount = min( $discount->d_max, $amount );
                }
            } elseif( 'fixed' === $discount->d_type || 'firstFixed' === $discount->d_type ) {
                $amount = $discount->d_amount;
            } elseif( 'free_delivery' === $discount->d_type ) {
                $amount = 0; //For free delivery we will calculate later
                $free_delivery = true;
            } else {
                $amount = 0;
                //$free_delivery = false;
            }
            $d_amount    += \round( $amount, 2 );

            $deductions['discount'] = [
                'amount' => \round( $amount, 2 ),
                'text' => "Coupon applied ($discount->d_code)",
                'info' => ! empty( $free_delivery ) ? 'Free Delivery' : '',
            ];
        }
        if( $user && ( $user->u_cash || $prev_applied_cash ) ) {
            if ( ( $subtotal - $d_amount ) > 0 ) {
                $amount = \round( $subtotal - $d_amount, 2 );
                $amount = \round( \min( $amount, $user->u_cash + $prev_applied_cash ), 2);
                $d_amount    += $amount;

                $deductions['cash'] = [
                    'amount' => $amount,
                    'text' => 'arogga cash applied',
                    'info' => '',
                ];
            } else {
                /*
                $deductions['cash'] = [
                    'amount' => '0.00',
                    'text' => 'arogga cash',
                    'info' => 'To use arogga cash order more than ৳499',
                ];
                */
            }
            
        }
        if ( ! empty( $args['man_discount'] ) ) {
            $amount = \round( $args['man_discount'], 2 );
            $d_amount    += $amount;

            $deductions['man_discount'] = [
                'amount' => $amount,
                'text' => 'Manual discount applied',
                'info' => '',
            ];
        }

        if( ! $offline && ! $free_delivery ){
            if( empty( $args['s_address'] ) || empty( $args['s_address']['district'] ) ){
                $args['s_address']['district'] = 'Dhaka City';
            }
            if( 'Dhaka City' == $args['s_address']['district'] ){
                if ( ( $subtotal - $d_amount ) < 999 ) {
                    $additions['delivery'] = [
                        'amount' => '39.00',
                        'text' => 'Delivery charge',
                        'info' => 'To get free delivery order more than ৳999',
                    ];
                    $a_amount += 39.00;
                } else {
                    $additions['delivery'] = [
                        'amount' => '00',
                        'text' => 'Delivery charge',
                        'info' => '',
                    ];
                }
            } else {
                //Other districts
                $additions['delivery'] = [
                    'amount' => '89.00',
                    'text' => sprintf( 'Delivery charge', $args['s_address']['district'] ),
                    'info' => sprintf( '%s, %s', $args['s_address']['area'], $args['s_address']['district'] ),
                ];
                $a_amount += 89.00;
            }
        }

        if ( ! empty( $args['man_addition'] ) ) {
            $amount = \round( $args['man_addition'], 2 );
            $a_amount    += $amount;

            $deductions['man_addition'] = [
                'amount' => $amount,
                'text' => 'Manual addition applied',
                'info' => '',
            ];
        }
        $subtotal = \round( $subtotal, 2 );
        $total = \round( $subtotal - $d_amount + $a_amount, 2);
        $total_floor = \floor($total);
        if( $total_floor < $total ) {
            $deductions['rounding'] = [
                'amount' => \round( $total - $total_floor, 2 ),
                'text' => 'Rounding Off',
                'info' => '',
            ];
            $d_amount    += \round( $total - $total_floor, 2 );
            $total = $total_floor;
        }

        if ( $total > 4999 ) {
            $cash_back = 100;
        } elseif( $total > 3999 ) {
            $cash_back = 80;
        } elseif( $total > 2999 ) {
            $cash_back = 60;
        } elseif( $total > 1999 ) {
            $cash_back = 40;
        } elseif( $total > 999 ) {
            $cash_back = 20;
        } else {
            $cash_back = 0;
        }

        if ( $offline ) {
            $cash_back = 0;
        }
        
        $data = [
            'medicines'   => $m_return,
            'deductions'  => $deductions,
            'additions'   => $additions,
            'subtotal'    => \round( $subtotal, 2 ),
            'total'       => \round( $total, 2),
            'a_amount'    => \round( $a_amount, 2),
            'd_amount'    => \round( $d_amount, 2),
            'total_items' => count( $m_return ),
            'rx_req'      => $rx_req,
            //'rx_req'      => false,
            'cash_back'   => $cash_back,
            'a_message'   => '', //additional message, Show just above purchase button
        ];

        return $data;
    }

    public static function ModifyOrderMedicines( $order, $medicineQty, $prev_order_data = null ) {
        if( ! $order || ! $order->o_id) {
            return false;
        }

        $new_ids = [];
        $insert = [];
        $old_data = [];

        $query = DB::instance()->select( 't_o_medicines', [ 'o_id' => $order->o_id ], 'm_id, m_qty, m_price, m_d_price, s_price, om_status' );
        while ( $old = $query->fetch() ) {
            $old_data[ $old['m_id'] ] = $old;
        }

        foreach ( $medicineQty as $id_qty ) {
            $m_id = isset($id_qty['m_id']) ? (int)$id_qty['m_id'] : 0;
            $quantity = isset($id_qty['qty']) ? (int)$id_qty['qty'] : 0;

            if( ! ( $medicine = Medicine::getMedicine( $m_id ) ) ) {
                continue;
            }
            $new_ids[] = $medicine->m_id;

            if( isset( $old_data[ $medicine->m_id ] ) ) {
                $change = [];
                if( $quantity != $old_data[ $medicine->m_id ]['m_qty'] ) {
                    $change['m_qty'] = $quantity;
                    if( $prev_order_data && $order->o_ph_id ){
                        //update inventory
                        if( 'available' == $old_data[ $medicine->m_id ]['om_status'] ){
                            if( $old_data[ $medicine->m_id ]['m_qty'] >= $quantity ){
                                Inventory::qtyUpdateByPhMid( $order->o_ph_id, $medicine->m_id, $old_data[ $medicine->m_id ]['m_qty'] - $quantity );
                            } else {
                                $inventory = Inventory::getByPhMid( $order->o_ph_id, $medicine->m_id );
                                if( ( $inventory->i_qty + $old_data[ $medicine->m_id ]['m_qty'] ) >= $quantity ){
                                    Inventory::qtyUpdateByPhMid( $order->o_ph_id, $medicine->m_id, $old_data[ $medicine->m_id ]['m_qty'] - $quantity );
                                } else {
                                    Inventory::qtyUpdateByPhMid( $order->o_ph_id, $medicine->m_id, $old_data[ $medicine->m_id ]['m_qty'] );
                                    $change['om_status'] = 'later';
                                }
                            }
                        } elseif( 'later' == $old_data[ $medicine->m_id ]['om_status'] ){
                            $inventory = Inventory::getByPhMid( $order->o_ph_id, $medicine->m_id );
                            if( $inventory && $inventory->i_qty >= $quantity ){
                                $inventory->i_qty = $inventory->i_qty - $quantity;
                                $inventory->update();
                                $change['s_price'] = $inventory->i_price;
                                $change['om_status'] = 'available';
                            }
                        }
                    }
                }
                if( $medicine->m_price != $old_data[ $medicine->m_id ]['m_price'] ) {
                    $change['m_price'] = $medicine->m_price;
                }
                if( $medicine->m_d_price != $old_data[ $medicine->m_id ]['m_d_price'] ) {
                    $change['m_d_price'] = $medicine->m_d_price;
                }
                if( $change ) {
                    DB::instance()->update( 't_o_medicines', $change, [ 'o_id' => $order->o_id, 'm_id' => $medicine->m_id ] );
                }
            } else {
                $insert_data = [
                    'o_id' => $order->o_id,
                    'm_id' => $medicine->m_id,
                    'm_unit' => $medicine->m_unit,
                    'm_price' => $medicine->m_price ?: 0,
                    'm_d_price' => $medicine->m_d_price ?: 0,
                    'm_qty' => $quantity,
                ];
                
                if( $prev_order_data ){
                    $inventory = Inventory::getByPhMid( $order->o_ph_id, $medicine->m_id );
                    if( $inventory && $inventory->i_qty >= $quantity ){
                        $inventory->i_qty = $inventory->i_qty - $quantity;
                        $inventory->update();
                        $insert_data['s_price'] = $inventory->i_price;
                        $insert_data['om_status'] = 'available';
                    } else {
                        $insert_data['s_price'] = 0;
                        $insert_data['om_status'] = 'later';
                    }
                }
                $insert[] = $insert_data;
            }
        }
        $d_ids = \array_diff( \array_keys( $old_data ), $new_ids );

        if( $d_ids ) {
            $in  = str_repeat('?,', count($d_ids) - 1) . '?';

            $query = DB::db()->prepare( "DELETE FROM t_o_medicines WHERE o_id = ? AND m_id IN ($in)" );
            $query->execute( \array_merge([$order->o_id], $d_ids) );
            if( $prev_order_data && $order->o_ph_id ){
                foreach ( $d_ids as $d_id ) {
                    if( 'available' == $old_data[ $d_id ]['om_status'] ){
                        //update inventory
                        Inventory::qtyUpdateByPhMid( $order->o_ph_id, $d_id, $old_data[ $d_id ]['m_qty'] );
                    }
                }
            }
        }
        DB::instance()->insertMultiple( 't_o_medicines', $insert );

        Functions::setInternalOrderStatus( $order );

        return true;
    }

    public static function maybeJsonEncode( $value ) {
        if ( is_array( $value ) || is_object( $value ) ) {
            return \json_encode( $value );
        }
        return $value;
    }

    public static function maybeJsonDecode( $value ) {
        if ( ! is_string( $value ) ) { return $value; }
        if ( strlen( $value ) < 2 ) { return $value; }
        if( 0 !== \strpos( $value, '{') && 0 !== \strpos( $value, '[') ) { return $value; }
    
        $json_data = \json_decode( $value, true );
        if ( \json_last_error() !== \JSON_ERROR_NONE ) { return $value; }
        return $json_data;
    }

    public static function pointInPolygon($p, $polygon) {
        if( !$p || !\is_array($p) || !\is_array($polygon) ) {
            return false;
        }
        //if you operates with (hundred)thousands of points
        //set_time_limit(60);
        $c = 0;
        $p1 = $polygon[0];
        $n = count($polygon);
    
        for ($i=1; $i<=$n; $i++) {
            $p2 = $polygon[$i % $n];
            if ($p[1] > min($p1[1], $p2[1])
                && $p[1] <= max($p1[1], $p2[1])
                && $p[0] <= max($p1[0], $p2[0])
                && $p1[1] != $p2[1]) {
                    $xinters = ($p[1] - $p1[1]) * ($p2[0] - $p1[0]) / ($p2[1] - $p1[1]) + $p1[0];
                    if ($p1[0] == $p2[0] || $p[0] <= $xinters) {
                        $c++;
                    }
            }
            $p1 = $p2;
        }
        // if the number of edges we passed through is even, then it's not in the poly.
        return $c%2!=0;
    }

    public static function isInside($lat, $long, $area = false ) {      
        $locations = [
            'dhaka' => [
              [23.663722, 90.456617],
              [23.710720, 90.509878],
              [23.782770, 90.473925],
              [23.826433, 90.488741],
              [23.901250, 90.448270],
              [23.883042, 90.398767],
              [23.901272, 90.384341],
              [23.882106, 90.348942],
              [23.752972, 90.328691],
              [23.709614, 90.363012],
              [23.706776, 90.404532],
            ],
            'chittagong' => [
              [22.368476, 91.753262],
              [22.371652, 91.754636],
              [22.430376, 91.871818],
              [22.429093, 91.890720],
              [22.416709, 91.883488],
              [22.403997, 91.890347],
              [22.332232, 91.863481],
              [22.307491, 91.800634],
              [22.281449, 91.796849],
              [22.262373, 91.838371],
              [22.224910, 91.801293],
              [22.273198, 91.763899],
            ],
        ];
      
        $isInside = false;
        foreach ($locations as $city => $location) {
            if( $area && $area != $city ){
                continue;
            }
            if( static::pointInPolygon([$lat, $long], $location) ){
                $isInside = true;
                break;
            }
        }
        return $isInside;
      }

      public static function ledgerCreate( $reason, $amount, $type ) {
          if( \in_array( $type, ['collection', 'input', 'credit'] ) ){
            $amount = \abs( $amount );
          } elseif( \in_array( $type, ['salary', 'purchase', 'debit'] ) ){
            $amount = \abs( $amount ) * -1;
          }
          $data = [
            'l_uid' => Auth::id(),
            'l_created' => \date( 'Y-m-d H:i:s' ),
            'l_reason' => \mb_strimwidth( $reason, 0, 255, '...' ),
            'l_type'   => $type,
            'l_amount' => \round( $amount, 2 ),
          ];
          return DB::instance()->insert( 't_ledger', $data );
      }

    public static function getPicUrl( $m_id ){
        $url = '';
        if( ! $m_id ){
            return $url;
        }
        $path = \sprintf( '/images/medicine/%d/%d.jpg', \floor( $m_id / 1000 ), $m_id );

        if ( \file_exists( STATIC_DIR . $path ) ) {
            $url = STATIC_URL . $path . '?v=' . @\filemtime(STATIC_DIR . $path) ?: 1;
        }
        return $url;
    }
    
    public static function getProfilePicUrl( $u_id ){
        $url = '';
        if( ! $u_id ){
            return $url;
        }
        $path = \sprintf( '/users/%d/%d-*.{jpg,jpeg,png,gif}', \floor( $u_id / 1000 ), $u_id );
        $image_path = '';
        foreach( glob( STATIC_DIR . $path, GLOB_BRACE ) as $image ){
            $image_path = $image;
            break;
        }
        if ( $image_path ) {
            $url = str_replace( STATIC_DIR, STATIC_URL, $image_path );
        }
        return $url;
    }

    public static function getPicUrls( $m_id ){
        $url = [];
        if( ! $m_id ){
            return $url;
        }
        $basePath = \sprintf( '/images/medicine/%d/%d', \floor( $m_id / 1000 ), $m_id );

        foreach ( [0,1,2,3,4] as $number ) {
            $path = $basePath;
            if( $number ){
                $path .= "-{$number}";
            }
            $path .= '.jpg';

            if ( \file_exists( STATIC_DIR . $path ) ) {
                $url[] = STATIC_URL . $path . '?v=' . @\filemtime(STATIC_DIR . $path) ?: 1;
            } else {
                break;
            }
        }
        return $url;
    }

    public static function getPicUrlsAdmin( $m_id ){
        $url = [];
        if( ! $m_id ){
            return $url;
        }
        $basePath = \sprintf( '/images/medicine/%d/%d', \floor( $m_id / 1000 ), $m_id );

        foreach ( [0,1,2,3,4] as $number ) {
            $path = $basePath;
            if( $number ){
                $path .= "-{$number}";
            }
            $path .= '.jpg';

            if ( \file_exists( STATIC_DIR . $path ) ) {
                $url[] = [
                    'src' => STATIC_URL . $path . '?v=' . @\filemtime(STATIC_DIR . $path) ?: 1,
                    'title' => basename( $path ),
                ];
            } else {
                break;
            }
        }
        return $url;
    }

    public static function modifyMedicineImages( $m_id, $images ){
        if( ! $m_id || ! is_array( $images ) ){
            return false;
        }
        $upload_folder = STATIC_DIR . '/images/medicine/' . \floor( $m_id / 1000 );
        // $prevFiles = array_flip( Functions::getPicUrls( $m_id ) );
        if ( ! is_dir($upload_folder)) {
            @mkdir($upload_folder, 0755, true);
        }
        if( $images && is_array( $images ) ){
            $imgArray = [];
            foreach ( $images as &$file ) {
                if( ! $file || ! is_array( $file ) ){
                    continue;
                }
                if( 0 === strpos( $file['src'], 'http') ) {    
                    array_push( $imgArray , [$m_id . '-'. $file['indx'] => $file['src']]);                
                    // unset( $prevFiles[ $file['src'] ] );                    
                } else {
                    $file['src'] = explode( '?', $file['src'] )[0];
                    $image_path = str_replace( STATIC_URL, STATIC_DIR, $file['src'] );
                    $prev_name = basename( $image_path );
                    $mime = @mime_content_type($file['src']);
                    if( ! $mime ){
                        continue;
                    }
                    $ext = explode('/', $mime )[1];
                    if( ! $ext || ! in_array( strtolower($ext), [ 'jpg', 'jpeg' ] ) ){
                        continue;
                    }
                    $ext = 'jpg';

                    $name = $file['indx'] ? $m_id . '-'. $file['indx'] . '.' . $ext : $m_id . '.'. $ext;
                    $new_file = \sprintf( '%s/%s', $upload_folder, $name );

                    if( file_exists( $new_file ) ){
                        rename( $new_file, $new_file . '.tmp');                        
                    }

                    //$imgstring = trim( str_replace("data:{$mime};base64,", "", $file['src'] ) );
                    $imgstring = trim( explode( ',', $file['src'] )[1] );
                    $imgstring = str_replace( ' ', '+', $imgstring );
                    if( strlen( $imgstring ) > 10 * 1024 * 1024 ){
                        continue;
                    }
                    if($prev_name !== $name){
                        if( file_exists( $new_file ) ){
                            rename( $new_file, $new_file . '.tmp');                        
                        }
                        if( file_exists( \sprintf( '%s/%s.tmp', $upload_folder, $prev_name ) ) ){
                            rename( \sprintf( '%s/%s.tmp', $upload_folder, $prev_name ), \sprintf( '%s/%s', $upload_folder, $name ) );
                        } else if( file_exists( \sprintf( '%s/%s', $upload_folder, $prev_name )) ){
                            rename( \sprintf( '%s/%s', $upload_folder, $prev_name ), \sprintf( '%s/%s', $upload_folder, $name ) );
                        }
                    }
                    if( file_put_contents( $new_file, base64_decode( $imgstring ) ) ) {
                        // Set correct file permissions.
                        $stat  = stat( dirname( $new_file ) );
                        $perms = $stat['mode'] & 0000666;
                        @chmod( $new_file, $perms );
                        $file['src'] = str_replace( STATIC_DIR,  STATIC_URL, $new_file );
                    }
                    
                    array_push( $imgArray , [$m_id . '-'. $file['indx'] => $file['src']]);
                    array_flip($imgArray);
                }
            }
            $query = DB::db()->prepare("SELECT meta_id FROM `t_medicine_meta` WHERE m_id = ? AND meta_key = ?");
            $query->execute([$m_id, 'images']);
            $meta = $query->fetch();
            if($meta == null){
                Meta::insert( 'medicine', $m_id, ['images' => $imgArray] );
            }else{
                Meta::set( 'medicine', $m_id, 'images' , $imgArray );
            }
            unset( $file );
        } 
        // if( $prevFiles ){            
        //     foreach ( array_flip($prevFiles) as $url ) {
        //         $url = explode( '?', $url )[0];
        //         $path = \str_replace( STATIC_URL, STATIC_DIR, $url );
        //         @unlink( $path );
        //     }
        // }
        // if( $images && is_array( $images ) ){
        //     foreach ( $images as $file ) {
        //         if( ! $file || ! is_array( $file ) ){
        //             continue;
        //         }
        //         if( 0 === strpos( $file['src'], 'http')   ){
        //             // unset( $prevFiles[ $file['src'] ] );
        //             $file['src'] = explode( '?', $file['src'] )[0];
        //             $image_path = str_replace( STATIC_URL, STATIC_DIR, $file['src'] );
        //             $prev_name = basename( $image_path );                    
        //             $mime = @mime_content_type($image_path);

        //             if( 0 !== strpos( $prev_name, $m_id . '.' ) && 0 !== strpos( $prev_name, $m_id . '-' ) ){
        //                 continue;
        //             }
                    
        //             if( ! $mime ){
        //                 continue;
        //             }
        //             $ext = explode('/', $mime )[1];
        //             if( ! $ext || ! in_array( strtolower($ext), [ 'jpg', 'jpeg' ] ) ){
        //                 continue;
        //             }
        //             $ext = 'jpg';

        //             $name = $file['indx'] ? $m_id . '-'. $file['indx'] . '.' . $ext : $m_id . '.'. $ext;
        //             $new_file = \sprintf( '%s/%s', $upload_folder, $name );                
        //             if($prev_name !== $name){
        //                 if( file_exists( $new_file ) ){
        //                     rename( $new_file, $new_file . '.tmp');                        
        //                 }
        //                 if( file_exists( \sprintf( '%s/%s.tmp', $upload_folder, $prev_name ) ) ){
        //                     rename( \sprintf( '%s/%s.tmp', $upload_folder, $prev_name ), \sprintf( '%s/%s', $upload_folder, $name ) );
        //                 } else if( file_exists( \sprintf( '%s/%s', $upload_folder, $prev_name )) ){
        //                     rename( \sprintf( '%s/%s', $upload_folder, $prev_name ), \sprintf( '%s/%s', $upload_folder, $name ) );
        //                 }
        //             }


        //         }
        //     }
        // }
        // clearstatcache();

        // foreach ( [0,1,2,3,4] as $number ) {
        //     $path = \sprintf( '%s/%s', $upload_folder, $m_id );
        //     if( $number ){
        //         $path .= "-{$number}";
        //     }
        //     $path .= '.jpg.tmp';
        //     if ( \file_exists( $path ) ) {
        //         unlink($path);
        //     }
        // }


    }
    // public static function modifyMedicineImages( $m_id, $images ){
    //     if( ! $m_id || ! is_array( $images ) ){
    //         return false;
    //     }
    //     $upload_folder = STATIC_DIR . '/images/medicine/' . \floor( $m_id / 1000 );
    //     $prevFiles = array_flip( Functions::getPicUrls( $m_id ) );
    //     if ( ! is_dir($upload_folder)) {
    //         @mkdir($upload_folder, 0755, true);
    //     }
    //     if( $images && is_array( $images ) ){
    //         foreach ( $images as &$file ) {
    //             if( ! $file || ! is_array( $file ) ){
    //                 continue;
    //             }
    //             if( 0 === strpos( $file['src'], 'http') ) {                    
    //                 unset( $prevFiles[ $file['src'] ] );                    
    //             } else {
    //                 $mime = @mime_content_type($file['src']);
    //                 if( ! $mime ){
    //                     continue;
    //                 }
    //                 $ext = explode('/', $mime )[1];
    //                 if( ! $ext || ! in_array( strtolower($ext), [ 'jpg', 'jpeg' ] ) ){
    //                     continue;
    //                 }
    //                 $ext = 'jpg';

    //                 $name = $file['indx'] ? $m_id . '-'. $file['indx'] . '.' . $ext : $m_id . '.'. $ext;
    //                 $new_file = \sprintf( '%s/%s', $upload_folder, $name );

    //                 if( file_exists( $new_file ) ){
    //                     rename( $new_file, $new_file . '.tmp');                        
    //                 }

    //                 //$imgstring = trim( str_replace("data:{$mime};base64,", "", $file['src'] ) );
    //                 $imgstring = trim( explode( ',', $file['src'] )[1] );
    //                 $imgstring = str_replace( ' ', '+', $imgstring );
    //                 if( strlen( $imgstring ) > 10 * 1024 * 1024 ){
    //                     continue;
    //                 }
    //                 if( file_put_contents( $new_file, base64_decode( $imgstring ) ) ) {
    //                     // Set correct file permissions.
    //                     $stat  = stat( dirname( $new_file ) );
    //                     $perms = $stat['mode'] & 0000666;
    //                     @chmod( $new_file, $perms );
    //                     $file['src'] = str_replace( STATIC_DIR,  STATIC_URL, $new_file );
    //                 }
    //             }
    //         }
    //         unset( $file );
    //     } 
    //     if( $prevFiles ){            
    //         foreach ( array_flip($prevFiles) as $url ) {
    //             $url = explode( '?', $url )[0];
    //             $path = \str_replace( STATIC_URL, STATIC_DIR, $url );
    //             @unlink( $path );
    //         }
    //     }
    //     if( $images && is_array( $images ) ){
    //         foreach ( $images as $file ) {
    //             if( ! $file || ! is_array( $file ) ){
    //                 continue;
    //             }
    //             if( 0 === strpos( $file['src'], 'http')   ){
    //                 // unset( $prevFiles[ $file['src'] ] );
    //                 $file['src'] = explode( '?', $file['src'] )[0];
    //                 $image_path = str_replace( STATIC_URL, STATIC_DIR, $file['src'] );
    //                 $prev_name = basename( $image_path );                    
    //                 $mime = @mime_content_type($image_path);

    //                 if( 0 !== strpos( $prev_name, $m_id . '.' ) && 0 !== strpos( $prev_name, $m_id . '-' ) ){
    //                     continue;
    //                 }
                    
    //                 if( ! $mime ){
    //                     continue;
    //                 }
    //                 $ext = explode('/', $mime )[1];
    //                 if( ! $ext || ! in_array( strtolower($ext), [ 'jpg', 'jpeg' ] ) ){
    //                     continue;
    //                 }
    //                 $ext = 'jpg';

    //                 $name = $file['indx'] ? $m_id . '-'. $file['indx'] . '.' . $ext : $m_id . '.'. $ext;
    //                 $new_file = \sprintf( '%s/%s', $upload_folder, $name );                
    //                 if($prev_name !== $name){
    //                     if( file_exists( $new_file ) ){
    //                         rename( $new_file, $new_file . '.tmp');                        
    //                     }
    //                     if( file_exists( \sprintf( '%s/%s.tmp', $upload_folder, $prev_name ) ) ){
    //                         rename( \sprintf( '%s/%s.tmp', $upload_folder, $prev_name ), \sprintf( '%s/%s', $upload_folder, $name ) );
    //                     } else if( file_exists( \sprintf( '%s/%s', $upload_folder, $prev_name )) ){
    //                         rename( \sprintf( '%s/%s', $upload_folder, $prev_name ), \sprintf( '%s/%s', $upload_folder, $name ) );
    //                     }
    //                 }


    //             }
    //         }
    //     }
    //     clearstatcache();

    //     foreach ( [0,1,2,3,4] as $number ) {
    //         $path = \sprintf( '%s/%s', $upload_folder, $m_id );
    //         if( $number ){
    //             $path .= "-{$number}";
    //         }
    //         $path .= '.jpg.tmp';
    //         if ( \file_exists( $path ) ) {
    //             unlink($path);
    //         }
    //     }


    // }

    public static function modifyLedgerFiles( $l_id, $ledgerFIles ){
        if( ! $l_id || ! is_array( $ledgerFIles ) ){
            return false;
        }
        $upload_folder = STATIC_DIR . '/ledger/' . \floor( $l_id / 1000 );
        if ( ! is_dir($upload_folder)) {
            @mkdir($upload_folder, 0755, true);
        }
        $prevFiles = array_flip( Functions::getLedgerFiles( $l_id ) );
        if( $ledgerFIles && is_array( $ledgerFIles ) ){
            foreach ( $ledgerFIles as $file ) {
                if( ! $file || ! is_array( $file ) ){
                    continue;
                }
                if( 0 === strpos( $file['src'], 'http') ){
                    unset( $prevFiles[ $file['src'] ] );
                } else {
                    $mime = @mime_content_type($file['src']);
                    if( ! $mime ){
                        continue;
                    }
                    $ext = explode('/', $mime )[1];
                    if( ! $ext || ! in_array( strtolower($ext), ['jpg', 'jpeg', 'png', 'gif', 'pdf'] ) ){
                        continue;
                    }
                    $name = preg_replace('/[^a-zA-Z0-9\-\._]/','-', $file['title']);
                    $name = explode( '.', $name )[0];
                    $name = trim( preg_replace('/-+/', '-', $name ), '-' ) ;
                    $new_file = \sprintf( '%s/%s-%s-%s.%s', $upload_folder, $l_id, $name, Functions::randToken( 'alnumlc', 8 ), $ext );

                    while( file_exists( $new_file ) ){
                        $new_file = \sprintf( '%s/%s-%s-%s.%s', $upload_folder, $l_id, $name, Functions::randToken( 'alnumlc', 8 ), $ext );
                    }

                    //$imgstring = trim( str_replace("data:{$mime};base64,", "", $file['src'] ) );
                    $imgstring = trim( explode( ',', $file['src'] )[1] );
                    $imgstring = str_replace( ' ', '+', $imgstring );
                    if( strlen( $imgstring ) > 10 * 1024 * 1024 ){
                        continue;
                    }
                    if( file_put_contents( $new_file, base64_decode( $imgstring ) ) ) {
                        // Set correct file permissions.
                        $stat  = stat( dirname( $new_file ) );
                        $perms = $stat['mode'] & 0000666;
                        @chmod( $new_file, $perms );
                    }
                }
            }
        }
        if( $prevFiles ){
            foreach ( array_flip($prevFiles) as $url ) {
                $path = \str_replace( STATIC_URL, STATIC_DIR, $url );
                @unlink( $path );
            }
        }
        return true;
    }

    public static function getLedgerFiles( $l_id, $path_or_url = 'url' ){
        $dir = STATIC_DIR . '/ledger/' . \floor( $l_id / 1000 );
        $array = glob( $dir . '/' . $l_id . "-*.*" );
        $value = [];
        foreach ( $array as $fullPath ) {
            if( 'path' == $path_or_url ){
                $value[] =  $fullPath;
            } else {
                $value[] = \str_replace( STATIC_DIR, STATIC_URL, $fullPath );
            }
        }
        return $value;
    }

    public static function qtyText( $qty, $medicine ) {
        if( !$medicine ){
          return '';
        }
        if( $medicine['form'] == $medicine['unit'] ) {
          $s = ( $qty === 1 ) ? '' : 's';
          return $qty . ' ' . $medicine['unit'] . $s;
        }
        if( $medicine['unit'] == 10 . ' ' . $medicine['form'] . 's' ) {
          return $qty*10 . ' ' . $medicine['form'] . 's';
        }
    
        return $qty . 'x' . $medicine['unit'];
    }

    public static function getLocations(){
        
        if ( $cache_data = Cache::instance()->get( 'locations' ) ){
            return $cache_data;
        }
        $query = DB::db()->prepare( 'SELECT * FROM t_locations ORDER BY l_division, l_district, l_area' );
        $query->execute();
        $data = [];
        while( $l = $query->fetch() ){
            $data[ $l['l_division'] ][ $l['l_district'] ][ $l['l_area'] ] = [
                'l_de_id' => $l['l_de_id' ],
                'l_postcode' => $l['l_postcode' ],
            ];
        }
        Cache::instance()->set( 'locations', $data );

        return $data;
    }

    public static function getDivisions(){

        $locations = Functions::getLocations();
        return array_unique( array_filter( array_keys( $locations ) ) );
    }

    public static function getDistricts( $division ){
        if( !$division ){
            return [];
        }

        $locations = Functions::getLocations();
        if( ! isset( $locations[ $division ] ) ){
            return [];
        }

        return array_unique( array_filter( array_keys( $locations[ $division ] ) ) );
    }

    public static function getAreas( $division, $district ){
        if( !$division || !$district ){
            return [];
        }

        $locations = Functions::getLocations();
        if( ! isset( $locations[ $division ] ) || ! isset( $locations[ $division ][ $district ] ) ){
            return [];
        }

        return array_unique( array_filter( array_keys( $locations[ $division ][ $district ] ) ) );
    }

    public static function getAddressByPostcode( $post_code, $map_area = '' ){
        if( !$post_code || !is_numeric( $post_code ) ){
            return [];
        }
        $return = [];
        $locations = Functions::getLocations();

        foreach ( $locations as $division => $v1 ) {
            foreach ( $v1 as $district => $v2 ) {
                foreach ($v2 as $area => $v3 ) {
                    if( !empty( $v3 ) && !empty( $v3['l_postcode'] ) && $post_code == $v3['l_postcode'] ){
                        $return = [
                            'division' => $division,
                            'district' => $district,
                            'area' => $area,
                        ];
                        if( $area == $map_area ){
                            break 3;
                        }
                    }
                }
            }
        }
        return $return;
    }
}