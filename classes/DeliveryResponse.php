<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate};
use GuzzleHttp\Client;

class DeliveryResponse {

    function __construct() {
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        //For Offline orders pharmacy is also delivery man
        if ( !\in_array( $user->u_role, [ 'delivery', 'pharmacy'] ) ) {
            Response::instance()->sendMessage( 'You are not a delivery man.' );
        }
    }

    function orders() {
        $status = isset( $_GET['status'] ) ? $_GET['status'] : '';
        $page = isset( $_GET['page'] ) ? (int)$_GET['page'] : '';
        $lat = isset( $_GET['lat'] ) ? $_GET['lat'] : '';
        $long = isset( $_GET['long'] ) ? $_GET['long'] : '';
        $per_page = 10;
        $limit    = $per_page * ( $page - 1 );

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_orders WHERE o_de_id = ?', Auth::id() );
        if ( $status ) {
            $db->add( ' AND o_status = ?', $status );
        }
        $db->add( ' AND o_i_status IN(?,?)', 'confirmed', 'paid' );

        if( 'delivered' == $status ) {
            $db->add( ' ORDER BY o_delivered DESC' );
        } else {
            $db->add( ' ORDER BY o_id ASC' );
        }

        $db->add( ' LIMIT ?, ?', $limit, $per_page );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Order');

        while( $order = $query->fetch() ){
            $data = $order->toArray();
            $data['paymentStatus'] = ( 'paid' === $order->o_i_status || 'paid' === $order->getMeta( 'paymentStatus' ) ) ? 'paid' : '';
            $data['o_i_note'] = (string)$order->getMeta('o_i_note');
            Response::instance()->appendData( '', $data );
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Orders Found' );
        } else {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function pendingCollection() {
        $query = DB::db()->prepare( 'SELECT o_ph_id, SUM(o_total) as amount FROM t_orders WHERE o_de_id = ? AND o_status = ? AND o_i_status = ? GROUP BY o_ph_id' );
        $query->execute( [ Auth::id(), 'delivered', 'confirmed' ] );

        while( $data = $query->fetch() ){
            if ( !empty($data['o_ph_id']) && ( $user = User::getUser( $data['o_ph_id'] ) ) ) {
                $data['o_ph_name'] = $user->u_name;
                Response::instance()->appendData( '', $data );
            }
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Collections Found' );
        } else {
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function sendCollection(){

        $ph_id = isset( $_POST['o_ph_id'] ) ? (int)$_POST['o_ph_id'] : 0;
        $amount = isset( $_POST['amount'] ) ? round( $_POST['amount'], 2 ) : 0.00;

        if( ! $ph_id || ! $amount ) {
            Response::instance()->sendMessage( 'Invalid pharmacy id or amount' );
        }

        $query = DB::db()->prepare( 'SELECT o_id, o_total FROM t_orders WHERE o_de_id = ? AND o_ph_id = ? AND o_status = ? AND o_i_status = ?' );
        $query->execute( [ Auth::id(), $ph_id, 'delivered', 'confirmed' ] );
        $o_ids = [];
        $total = 0;
        while( $order = $query->fetch() ){
            $o_ids[] = $order['o_id'];
            $total += $order['o_total'];
        }

        if( \round( $amount ) != \round( $total ) ) {
            Response::instance()->sendMessage( 'Amount Mismatch. Contact customer care.' );
        }
        $s_price_total = 0;
        if( $o_ids ) {
            $in  = str_repeat('?,', count($o_ids) - 1) . '?';
            $query = DB::db()->prepare( "SELECT SUM(s_price*m_qty) FROM t_o_medicines WHERE om_status = ? AND o_id IN ($in)" );
            $query->execute( \array_merge( ['available'], $o_ids ) );
            $s_price_total = $query->fetchColumn();
        }

        $data_array = [
            'co_fid' => Auth::id(),
            'co_tid' => $ph_id,
            'o_ids' => \json_encode( $o_ids ),
            'co_amount' => \round( $total, 2 ),
            'co_s_amount' => \round( $s_price_total, 2 ),
            'co_created' => \date( 'Y-m-d H:i:s' ),
        ];

        $id = DB::instance()->insert( 't_collections', $data_array );
        
        if( $id ) {
            if( $o_ids ) {
                $in  = str_repeat('?,', count($o_ids) - 1) . '?';
                $query = DB::db()->prepare( "UPDATE t_orders SET o_i_status = ? WHERE o_id IN ($in)" );
                $query->execute( \array_merge(['paid'], $o_ids) );

                foreach ( $o_ids as $o_id ) {
                    Cache::instance()->delete( $o_id, 'order' );
                }
            }
            Response::instance()->sendMessage( 'Collection done', 'success' );
        }

        Response::instance()->sendMessage( 'Something wrong. Plase try again.' );
    }

    function statusTo( $o_id, $status ){
        if ( !$o_id || !$status || !\in_array( $status, ['delivering', 'delivered'] ) ) {
            Response::instance()->sendMessage( 'No orders found.' );
        }
        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }
        if( Auth::id() != $order->o_de_id ){
            Response::instance()->sendMessage( 'You are not delivery man for this order.' );
        }
        $medicines = $order->medicines;
        if( 'delivered' == $status ) {
            foreach ( $medicines as $key => $value ) {
                if( $value['qty'] && 'available' != $value['om_status'] ){
                    Response::instance()->sendMessage( 'All medicines price are not set. Contact Pharmacy and tell them to input all medicines price.' );
                }
            }
        }

        if( $order->update( [ 'o_status' => $status ] ) ){
            $data = $order->toArray();
            $data['prescriptions'] = $order->prescriptions;
            $data['o_data'] = (array)$order->getMeta( 'o_data' );
            $data['o_data']['medicines'] = $medicines;
            Response::instance()->sendData( $data, 'success' );
        }
        Response::instance()->sendMessage( 'Something wrong. Please try again.' );
    }

}