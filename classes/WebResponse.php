<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate};
use GuzzleHttp\Client;

class WebResponse {

    function __construct() {
        \header("Access-Control-Allow-Origin: *");
    }


    public function carousel() {
        $carousel = [];
        foreach ( glob( STATIC_DIR . '/images/webCarousel/*.{jpg,jpeg,png,gif}', GLOB_BRACE ) as $value ) {
            $carousel[] = \str_replace( STATIC_DIR, STATIC_URL, $value ) . '?v=' . @\filemtime($value) ?: 1;
        }
        Response::instance()->sendData( $carousel, 'success');
    }

}