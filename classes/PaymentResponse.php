<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate};
use GuzzleHttp\Client;

class PaymentResponse {

    function __construct() {
        //\header("Access-Control-Allow-Origin: *");
        header('Content-Type: text/html; charset=utf-8');
    }

    public function home($o_id, $o_secret){
        if( ! MAIN ){
            $this->output( 'Error', 'This is a test site, can not pay here.' );
        }

        //$this->output( 'Error', 'Due to technical difficulty we cannot accept your online payment now. Please try again later or pay Cash On Delivery to our delivery man.' );

        $this->proceed( $o_id, $o_secret );
        exit;

        if( ! $o_id || ! $o_secret ){
            $this->output( 'Error', 'No id or secret provided' );
            return;
        }
        $order = Order::getOrder( $o_id );
        if( ! $order || $o_secret != $order->getMeta( 'o_secret' ) ){
            $this->output( 'Error', 'No Order found' );
            return;
        }
        ob_start();
        ?>
        <div>
            <div>
                <table>
                    <tr>
                        <td>Order ID</td>
                        <td><?php echo $order->o_id; ?></td>
                    </tr>
                    <tr>
                        <td>Customer Name</td>
                        <td><?php echo $order->u_name; ?></td>
                    </tr>
                </table>
            </div>
            <div>
                <form method="POST">
                    <input type="hidden" name="o_id" value="<?php echo $order->o_id; ?>" />
                    <input type="hidden" name="o_secret" value="<?php echo $o_secret; ?>" />
                    <button type="submit">Proceed</button>
                </form>
            </div>
        </div>
        <?php

        $this->output( 'Payment', '', ob_get_clean() );

    }

    public function proceed( $o_id, $o_secret ){

        if( ! $o_id || ! $o_secret ){
            $this->output( 'Error', 'No id or secret provided' );
        }
        $order = Order::getOrder( $o_id );
        if( ! $order || $o_secret != $order->getMeta( 'o_secret' ) ){
            $this->output( 'Error', 'No Order found' );
        }
        if( ! \in_array( $order->o_status, [ 'confirmed', 'delivering', 'delivered' ] ) ){
            $this->output( 'Error', 'You cannot pay for this order.' );
        }
        if( 'paid' === $order->o_i_status || 'paid' === $order->getMeta( 'paymentStatus' ) ){
            $this->output( 'Success', 'You have already paid for this order.' );
        }
        $txnNo = $order->getMeta( 'TxnNo' );
        if( ! $txnNo ){
            $txnNo = 'Txn' . $order->o_id . \date( 'YmdHis' );
            $order->setMeta( 'TxnNo', $txnNo );
        }
        $address = $order->o_gps_address;
        if( $order->o_gps_address && $order->o_address ){
            $address .= "\n";
        }
        $address .= $order->o_address;

        $items = [];

        $i = 1;
        foreach ( $order->medicines as $medicine ) {
            $items[] = implode( ',', [
                $i++,
                $medicine['name'] . '-' . $medicine['strength'],
                Functions::qtyText( $medicine['qty'], $medicine),
                $medicine['d_price'],
            ]);
        }

        $urlparamForHash = http_build_query( array(
            'mcnt_AccessCode' => FP_ACCESS_CODE,
            'mcnt_TxnNo' => $txnNo,
            'mcnt_ShortName' => 'Arogga',
            'mcnt_OrderNo' => $order->o_id,
            'mcnt_ShopId' => '256',
            'mcnt_Amount' => $order->o_total,
            'mcnt_Currency' => 'BDT'
        ));

        $secret = strtoupper( FP_SECRET_KEY );
        $hashinput = hash_hmac('SHA256',$urlparamForHash,$secret);


        $domain = 'arogga.com'; // or Manually put your domain name
        $ip = $_SERVER["SERVER_ADDR"];//domain ip
        $urlparam =array(
            'mcnt_TxnNo' => $txnNo,
            'mcnt_ShortName' => 'Arogga',
            'mcnt_OrderNo' => $order->o_id,
            'mcnt_ShopId' => '256',
            'mcnt_Amount' => $order->o_total,
            'mcnt_Currency' => 'BDT',
            'cust_InvoiceTo' => $order->u_name,
            'cust_CustomerServiceName' => 'E-commarce',//must
            'cust_CustomerName' => $order->u_name,//must
            'cust_CustomerEmail' => 'info@arogga.com',//must //we do not track
            'cust_CustomerAddress' => $address,
            'cust_CustomerContact' => $order->u_mobile,//must
            'cust_CustomerGender' => 'N/A', //we do not track
            'cust_CustomerCity' => 'Dhaka',//must
            'cust_CustomerState' => 'Dhaka',
            'cust_CustomerPostcode' => '1000', //we do not track
            'cust_CustomerCountry' => 'Bangladesh',
            'cust_Billingaddress' => $address,//must if not put ‘N/A’
            'cust_ShippingAddress' => $address,
            'cust_orderitems' => implode( '|', $items ),//must
            'GW' => '',//optional
            'CardType' => '', //optional
            'success_url' => SITE_URL . '/payment/v1/success/?o_id=' . $order->o_id,//must
            'cancel_url' => SITE_URL . '/payment/v1/cancel/?o_id=' . $order->o_id,//must
            'fail_url' => SITE_URL . '/payment/v1/fail/?o_id=' . $order->o_id,//must
            'emi_amout_per_month' =>'',//optional
            'emi_duration' => '',//optional
            'merchentdomainname' => $domain, // must
            'merchentip' => $ip,
            'mcnt_SecureHashValue' => $hashinput
        );

        $client = new Client();
        $res = $client->post( 'https://payment.fosterpayments.com.bd/fosterpayments/paymentrequest.php', ['json' => $urlparam ]);
        if( 200 !== $res->getStatusCode() ){
            $this->output( 'Error', 'Something wrong, Please try again.' );
        }
        $body = Functions::maybeJsonDecode( $res->getBody()->getContents() );
        if( ! $body || ! \is_array( $body ) || 200 !== (int)$body['status'] ){
            $this->output( 'Error', 'Something wrong, Please try again.' );
        }

        $data = $body['data'];
        //var_dump( $data );
        $redirect_url = $data['redirect_url'];
        $payment_id = $data['payment_id'];
        $url = $redirect_url . "?payment_id=" . $payment_id;

        header("Location:" . $url); 
        exit; 
    }

    public function success(){
        $o_id = !empty( $_GET['o_id'] ) ? (int)$_GET['o_id'] : 0;

        if( ! $o_id ){
            $this->output( 'Error', 'No id provided' );
        }
        $order = Order::getOrder( $o_id );
        if( ! $order ){
            $this->output( 'Error', 'No Order found' );
        }

        if( ! \in_array( $order->o_status, [ 'confirmed', 'delivering', 'delivered' ] ) ){
            $this->output( 'Error', 'You cannot pay for this order.' );
        }
        if( 'paid' === $order->getMeta( 'paymentStatus' ) ){
            $this->output( 'Success', 'You have already paid for this order.' );
        }

        $fp_MerchantTxnNo = isset( $_POST['MerchantTxnNo'] ) ? $_POST['MerchantTxnNo'] : '';
        $fp_OrderNo = isset( $_POST['OrderNo'] ) ? (int)$_POST['OrderNo'] : 0;
        $fp_hashkey = isset( $_POST['hashkey'] ) ? $_POST['hashkey'] : '';
        $fp_TxnResponse = isset( $_POST['TxnResponse'] ) ? (int)$_POST['TxnResponse'] : 0;
        $fp_TxnAmount = isset( $_POST['TxnAmount'] ) ? (int)$_POST['TxnAmount'] : 0;
        $fp_Currency = isset( $_POST['Currency'] ) ? $_POST['Currency'] : '';
        $fp_gateway_fee = isset( $_POST['servicecharge'] ) ? round( $_POST['servicecharge'], 2 ) : 0.00;

        if( $fp_OrderNo !== $order->o_id || $fp_MerchantTxnNo !== $order->getMeta( 'TxnNo' ) || $fp_hashkey  !== md5( $fp_TxnResponse . $fp_MerchantTxnNo . FP_SECRET_KEY ) ){
            $this->output( 'Error', 'No Orders found' );
        }

        $meta = $order->getMeta( 'paymentResponse' );
        if( ! is_array( $meta ) ){
            $meta = [];
        }
        $post = $_POST;
        $post['log_time'] = \date( 'Y-m-d H:i:s' );
        $post['log_type'] = 'success';
        $meta[] = $post;
        $order->setMeta( 'paymentResponse', $meta );

        if( 2 !== $fp_TxnResponse ){
            $this->output( 'Error', "Payment didn't succeed. Please Contact our support." );
        }

        if( 'BDT' !== $fp_Currency || round( $fp_TxnAmount ) !== round( $order->o_total ) ){
            $this->output( 'Error', "Payment amount mismatched." );
        }

        $client = new Client();
        $res = $client->get( 'https://payment.fosterpayments.com.bd/fosterpayments/TransactionStatus/txstatus.php?mcnt_TxnNo=' . $fp_MerchantTxnNo . '&mcnt_SecureHashValue=' . md5( FP_SECRET_KEY . $fp_MerchantTxnNo ) );
        if( 200 !== $res->getStatusCode() ){
            $this->output( 'Error', 'Something wrong, Please try again.' );
        }
        $body = Functions::maybeJsonDecode( $res->getBody()->getContents() );
        if( ! $body || ! \is_array( $body ) ){
            $this->output( 'Error', 'Something wrong, Please try again.' );
        }
        $_body = reset( $body );
        $fp_st_MerchantTxnNo = isset( $_body['MerchantTxnNo'] ) ? $_body['MerchantTxnNo'] : '';
        $fp_st_OrderNo = isset( $_body['OrderNo'] ) ? (int)$_body['OrderNo'] : 0;
        $fp_st_hashkey = isset( $_body['hashkey'] ) ? $_body['hashkey'] : '';
        $fp_st_TxnResponse = isset( $_body['TxnResponse'] ) ? (int)$_body['TxnResponse'] : '';
        $fp_st_TxnAmount = isset( $_body['TxnAmount'] ) ? (int)$_body['TxnAmount'] : 0;
        $fp_st_Currency = isset( $_body['Currency'] ) ? $_body['Currency'] : '';

        if( $fp_st_MerchantTxnNo != $fp_MerchantTxnNo || $fp_st_OrderNo != $fp_OrderNo || $fp_st_hashkey != $fp_hashkey || $fp_st_TxnResponse != $fp_TxnResponse || $fp_st_TxnAmount != $fp_TxnAmount || $fp_st_Currency != $fp_Currency ){
            $this->output( 'Error', 'Verification failed. Please contact support.' );
        }
        
        $updateData = [
            'o_payment_method' => 'fosterPayment',
        ];
        if( 'delivered' == $order->o_status && 'confirmed' == $order->o_i_status ){
            $updateData['o_i_status'] = 'paid';
        }
        $order->update( $updateData );
        $order->setMeta( 'paymentStatus', 'paid' );
        $order->setMeta( 'paymentGatewayFee', $fp_gateway_fee );

        $this->output( 'Success', 'Payment success' );
    }

    public function cancel(){
        $this->cancel_fail( 'cancel' );
    }

    public function fail(){
        $this->cancel_fail( 'fail' );
    }

    public function cancel_fail( $type ){
        $o_id = !empty( $_GET['o_id'] ) ? (int)$_GET['o_id'] : 0;

        if( ! $o_id ){
            $this->output( 'Error', 'No id provided' );
        }
        $order = Order::getOrder( $o_id );
        if( ! $order ){
            $this->output( 'Error', 'No Order found' );
        }

        if( ! \in_array( $order->o_status, [ 'confirmed', 'delivering', 'delivered' ] ) ){
            $this->output( 'Error', 'You cannot pay for this order.' );
        }
        if( 'paid' === $order->getMeta( 'paymentStatus' ) ){
            $this->output( 'Success', 'You have already paid for this order.' );
        }
        $try_again = '<a href="' . SITE_URL . '/payment/v1/' . $order->o_id . '/' . $order->getMeta( 'o_secret' ) . '/">Try Again</a>';

        if( 'POST' != $_SERVER['REQUEST_METHOD'] ){
            if( 'fail' == $type ){
                $this->output( 'Failed', 'Payment failed', $try_again );
            } elseif( 'cancel' == $type ){
                $this->output( 'Cancel', 'Payment cancelled', $try_again );
            }
        }


        $fp_MerchantTxnNo = isset( $_POST['MerchantTxnNo'] ) ? $_POST['MerchantTxnNo'] : '';
        $fp_OrderNo = isset( $_POST['OrderNo'] ) ? (int)$_POST['OrderNo'] : 0;
        $fp_hashkey = isset( $_POST['hashkey'] ) ? $_POST['hashkey'] : '';
        $fp_TxnResponse = isset( $_POST['TxnResponse'] ) ? (int)$_POST['TxnResponse'] : 0;

        if( $fp_OrderNo !== $order->o_id || $fp_MerchantTxnNo !== $order->getMeta( 'TxnNo' ) || $fp_hashkey  !== md5( $fp_TxnResponse . $fp_MerchantTxnNo . FP_SECRET_KEY ) ){
            $this->output( 'Error', 'No Orders found' );
        }

        $meta = $order->getMeta( 'paymentResponse' );
        if( ! is_array( $meta ) ){
            $meta = [];
        }
        $post = $_POST;
        $post['log_time'] = \date( 'Y-m-d H:i:s' );
        $post['log_type'] = $type;
        $meta[] = $post;
        $order->setMeta( 'paymentResponse', $meta );

        if( 'fail' == $type ){
            $this->output( 'Failed', 'Payment failed', $try_again );
        } elseif( 'cancel' == $type ){
            $this->output( 'Cancel', 'Payment cancelled', $try_again );
        }
    }

    public function ipn(){

        $fp_MerchantTxnNo = isset( $_POST['merchantTxnNo'] ) ? $_POST['merchantTxnNo'] : '';
        $fp_OrderNo = isset( $_POST['orderNo'] ) ? (int)$_POST['orderNo'] : 0;
        $fp_hashkey = isset( $_POST['hashkey'] ) ? $_POST['hashkey'] : '';

        $fp_TxnResponse = isset( $_POST['txnResponse'] ) ? (int)$_POST['txnResponse'] : '';
        $fp_TxnAmount = isset( $_POST['txnAmount'] ) ? (int)$_POST['txnAmount'] : 0;
        $fp_Currency = isset( $_POST['currency'] ) ? $_POST['currency'] : '';

        $order = Order::getOrder( $fp_OrderNo );
        if( ! $order ){
            exit;
        }

        if( $fp_OrderNo !== $order->o_id || $fp_MerchantTxnNo !== $order->getMeta( 'TxnNo' ) || $fp_hashkey  !== md5( $fp_TxnResponse . $fp_MerchantTxnNo . FP_SECRET_KEY ) ){
            exit;
        }

        if( 2 !== $fp_TxnResponse || 'BDT' !== $fp_Currency || round( $fp_TxnAmount ) !== round( $order->o_total ) ){
            exit;
        }
        if( 'paid' === $order->getMeta( 'paymentStatus' ) ){
            Response::instance()->sendMessage( 'Already updated.', 'success' );
        }

        $meta = $order->getMeta( 'paymentResponse' );
        if( ! is_array( $meta ) ){
            $meta = [];
        }
        $post = $_POST;
        $post['log_time'] = \date( 'Y-m-d H:i:s' );
        $post['log_type'] = 'ipn';
        $meta[] = $post;
        $order->setMeta( 'paymentResponse', $meta );

        $order->update( ['o_payment_method' => 'fosterPayment'] );
        $order->setMeta( 'paymentStatus', 'paid' );

        Response::instance()->sendMessage( 'IPN received and updated.', 'success' );
    }


    public function output( $title, $heading, $body = '' ){
        ?>
        <!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en-US">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="robots" content="noindex, nofollow">
            <title><?php echo $title . ' - Arogga'; ?></title>
        </head>
        <body>
            <div style="text-align: center;">
                <h3><?php echo $heading; ?></h3>
                <?php if( $body ){
                    echo "<div>$body</div>";
                } ?>
            </div>
        </body>
        </html>
        <?php
        exit;
    }

}