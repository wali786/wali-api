<?php

namespace OA;
use OA\Factory\Log;


class Route {
	/**
	 * List of roles and capabilities.
	 *
	 * @since 2.0.0
	 * @access public
	 * @var array
	 */
	public $routes;
	
	public $route_collector;
	public $route_dispatcher;

	
	private static $instance;

	/**
	 * Constructor
	 *
	 * @since 2.0.0
	 */
	public function __construct() {		
		try{
			
			$this->route_dispatcher = \FastRoute\cachedDispatcher(function(\FastRoute\RouteCollector $r) {
				$this->route_collector = $r;
				$this->addRoutes( $this->route_collector );
			}, [
				'cacheFile' => STATIC_DIR . '/froute.cache', /* required */
				'cacheDisabled' => !MAIN,     /* optional, enabled by default */
			]);
		} catch( \Exception $e  ){
			//echo $e;
			Response::instance()->sendMessage( 'ROUTE error.', 'error' );
			die;
		}
	}
	
	public static function instance() {
		if( ! self::$instance instanceof self ) {
			self::$instance = new self;
		}
		return self::$instance;
	}
	
	protected function addRoutes( $rc ) {

		$rc->post( '/v1/auth/sms/send/', [ '\OA\Auth', 'SMSSend' ] );
		$rc->post( '/v1/auth/sms/verify/', [ '\OA\Auth', 'SMSVerify' ] );
		$rc->post( '/v1/auth/logout/', [ '\OA\Auth', 'logout' ] );
		$rc->post( '/admin/v1/auth/sms/send/', [ '\OA\Auth', 'adminSMSSend' ] );
		$rc->post( '/admin/v1/auth/sms/verify/', [ '\OA\Auth', 'adminSMSVerify' ] );

		$rc->get( '/v1/data/initial/[{table}/{page:\d+}/]', [ '\OA\RouteResponse', 'dataInitial_v1' ] );
		$rc->get( '/v2/data/initial/[{table}/{page:\d+}/]', [ '\OA\RouteResponse', 'dataInitial_v2' ] );
		$rc->get( '/v1/data/check/{dbVersion:\d+}/', [ '\OA\RouteResponse', 'dataCheck' ] );
		$rc->get( '/v1/home/', [ '\OA\RouteResponse', 'home' ] );
		$rc->get( '/v2/home/', [ '\OA\RouteResponse', 'home_v2' ] );
		$rc->get( '/v1/medicines/[{search}/[{page:\d+}/]]', [ '\OA\RouteResponse', 'medicines' ] );
		$rc->get( '/v1/sameGeneric/{g_id:\d+}/{page:\d+}/', [ '\OA\RouteResponse', 'sameGeneric' ] );
		$rc->get( '/v1/medicine/{m_id:\d+}/', [ '\OA\RouteResponse', 'medicineSingle_v1' ] );
		$rc->get( '/v2/medicine/{m_id:\d+}/', [ '\OA\RouteResponse', 'medicineSingle_v2' ] );
		$rc->get( '/v1/medicine/price/{m_id:[0-9,]+}/', [ '\OA\RouteResponse', 'medicinePrice' ] );
		$rc->post( '/v1/medicine/suggest/', [ '\OA\RouteResponse', 'medicineSuggest' ] );

		$rc->post( '/v1/cart/details/', [ '\OA\RouteResponse', 'cartDetails' ] );
		$rc->post( '/v1/discount/check/', [ '\OA\RouteResponse', 'dicountCheck' ] );

		$rc->post( '/v1/order/add/', [ '\OA\RouteResponse', 'orderAdd' ] );
		$rc->get( '/v1/order/{o_id:\d+}/', [ '\OA\RouteResponse', 'orderSingle' ] );
		$rc->get( '/v1/invoice/{o_id:\d+}/{secret}/', [ '\OA\RouteResponse', 'invoice' ] );
		$rc->get( '/v1/orders/{status}/[{page:\d+}/]', [ '\OA\RouteResponse', 'orders' ] );
		$rc->get(    '/v1/cashBalance/', [ '\OA\RouteResponse', 'cashBalance' ] );
		$rc->get( '/v1/collections/', [ '\OA\RouteResponse', 'collections' ] );
		$rc->get( '/v1/location/', [ '\OA\RouteResponse', 'location' ] );
		$rc->get( '/v1/profile/', [ '\OA\RouteResponse', 'profile' ] );
		$rc->post( '/v1/profile/', [ '\OA\RouteResponse', 'profileUpdate' ] );
		$rc->get( '/v1/offers/', [ '\OA\RouteResponse', 'offers' ] );
		$rc->get( '/v1/faqsHeaders/', [ '\OA\RouteResponse', 'FAQsHeaders' ] );
		$rc->get( '/v1/faqs/{slug}/', [ '\OA\RouteResponse', 'FAQsReturn' ] );
		$rc->get( '/v1/locationData/', [ '\OA\RouteResponse', 'locationData' ] );
		$rc->post( '/v1/saveInternalNote/{o_id:\d+}/', [ '\OA\RouteResponse', 'saveInternalNote' ] );

		$rc->addGroup('/payment/v1', function ( $rc ) {
			$rc->get( '/{o_id:\d+}/{secret}/', [ '\OA\PaymentResponse', 'home' ] );
			$rc->post( '/{o_id:\d+}/{secret}/', [ '\OA\PaymentResponse', 'proceed' ] );
			$rc->post( '/success/', [ '\OA\PaymentResponse', 'success' ] );
			$rc->post( '/cancel/', [ '\OA\PaymentResponse', 'cancel' ] );
			$rc->post( '/fail/', [ '\OA\PaymentResponse', 'fail' ] );
			$rc->post( '/ipn/', [ '\OA\PaymentResponse', 'ipn' ] );
		});

		$rc->addGroup('/web/v1', function ( $rc ) {
			$rc->get( '/carousel/', [ '\OA\WebResponse', 'carousel' ] );
		});

		$rc->addGroup('/cron/v1', function ( $rc ) {
			$rc->get(    '/daily/', [ '\OA\CronResponse', 'daily' ] );
			$rc->get(    '/dumpLog/', [ '\OA\CronResponse', 'dumpLog' ] );
		});
		$rc->addGroup('/cache/v1', function ( $rc ) {
			$rc->get(    '/flush/{id:\d+}/', [ '\OA\CacheResponse', 'cacheFlush' ] );
			$rc->get(    '/stats/{id:\d+}/', [ '\OA\CacheResponse', 'cacheStats' ] );

			//$rc->get(    '/set/{key}/{value}/[{group}/]', [ '\OA\CacheResponse', 'set' ] );
			//$rc->get(    '/get/{key}/[{group}/]', [ '\OA\CacheResponse', 'get' ] );
			//$rc->get(    '/delete/{key}/[{group}/]', [ '\OA\CacheResponse', 'delete' ] );
		});
		$rc->addGroup('/delivery/v1', function ( $rc ) {
			$rc->get(    '/orders/', [ '\OA\DeliveryResponse', 'orders' ] );
			$rc->get(  '/pendingCollection/', [ '\OA\DeliveryResponse', 'pendingCollection' ] );
			$rc->post( '/sendCollection/', [ '\OA\DeliveryResponse', 'sendCollection' ] );
			$rc->get(  '/statusTo/{o_id:\d+}/{status}/', [ '\OA\DeliveryResponse', 'statusTo' ] );
		});
		$rc->addGroup('/pharmacy/v1', function ( $rc ) {
			$rc->get(    '/orders/', [ '\OA\PharmacyResponse', 'orders' ] );
			$rc->get(    '/pharmacyLater/', [ '\OA\PharmacyResponse', 'pharmacyLater' ] );
			$rc->post(    '/receivedCollection/{co_id:\d+}/', [ '\OA\PharmacyResponse', 'receivedCollection' ] );
			$rc->post(    '/supplierPrice/', [ '\OA\PharmacyResponse', 'supplierPrice' ] );
			$rc->post(  '/setOMStatus/', [ '\OA\PharmacyResponse', 'setOMStatus' ] );
			$rc->post(  '/setCombineStatus/', [ '\OA\PharmacyResponse', 'setCombineStatus' ] );
			$rc->post(  '/combineClearPacked/', [ '\OA\PharmacyResponse', 'combineClearPacked' ] );
			$rc->post(  '/setCombinePrice/', [ '\OA\PharmacyResponse', 'setCombinePrice' ] );
			$rc->post(  '/internalStatusTo/{o_id:\d+}/{status}/', [ '\OA\PharmacyResponse', 'internalStatusTo' ] );

			//For Offline orders pharmacy is also delivery man
			$rc->get(  '/pendingCollection/', [ '\OA\DeliveryResponse', 'pendingCollection' ] );
			$rc->post( '/sendCollection/', [ '\OA\DeliveryResponse', 'sendCollection' ] );
		});
		$rc->addGroup('/wholesale/v1', function ( $rc ) {
			$rc->get(    '/orders/', [ '\OA\WholesaleResponse', 'orders' ] );
			$rc->post(    '/notAvailable/{o_id:\d+}/{m_id:\d+}/', [ '\OA\WholesaleResponse', 'notAvailable' ] );
			$rc->post(    '/supplierPrice/', [ '\OA\WholesaleResponse', 'supplierPrice' ] );
			$rc->post(    '/pause/', [ '\OA\WholesaleResponse', 'pause' ] );
		});
		$rc->addGroup('/admin/v1', function ( $rc ) {
			$rc->get( '/allLocations/', [ '\OA\AdminResponse', 'allLocations' ] );

			$rc->addGroup('/medicines', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'medicines' ] );
				$rc->post(   '/', [ '\OA\AdminResponse', 'medicineCreate' ] );
				$rc->get(    '/{m_id:\d+}/', [ '\OA\AdminResponse', 'medicineSingle' ] );
				$rc->post(   '/{m_id:\d+}/', [ '\OA\AdminResponse', 'medicineUpdate' ] );
				$rc->delete( '/{m_id:\d+}/', [ '\OA\AdminResponse', 'medicineDelete' ] );
			});
			$rc->addGroup('/users', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'users' ] );
				$rc->post(   '/', [ '\OA\AdminResponse', 'userCreate' ] );
				$rc->get(    '/{u_id:\d+}/', [ '\OA\AdminResponse', 'userSingle' ] );
				$rc->post(   '/{u_id:\d+}/', [ '\OA\AdminResponse', 'userUpdate' ] );
				$rc->delete( '/{u_id:\d+}/', [ '\OA\AdminResponse', 'userDelete' ] );
			});
			$rc->addGroup('/orders', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'orders' ] );
				$rc->post(   '/', [ '\OA\AdminResponse', 'orderCreate' ] );
				$rc->get(    '/{o_id:\d+}/', [ '\OA\AdminResponse', 'orderSingle' ] );
				$rc->post(   '/{o_id:\d+}/', [ '\OA\AdminResponse', 'orderUpdate' ] );
				$rc->delete( '/{o_id:\d+}/', [ '\OA\AdminResponse', 'orderDelete' ] );
			});
			$rc->addGroup('/offlineOrders', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'offlineOrders' ] );
				$rc->post(   '/', [ '\OA\AdminResponse', 'offlineOrderCreate' ] );
				$rc->get(    '/{o_id:\d+}/', [ '\OA\AdminResponse', 'orderSingle' ] );
				$rc->post(   '/{o_id:\d+}/', [ '\OA\AdminResponse', 'offlineOrderUpdate' ] );
				$rc->delete( '/{o_id:\d+}/', [ '\OA\AdminResponse', 'orderDelete' ] );
			});
			$rc->addGroup('/orderMedicines', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'orderMedicines' ] );
				//$rc->post(   '/', [ '\OA\AdminResponse', 'orderCreate' ] );
				$rc->get(    '/{om_id:\d+}/', [ '\OA\AdminResponse', 'orderMedicineSingle' ] );
				$rc->post(   '/{om_id:\d+}/', [ '\OA\AdminResponse', 'orderMedicineUpdate' ] );
				$rc->delete( '/{om_id:\d+}/', [ '\OA\AdminResponse', 'orderMedicineDelete' ] );
			});
			$rc->addGroup('/laterMedicines', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'laterMedicines' ] );
			});
			$rc->addGroup('/inventory', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'inventory' ] );
				$rc->get(    '/{i_id:\d+}/', [ '\OA\AdminResponse', 'inventorySingle' ] );
				$rc->post(   '/{i_id:\d+}/', [ '\OA\AdminResponse', 'inventoryUpdate' ] );
				$rc->delete( '/{i_id:\d+}/', [ '\OA\AdminResponse', 'inventoryDelete' ] );
				$rc->get(    '/balance/', [ '\OA\AdminResponse', 'inventoryBalance' ] );
			});
			$rc->addGroup('/purchases', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'purchases' ] );
				$rc->post(   '/', [ '\OA\AdminResponse', 'purchaseCreate' ] );
				$rc->get(    '/{pu_id:\d+}/', [ '\OA\AdminResponse', 'purchaseSingle' ] );
				$rc->post(   '/{pu_id:\d+}/', [ '\OA\AdminResponse', 'purchaseUpdate' ] );
				$rc->delete( '/{pu_id:\d+}/', [ '\OA\AdminResponse', 'purchaseDelete' ] );
				$rc->get(    '/pendingTotal/', [ '\OA\AdminResponse', 'purchasesPendingTotal' ] );
				$rc->post(    '/sync/', [ '\OA\AdminResponse', 'purchasesSync' ] );
			});
			$rc->addGroup('/collections', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'collections' ] );
				$rc->get(    '/{co_id:\d+}/', [ '\OA\AdminResponse', 'collectionSingle' ] );
			});
			$rc->addGroup('/ledger', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'ledger' ] );
				$rc->post(   '/', [ '\OA\AdminResponse', 'ledgerCreate' ] );
				$rc->get(    '/{l_id:\d+}/', [ '\OA\AdminResponse', 'ledgerSingle' ] );
				$rc->post(   '/{l_id:\d+}/', [ '\OA\AdminResponse', 'ledgerUpdate' ] );
				$rc->delete( '/{l_id:\d+}/', [ '\OA\AdminResponse', 'ledgerDelete' ] );
				$rc->get(    '/balance/', [ '\OA\AdminResponse', 'ledgerBalance' ] );
			});
			$rc->addGroup('/companies', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'companies' ] );
				$rc->get(    '/{c_id:\d+}/', [ '\OA\AdminResponse', 'companySingle' ] );
			});
			$rc->addGroup('/generics', function ( $rc ) {
				$rc->get(    '/', [ '\OA\AdminResponse', 'generics' ] );
				$rc->get(    '/{g_id:\d+}/', [ '\OA\AdminResponse', 'genericSingle' ] );
			});
			$rc->addGroup('/report', function ( $rc ) {
				$rc->get( 	'/orders/', [ '\OA\ReportResponse', 'orders' ] );
				$rc->get( 	'/users/', [ '\OA\ReportResponse', 'users' ] );
				$rc->get( 	'/summary/', [ '\OA\ReportResponse', 'summary' ] );
				$rc->get( 	'/popular/medicines/', [ '\OA\ReportResponse', 'popularMedicines' ] );
			});
		});
	}
	
	public function dispatch(){

		$httpMethod = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];
		if( 'OPTIONS' === $httpMethod ) {
			http_response_code( 200 );
			header("Access-Control-Allow-Origin: *");
			header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
			header("Access-Control-Allow-Headers: Authorization, Content-Type, Accept, Origin");
			header("Access-Control-Max-Age: 86400");
			die;
		}

		//$uri = str_replace( '/arogga', '', $uri);

		// Strip query string (?foo=bar) and decode URI
		if (false !== $pos = strpos($uri, '?')) {
			$uri = substr($uri, 0, $pos);
		}
		$uri = rtrim( rawurldecode($uri), '/' ) . '/';

		Log::instance()->set( 'log_uri', $uri );
		
		if( '/' == $uri ){
			Response::instance()->setCode( 404 );
			Response::instance()->sendMessage( 'Nothing Found' );
		}
		
		$routeInfo = $this->route_dispatcher->dispatch($httpMethod, $uri);
		
		switch ($routeInfo[0]) {
			case \FastRoute\Dispatcher::NOT_FOUND:
				Response::instance()->setCode( 404 );
				Response::instance()->sendMessage( 'Nothing Found' );
				break;
			case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $routeInfo[1];
				Response::instance()->setCode( 405 );
				Response::instance()->sendMessage( 'Not Allowed' );
				break;
			case \FastRoute\Dispatcher::FOUND:

				$callback = $routeInfo[1];
				$vars = $routeInfo[2];

				if( $callback && is_callable( $callback ) ){
					if ( is_array( $callback ) ) {
						call_user_func_array( [ new $callback[0], $callback[1] ], $vars );
					} else {
						call_user_func_array( $callback, $vars );
					}
				}
				break;
		}
		Response::instance()->setCode( 404 );
		Response::instance()->sendMessage( 'Nothing Found' );
	}
}
