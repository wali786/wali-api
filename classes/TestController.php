<?php

namespace OA;

use OA\Factory\{Order, User, };
use GuzzleHttp\Client;

class TestController
{

    public function testUrl1()
    {
        
        $data = Order::getAllOrders();
        Response::instance()->setStatus('success');
        Response::instance()->appendData('', [
            'title' => "Order List",
            'data' => $_GET['dateFrom'],
        ]);
        Response::instance()->send();
    }

    public function orderReport()
    {
        $dateFrom = $_GET['dateFrom'];
        $dateTo = $_GET['dateTo'];
        $data = Order::report($dateFrom,$dateTo);
        Response::instance()->setStatus('success');
        Response::instance()->appendData('',$data);
        Response::instance()->send();
    }

    public function orderReportSummery()
    {
        $dateFrom = $_GET['dateFrom'];
        $dateTo = $_GET['dateTo'];
        $data = Order::summary($dateFrom,$dateTo);
        Response::instance()->setStatus('success');
        Response::instance()->appendData('',$data);
        Response::instance()->send();
    }

    public function userOrderReport()
    {
        $dateFrom = $_GET['dateFrom'];
        $dateTo = $_GET['dateTo'];
        $data = Order::userReport($dateFrom,$dateTo);
        Response::instance()->setStatus('success');
        Response::instance()->appendData('',$data);
        Response::instance()->send();
    }

    public function popularMedicines()
    {
        $dateFrom = $_GET['dateFrom'];
        $dateTo = $_GET['dateTo'];
        $data = Order::popularMedicines($dateFrom,$dateTo);
        Response::instance()->setStatus('success');
        Response::instance()->appendData('',$data);
        Response::instance()->send();
    }

    public function orderPlace()
    {
        for($i = 0; $i<20; $i++){

            $u_array = [
                'u_name' => Functions::randToken( 'alpha', 6 ) . ' ' . Functions::randToken( 'alpha', 6 ),
                'u_mobile' => '+8801' . Functions::randToken( 'numeric', 9 ),
            ];
            $user = User::getBy( 'u_mobile', $u_array['u_mobile'] );
    
            if( ! $user ) {
                do{
                    $u_referrer = Functions::randToken( 'distinct', 10 );
        
                } while( User::getBy( 'u_referrer', $u_referrer ) );
        
                $u_array['u_referrer'] = $u_referrer;
                $user = new User;
                $user->insert( $u_array );
            }
            $medicineQty = [];
    
            $query = DB::db()->prepare( 'SELECT m_id FROM t_medicines WHERE m_status = ? AND m_rob > ? ORDER BY RAND() LIMIT 10' );
            $query->execute( [ 'active', 0 ] );
            while( $m = $query->fetch() ){
                $medicineQty[ $m['m_id'] ] = rand(1,10);
            }
            
            $order = new Order;
            $cart_data = Functions::cartData( $user, $medicineQty );
    
            $c_medicines = $cart_data['medicines'];
            unset( $cart_data['medicines'] );
    
            $o_data = [
                'u_id' => $user->u_id,
                'u_name' => $user->u_name,
                'u_mobile' => $user->u_mobile,
                'o_address' => Functions::randToken( 'alpha', 6 ) . ' ' . Functions::randToken( 'alpha', 6 ),
                'o_status' => array_rand( array_flip( ['processing', 'confirmed', 'delivering', 'delivered'] )),
                'o_i_status' => array_rand( array_flip( ['dncall', 'confirmed', 'packing'] )),
                'o_created' => \date( 'Y-m-d H:i:s',time() - random_int( 0, 7884000) ), //3 months
            ];
            if( 'delivered' == $o_data['o_status'] ){
                $o_data['o_delivered'] = \date( 'Y-m-d H:i:s',\strtotime( $o_data['o_created'] ) + \rand( 122400, 165600) );
                $o_data['o_i_status'] = 'paid';
            }
            $o_data['o_subtotal'] = $cart_data['subtotal'];
            $o_data['o_addition'] = $cart_data['a_amount'];
            $o_data['o_deduction'] = $cart_data['d_amount'];
            $o_data['o_total'] = $cart_data['total'];
    
            $order->insert( $o_data  );
            Functions::ModifyOrderMedicines( $order, $c_medicines );
            $meta = [
                'o_data' => $cart_data,
                'o_secret' => Functions::randToken( 'alnumlc', 16 ),
            ];
            $order->insertMetas( $meta );
    
            $cash_back = $order->cashBackAmount();
    
            //again get user. User data may changed.
            $user = User::getUser( $order->u_id );
            
            if ( $cash_back ) {
                $user->u_p_cash = $user->u_p_cash + $cash_back;
            }
            if( isset($cart_data['deductions']['cash']) ){
                $user->u_cash = $user->u_cash - $cart_data['deductions']['cash']['amount'];
            }
            $user->update();
        }
    }
}