<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate, Inventory};
use GuzzleHttp\Client;

class AdminResponse {
    private $user;

    function __construct() {
        \header("Access-Control-Allow-Origin: *");
        //\header("Access-Control-Request-Headers: *");
        \define( 'ADMIN', true );

        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->setCode( 403 );
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'You are not logged in' );
        }
        if( ! $user->can( 'backendAccess' ) ) {
            Response::instance()->setCode( 401 );
            Response::instance()->sendMessage( 'Your account does not have admin access.');
        }
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        if( $user->can( 'onlyGET' ) && $httpMethod != 'GET' ) {
            Response::instance()->sendMessage( 'Your account does not have permission to do this.');
        }

        $this->user = $user;
    }

    public function medicines() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $category = isset( $_GET['_category'] ) ? $_GET['_category'] : '';
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $c_id = isset( $_GET['_c_id'] ) ? $_GET['_c_id'] : 0;
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;
        $available = isset( $_GET['_available'] ) ? (int)$_GET['_available'] : 0;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_medicines WHERE 1=1' );
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND m_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if ( $search ) {
            if( \is_numeric($search) && !$ids ){
                $db->add( ' AND m_id = ?', $search );
            } else {
                $search = preg_replace('/[^a-z0-9\040\.\-]+/i', ' ', $search);
                $org_search = $search = \rtrim( \trim(preg_replace('/\s\s+/', ' ', $search ) ), '-' );

                if( false === \strpos( $search, ' ' ) ){
                    $search .= '*';
                } else {
                    $search = '+' . \str_replace( ' ', ' +', $search) . '*';
                }
                if( \strlen( $org_search ) > 2 ){
                    $db->add( " AND (MATCH(m_name) AGAINST (? IN BOOLEAN MODE) OR m_name LIKE ?)", $search, "{$org_search}%" );
                } elseif( $org_search ) {
                    $db->add( ' AND m_name LIKE ?', "{$org_search}%" );
                }
            }
        }
        if( $category ) {
            $db->add( ' AND m_category = ?', $category );
        }
        if( $status ) {
            $db->add( ' AND m_status = ?', $status );
        }
        if( $c_id ){
            $db->add( ' AND m_c_id = ?', $c_id );
        }
        if( $available ){
            $db->add( ' AND m_rob = ?', 1 );
        } 

        if( $orderBy && \property_exists('\OA\Factory\Medicine', $orderBy ) ) {
            $db->add( " ORDER BY $orderBy $order" );
        }
        
        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );

        $cache_key = \md5( $db->getSql() . \json_encode($db->getParams()) );
        
        if ( $cache_data = Cache::instance()->get( $cache_key, 'adminMedicines' ) ){
            Response::instance()->setData( $cache_data['data'] );
            Response::instance()->setResponse( 'total', $cache_data['total'] );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');

        while( $medicine = $query->fetch() ){
            $data = $medicine->toArray();
            $data['id'] = $medicine->m_id;
            $data['attachedFiles'] = Functions::getPicUrlsAdmin($medicine->m_id);

            Response::instance()->appendData( '', $data );
        }
        if ( $all_data = Response::instance()->getData() ) {
            $cache_data = [
                'data' => $all_data,
                'total' => $total,
            ];
            Cache::instance()->set( $cache_key, $cache_data, 'adminMedicines', 60 * 60 * 24 );

            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No medicines Found' );
        }
    }

    public function medicineCreate() {
        if( ! $this->user->can( 'medicineCreate' ) ) {
            Response::instance()->sendMessage( 'Your account does not have medicine create capabilities.');
        }
        if(empty($_POST['m_name']) ) {
            Response::instance()->sendMessage( 'Name is Required' );
        }
        $medicine = new Medicine;
        $_POST['m_u_id'] = Auth::id();
        unset( $_POST['m_generic'], $_POST['m_company'] );
        $medicine->insert( $_POST );
        if( 'allopathic' !== $medicine->m_category && isset( $_POST['description'] ) ){
            $medicine->setMeta( 'description', $_POST['description'] );
        }
        if( isset($_POST['m_status']) && 'active' != $_POST['m_status'] ){
            //If status active, then it will incr from Medicine class
            Cache::instance()->incr( 'suffixForMedicines' );
        }

        if( isset( $_POST['attachedFiles'] ) ){
            Functions::modifyMedicineImages( $medicine->m_id, $_POST['attachedFiles'] );
        }

        $this->medicineSingle( $medicine->m_id );
    }

    public function medicineSingle( $m_id ) {

        if ( ! $m_id ) {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        if( $medicine = Medicine::getMedicine( $m_id ) ){
            Response::instance()->setStatus( 'success' );
            //$price = $medicine->m_price * (intval($medicine->m_unit));
            //$d_price = ( ( $price * 90 ) / 100 );

            $data = $medicine->toArray();
            $data['id'] = $medicine->m_id;
            $data['attachedFiles'] = Functions::getPicUrlsAdmin($medicine->m_id);
            if( 'allopathic' !== $medicine->m_category ){
                $data['description'] = (string)$medicine->getMeta( 'description' );
            }

            Response::instance()->setData( $data );
            
        } else {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        Response::instance()->send();
    }

    public function medicineUpdate( $m_id ) {

        if( ! $this->user->can( 'medicineEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have medicine edit capabilities.');
        }
        if ( ! $m_id ) {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        if( $medicine = Medicine::getMedicine( $m_id ) ){
            //$_POST['m_comment'] = isset($_POST['m_comment'])? $_POST['m_comment'] : '';
            unset( $_POST['m_generic'], $_POST['m_company'] );
            $medicine->update( $_POST );
            if( 'allopathic' !== $medicine->m_category && isset( $_POST['description'] ) ){
                $medicine->setMeta( 'description', $_POST['description'] );
            }

            Functions::modifyMedicineImages( $medicine->m_id, isset( $_POST['attachedFiles'] ) ? $_POST['attachedFiles'] : [] );

            $this->medicineSingle( $medicine->m_id );
            
        } else {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        Response::instance()->send();
    }

    public function medicineDelete( $m_id ) {

        if( ! $this->user->can( 'medicineDelete' ) ) {
            Response::instance()->sendMessage( 'Your account does not have medicine delete capabilities.');
        }

        if ( ! $m_id ) {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        if( $medicine = Medicine::getMedicine( $m_id ) ){
            $medicine->delete();
            Response::instance()->setStatus( 'success' );
            Response::instance()->setData( ['id' => $m_id ] ); 
        } else {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        Response::instance()->send();
    }

    public function users() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $role = isset( $_GET['_role'] ) ? $_GET['_role'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_users WHERE 1=1' );
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND u_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if ( $search ) {
            if( \is_numeric( $search ) && 0 === \strpos( $search, '0' ) ) {
                $search = "+88{$search}";
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND u_mobile LIKE ?', "{$search}%" );
            } elseif( \is_numeric( $search ) ) {
                $db->add( ' AND u_id = ?', $search );
            } else {
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND u_name LIKE ?', "{$search}%" );
            }
        }
        if( $status ) {
            $db->add( ' AND u_status = ?', $status );
        }
        if( $role ) {
            if( false !== \strpos( $role, ',' ) ) {
                $roles = \array_filter( \array_map( 'trim', \explode( ',', $role ) ) );
                $in  = str_repeat('?,', count($roles) - 1) . '?';
                $db->add( " AND u_role IN ($in)", ...$roles );
            } else {
                $db->add( ' AND u_role = ?', $role );
            }
        }
        if( $orderBy && \property_exists('\OA\Factory\User', $orderBy ) ) {
            $db->add( " ORDER BY $orderBy $order" );
        }
        
        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $cache_key = \md5( $db->getSql() . \json_encode($db->getParams()) );
        
        if ( $role && ( $cache_data = Cache::instance()->get( $cache_key, 'adminUsers' ) ) ){
            //send cached data only if there user role. Users are changing too much
            //Currently when searching for roles users
            Response::instance()->setData( $cache_data['data'] );
            Response::instance()->setResponse( 'total', $cache_data['total'] );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }

        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\User');

        while( $user = $query->fetch() ){
            $data = $user->toArray();
            $data['id'] = $user->u_id;

            Response::instance()->appendData( '', $data );
        }
        if ( $all_data = Response::instance()->getData() ) {
            if( $role ){
                $cache_data = [
                    'data' => $all_data,
                    'total' => $total,
                ];
                Cache::instance()->set( $cache_key, $cache_data, 'adminUsers', 60 * 60 * 24 );
            }

            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No users Found' );
        }
    }

    public function userSingle( $u_id ) {

        if ( ! $u_id ) {
            Response::instance()->sendMessage( 'No users Found' );
        }

        if( $user = User::getUser( $u_id ) ){
            Response::instance()->setStatus( 'success' );

            $data = $user->toArray();
            $data['id'] = $user->u_id;

            Response::instance()->setData( $data );
            
        } else {
            Response::instance()->sendMessage( 'No users Found' );
        }

        Response::instance()->send();
    }
    public function userCreate() {
        if( ! $this->user->can( 'userCreate' ) ) {
            Response::instance()->sendMessage( 'Your account does not have user create capabilities.');
        }
        if(empty($_POST['u_name']) || empty($_POST['u_mobile']) ) {
            Response::instance()->sendMessage( 'All Fields Required' );
        }
        if( ! preg_match( '/(^(\+8801|8801|008801))(\d){9}$/', $_POST['u_mobile'] ) ) {
            Response::instance()->sendMessage( 'Invalid mobile number.');
        }
        if( User::getBy( 'u_mobile', $_POST['u_mobile'] ) ){
            Response::instance()->sendMessage( 'Mobile number already exists.' );
        }
        do{
            $u_referrer = Functions::randToken( 'distinct', 10 );

        } while( User::getBy( 'u_referrer', $u_referrer ) );

        $_POST['u_referrer'] = $u_referrer;

        $user = new User;
        if( $user->insert( $_POST ) ) {
            //we want cache update only when admin changes user.
            Cache::instance()->incr( 'suffixForUsers' );
        }

        $this->userSingle( $user->u_id );
    }

    public function userUpdate( $u_id ) {

        if( ! $this->user->can( 'userEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have user edit capabilities.');
        }

        if ( ! $u_id ) {
            Response::instance()->sendMessage( 'No users Found' );
        }
        if( $user = User::getUser( $u_id ) ){
            if( ! $this->user->can( 'userChangeRole' ) ) {
                unset( $_POST['u_role'], $_POST['u_cash'], $_POST['u_p_cash'] );
            }
            if( $user->update( $_POST ) ) {
                //we want cache update only when admin changes user.
                Cache::instance()->incr( 'suffixForUsers' );
            }

            $this->userSingle( $user->u_id );
            
        } else {
            Response::instance()->sendMessage( 'No users Found' );
        }

        Response::instance()->send();
    }

    public function userDelete( $u_id ) {
        if( ! $this->user->can( 'userDelete' ) ) {
            Response::instance()->sendMessage( 'Your account does not have user delete capabilities.');
        }

        if ( ! $u_id ) {
            Response::instance()->sendMessage( 'No users Found' );
        }

        if( $user = User::getUser( $u_id ) ){
            if( $user->delete() ){
                //we want cache update only when admin changes user.
                Cache::instance()->incr( 'suffixForUsers' );
            }

            Response::instance()->setStatus( 'success' );
            Response::instance()->setData( ['id' => $u_id ]);
            
        } else {
            Response::instance()->sendMessage( 'No users Found' );
        }

        Response::instance()->send();
    }

    public function orders() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $u_id = isset( $_GET['u_id'] ) ? (int)$_GET['u_id'] : 0;
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $i_status = isset( $_GET['_i_status'] ) ? $_GET['_i_status'] : '';
        $o_created = isset( $_GET['_o_created'] ) ? $_GET['_o_created'] : '';
        $o_created_end = isset( $_GET['_o_created_end'] ) ? $_GET['_o_created_end'] : '';
        $o_delivered = isset( $_GET['_o_delivered'] ) ? $_GET['_o_delivered'] : '';
        $o_delivered_end = isset( $_GET['_o_delivered_end'] ) ? $_GET['_o_delivered_end'] : '';
        $de_id = isset( $_GET['_de_id'] ) ? $_GET['_de_id'] : 0;
        $payment_method = isset( $_GET['_payment_method'] ) ? $_GET['_payment_method'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_orders WHERE 1=1' );
        if ( $search ) {
            if( \is_numeric( $search ) && 0 === \strpos( $search, '0' ) ) {
                $search = "+88{$search}";
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND u_mobile LIKE ?', "{$search}%" );
            } elseif( \is_numeric( $search ) ) {
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND o_id LIKE ?', "{$search}%" );
            }
        }

        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND o_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if( $u_id ){
            $db->add( ' AND u_id = ?', $u_id );
        } else {
            //For offline orders there is no user. So show only online orders
            $db->add( ' AND u_id > ?', 0 );
        }
        if( $status ) {
            $db->add( ' AND o_status = ?', $status );
        }
        if( $i_status ) {
            $db->add( ' AND o_i_status = ?', $i_status );
        }
        if( $o_created ) {
            $db->add( ' AND o_created >= ? AND o_created <= ?', $o_created . ' 00:00:00', ($o_created_end ?: $o_created) . ' 23:59:59' );
        }
        if( $o_delivered ){
            $db->add( ' AND o_delivered >= ? AND o_delivered <= ?', $o_delivered . ' 00:00:00', ($o_delivered_end ?: $o_delivered) . ' 23:59:59' );
        }
        if( $de_id ){
            $db->add( ' AND o_de_id = ?', $de_id );
        }
        if( $payment_method ){
            $db->add( ' AND o_payment_method = ?', $payment_method );
        }
        if( $orderBy && \property_exists('\OA\Factory\Order', $orderBy ) ) {
            $db->add( " ORDER BY $orderBy $order" );
        }
        
        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Order');

        while( $order = $query->fetch() ){
            $data = $order->toArray();
            $data['id'] = $order->o_id;
            $data['o_i_note'] = (string)$order->getMeta('o_i_note');

            //$data['d_code'] = (string)$order->getMeta( 'd_code' );
            //$data['o_note'] = (string)$order->getMeta('o_note');
            //$data['prescriptions'] = $order->prescriptions;
            //$data['medicineQty'] = $order->medicineQty;

            $supplierPrice = 'delivered' == $order->o_status ? $order->getMeta( 'supplierPrice' ) : 0.00;
            if( false === $supplierPrice ){
                $query2 = DB::db()->prepare( "SELECT SUM(s_price*m_qty) FROM t_o_medicines WHERE o_id = ? AND om_status = ?" );
                $query2->execute( [ $order->o_id, 'available' ] );
                $supplierPrice = round( $query2->fetchColumn(), 2 );
                $order->setMeta( 'supplierPrice', $supplierPrice );
            }
            $data['supplierPrice'] = $supplierPrice;
            $data['paymentGatewayFee'] = 'fosterPayment' == $order->o_payment_method ? $order->getMeta( 'paymentGatewayFee' ) : 0.00;

            //$data['invoiceUrl'] = \sprintf( SITE_URL . '/v1/invoice/%d/%s/', $order->o_id, $order->getMeta( 'o_secret' ) );

            Response::instance()->appendData( '', $data );
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No orders Found' );
        } else {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    public function orderSingle( $o_id ) {

        if ( ! $o_id ) {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        if( $order = Order::getOrder( $o_id ) ){
            Response::instance()->setStatus( 'success' );

            $defaultSAddress = [
                'division' => '',
                'district' => '',
                'area' => '',
            ];

            $data = $order->toArray();
            $data['id'] = $order->o_id;
            $data['d_code'] = (string)$order->getMeta( 'd_code' );
            $data['o_note'] = (string)$order->getMeta('o_note');
            $data['o_i_note'] = (string)$order->getMeta('o_i_note');
            $data['s_address'] = $order->getMeta('s_address')?:$defaultSAddress;
            //$data['man_discount'] = (string)$order->getMeta( 'man_discount' );
            //$data['man_addition'] = (string)$order->getMeta('man_addition');
            $data['prescriptions'] = $order->prescriptions;
            $data['medicineQty'] = $order->medicineQty;
            $data['supplierPrice'] = 'delivered' == $order->o_status ? $order->getMeta( 'supplierPrice' ) : 0.00;
            $data['paymentGatewayFee'] = 'fosterPayment' == $order->o_payment_method ? $order->getMeta( 'paymentGatewayFee' ) : 0.00;

            $secret = $order->getMeta( 'o_secret' );
            if( ! $secret ) {
                $secret = Functions::randToken( 'alnumlc', 16 );
                $order->setMeta( 'o_secret', $secret );
            }
            $data['invoiceUrl'] = \sprintf( SITE_URL . '/v1/invoice/%d/%s/', $order->o_id, $secret );
            $data['paymentResponse'] = $order->getMeta( 'paymentResponse' );
            if( \in_array( $order->o_status, [ 'confirmed', 'delivering', 'delivered' ] ) && 'confirmed' == $order->o_i_status && 'paid' !== $order->getMeta( 'paymentStatus' ) ){
                $data['paymentUrl'] = \sprintf( SITE_URL . '/payment/v1/%d/%s/', $order->o_id, $secret );
            }

            Response::instance()->setData( $data );
            
        } else {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        Response::instance()->send();
    }

    public function orderDelete( $o_id ) {

        if( ! $this->user->can( 'orderDelete' ) ) {
            Response::instance()->sendMessage( 'Your account does not have order delete capabilities.');
        }
        if ( ! $o_id ) {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        if( $order = Order::getOrder( $o_id ) ){
            $order->delete();

            Response::instance()->setStatus( 'success' );
            Response::instance()->setData( ['id' => $o_id ]);
            
        } else {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        Response::instance()->send();
    }

    public function orderUpdate( $o_id ) {

        if( ! $this->user->can( 'orderEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have order edit capabilities.');
        }
        if ( ! $o_id ) {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        if( $order = Order::getOrder( $o_id ) ){
            if( \in_array( $order->o_status, [ 'cancelled', 'damaged' ] ) ) {
                Response::instance()->sendMessage( 'You can not edit this order anymore.');
            }
            if( ! ( $user = User::getUser( $_POST['u_id'] ) ) ) {
                //Response::instance()->sendMessage( 'Invalid order user.');
            }
            $new_status = isset( $_POST['o_status'] ) ? $_POST['o_status'] : '';
            if( 'delivered' == $order->o_status) {
                if( 'returned' == $new_status  ) {
                    $order->update( ['o_status' => 'returned'] );
                    //Refund user
                    //again get user. User data may changed.
                    $user = User::getUser( $order->u_id );
                    if( $user ){
                        $user->cashUpdate( $order->o_total );
                    }

                    Functions::ledgerCreate( "Order #{$order->o_id} returned", -$order->o_total, 'debit' );
                    $this->orderSingle( $order->o_id );
                }
                Response::instance()->sendMessage( 'You can not edit this order anymore.');
            } elseif( 'returned' == $new_status ){
                Response::instance()->sendMessage( 'You can not return an order which not yet delivered.');
            }

            if( $order->o_subtotal && empty( $_POST['medicineQty'] ) ){
                Response::instance()->sendMessage( 'Medicines are empty. Order not saved.');
            }

            $prev_order = clone $order;
            $prev_o_data = (array)$prev_order->getMeta( 'o_data' );
            $prev_cash_back = 0;
            $prev_applied_cash = 0;
            if( 'call' != $prev_order->o_i_status ) {
                $prev_cash_back = $prev_order->cashBackAmount();
            }

            if( isset($prev_o_data['deductions']['cash']) ) {
                $prev_applied_cash = $prev_o_data['deductions']['cash']['amount'];
            }
            $d_code = isset($_POST['d_code']) ? $_POST['d_code']: '';
            $o_note = isset($_POST['o_note']) ? filter_var($_POST['o_note'], FILTER_SANITIZE_STRING) : '';
            $o_i_note = isset($_POST['o_i_note']) ? filter_var($_POST['o_i_note'], FILTER_SANITIZE_STRING) : '';
            $s_address = ( isset( $_POST['s_address'] ) && is_array($_POST['s_address']) )? $_POST['s_address']: [];
            if( $s_address ){
                $s_address['location'] = sprintf('%s, %s, %s, %s', $s_address['homeAddress'], $s_address['area'], $s_address['district'], $s_address['division'] );
                $_POST['o_gps_address'] = $s_address['location'];
            }
            $cart_data = Functions::cartData( $user, $_POST['medicineQty'], $d_code, $prev_applied_cash, false, ['s_address' => $s_address] );

            if( isset($cart_data['deductions']['cash']) && !empty($cart_data['deductions']['cash']['info'])) {
                $cart_data['deductions']['cash']['info'] = "Didn't apply because the order value was less than ৳499.";
            }
            if( isset($cart_data['additions']['delivery']) && !empty($cart_data['additions']['delivery']['info'])) {
                $cart_data['additions']['delivery']['info'] = str_replace('To get free delivery order more than', 'Because the order value was less than', $cart_data['additions']['delivery']['info']);
            }
            $c_medicines = $cart_data['medicines'];
            unset( $cart_data['medicines'] );

            $o_data = $_POST;
            $o_data['o_subtotal'] = $cart_data['subtotal'];
            $o_data['o_addition'] = $cart_data['a_amount'];
            $o_data['o_deduction'] = $cart_data['d_amount'];
            $o_data['o_total'] = $cart_data['total'];

            $order->update( $o_data );
            $order->setMeta( 'd_code', $d_code );
            $order->setMeta( 'o_note', $o_note );
            $order->setMeta( 'o_i_note', $o_i_note );
            $order->setMeta( 'o_data', $cart_data );
            $order->setMeta( 's_address', $s_address );
            Functions::ModifyOrderMedicines( $order, $c_medicines, $prev_order );

            //again get user. User data may changed.
            $user = User::getUser( $order->u_id );
            if( $user ){
                $user->u_cash += $prev_applied_cash;
                if( isset($cart_data['deductions']['cash']) ) {
                    $user->u_cash -= $cart_data['deductions']['cash']['amount'];
                }
                $cash_back = 0;
                if( 'call' != $order->o_i_status ) {
                    $cash_back = $order->cashBackAmount();
                    $user->u_p_cash = $user->u_p_cash - $prev_cash_back + $cash_back;
                }
                $user->update();

                if( $cash_back && ! $prev_cash_back ){
                    $message = "Congratulations!!! You have received a cashback of ৳{$cash_back} from arogga. The cashback will be automatically applied at your next order.";
                    Functions::sendNotification( $user->fcm_token, 'Cashback Received.', $message );
                }
            }

            $this->orderSingle( $order->o_id );
            
        } else {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        Response::instance()->send();
    }

    public function orderCreate() {
        if( ! $this->user->can( 'orderCreate' ) ) {
            Response::instance()->sendMessage( 'Your account does not have order create capabilities.');
        }
        if(empty($_POST['u_name']) || empty($_POST['u_mobile']) || empty($_POST['medicineQty']) ) {
            Response::instance()->sendMessage( 'All Fields Required' );
        }
        if( ! preg_match( '/(^(\+8801|8801|008801))(\d){9}$/', $_POST['u_mobile'] ) ) {
            Response::instance()->sendMessage( 'Invalid mobile number.');
        }
        $user = User::getBy( 'u_mobile', $_POST['u_mobile'] );

        if( ! $user ) {
            do{
                $u_referrer = Functions::randToken( 'distinct', 10 );
    
            } while( User::getBy( 'u_referrer', $u_referrer ) );
    
            $_POST['u_referrer'] = $u_referrer;
            $user = new User;
            $user->insert( $_POST );
        }
        $s_address = ( isset( $_POST['s_address'] ) && is_array($_POST['s_address']) )? $_POST['s_address']: [];
        
        $order = new Order;
        $cart_data = Functions::cartData( $user, $_POST['medicineQty'], isset($_POST['d_code']) ? $_POST['d_code']: '', 0, false, ['s_address' => $s_address] );

        if( isset($cart_data['deductions']['cash']) && !empty($cart_data['deductions']['cash']['info'])) {
            $cart_data['deductions']['cash']['info'] = "Didn't apply because the order value was less than ৳499.";
        }
        if( isset($cart_data['additions']['delivery']) && !empty($cart_data['additions']['delivery']['info'])) {
            $cart_data['additions']['delivery']['info'] = str_replace('To get free delivery order more than', 'Because the order value was less than', $cart_data['additions']['delivery']['info']);
        }

        $c_medicines = $cart_data['medicines'];
        unset( $cart_data['medicines'] );

        $o_data = $_POST;
        $o_data['o_subtotal'] = $cart_data['subtotal'];
        $o_data['o_addition'] = $cart_data['a_amount'];
        $o_data['o_deduction'] = $cart_data['d_amount'];
        $o_data['o_total'] = $cart_data['total'];
        $o_data['u_id'] = $user->u_id;

        $order->insert( $o_data  );
        Functions::ModifyOrderMedicines( $order, $c_medicines );
        $meta = [
            'o_data' => $cart_data,
            'o_secret' => Functions::randToken( 'alnumlc', 16 ),
            's_address' => $s_address,
        ];
        if( ! empty( $_POST['d_code'] ) ) {
            $meta['d_code'] = $_POST['d_code'];
        }
        $order->insertMetas( $meta );

        $cash_back = $order->cashBackAmount();

        //again get user. User data may changed.
        $user = User::getUser( $order->u_id );
        
        if ( $cash_back ) {
            $user->u_p_cash = $user->u_p_cash + $cash_back;
        }
        if( isset($cart_data['deductions']['cash']) ){
            $user->u_cash = $user->u_cash - $cart_data['deductions']['cash']['amount'];
        }
        $user->update();
        
        $this->orderSingle( $order->o_id );
    }

    public function offlineOrders() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $ph_id = isset( $_GET['_ph_id'] ) ? $_GET['_ph_id'] : 0;
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $i_status = isset( $_GET['_i_status'] ) ? $_GET['_i_status'] : '';
        $o_created = isset( $_GET['_o_created'] ) ? $_GET['_o_created'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_orders WHERE 1=1' );
        if ( $search && \is_numeric( $search ) ) {
            $search = addcslashes( $search, '_%\\' );
            $db->add( ' AND o_id LIKE ?', "{$search}%" );
        }
        //For offline orders there is no user 
        $db->add( ' AND u_id = ?', 0 );

        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND o_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if( 'pharmacy' == $this->user->u_role ){
            $db->add( ' AND o_ph_id = ?', Auth::id() );
        } elseif( $ph_id ) {
            $db->add( ' AND o_ph_id = ?', $ph_id );
        } else {
            $db->add( ' AND o_ph_id > ?', 0 );
        }

        if( $status ) {
            $db->add( ' AND o_status = ?', $status );
        }
        if( $i_status ) {
            $db->add( ' AND o_i_status = ?', $i_status );
        }
        if( $o_created ) {
            $db->add( ' AND o_created >= ? AND o_created <= ?', $o_created . ' 00:00:00', $o_created . ' 23:59:59' );
        }
        if( $orderBy && \property_exists('\OA\Factory\Order', $orderBy ) ) {
            $db->add( " ORDER BY $orderBy $order" );
        }
        
        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Order');

        while( $order = $query->fetch() ){
            $data = $order->toArray();
            $data['id'] = $order->o_id;
            //$data['medicineQty'] = $order->medicineQty;

            Response::instance()->appendData( '', $data );
        }
        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No orders Found' );
        }
    }

    public function offlineOrderCreate() {
        if( ! $this->user->can( 'offlineOrderCreate' ) ) {
            Response::instance()->sendMessage( 'Your account does not have order create capabilities.');
        }
        if( empty($_POST['medicineQty']) ) {
            Response::instance()->sendMessage( 'Medicines Required' );
        }
        $args = [];
        $man_discount = ( !empty($_POST['man_discount']) && is_numeric( $_POST['man_discount'] ) ) ? \round( $_POST['man_discount'], 2 ) : 0;
        $man_addition = ( !empty($_POST['man_addition']) && is_numeric( $_POST['man_addition'] ) ) ? \round( $_POST['man_addition'], 2 ) : 0;
        $args['man_discount'] = $man_discount;
        $args['man_addition'] = $man_addition;

        $order = new Order;
        $cart_data = Functions::cartData( '', $_POST['medicineQty'], '', 0, true, $args );

        $c_medicines = $cart_data['medicines'];
        unset( $cart_data['medicines'] );

        $o_data = $_POST;
        $o_data['o_subtotal'] = $cart_data['subtotal'];
        $o_data['o_addition'] = $cart_data['a_amount'];
        $o_data['o_deduction'] = $cart_data['d_amount'];
        $o_data['o_total'] = $cart_data['total'];
        $o_data['u_name'] = 'Offline';
        $o_data['u_id'] = 0;
        $o_data['o_ph_id'] = Auth::id();
        $o_data['o_de_id'] = Auth::id();
        $o_data['o_status'] = 'delivered';
        $o_data['o_i_status'] = 'confirmed';

        $order->insert( $o_data  );
        Functions::ModifyOrderMedicines( $order, $c_medicines );
        $meta = [ 
            'o_data' => $cart_data,
            'man_discount' => $man_discount,
            'man_addition' => $man_addition,
        ];
        $order->insertMetas( $meta );

        foreach ( $order->medicineQty as $id_qty ) {
            $m_id = isset($id_qty['m_id']) ? (int)$id_qty['m_id'] : 0;
            $quantity = isset($id_qty['qty']) ? (int)$id_qty['qty'] : 0;

            if( $inventory = Inventory::getByPhMid( $order->o_ph_id, $m_id ) ){
                $inventory->i_qty = $inventory->i_qty - $quantity;
                $inventory->update();
                DB::instance()->update( 't_o_medicines', ['om_status' => 'available', 's_price' => $inventory->i_price ], [ 'o_id' => $order->o_id, 'm_id' => $m_id ] );
            }
        }

        //To trigger
        //$order->update( ['o_status' => 'delivering', 'o_i_status' => 'confirmed'] );
        //$order->update( ['o_status' => 'delivered'] );
        
        $this->orderSingle( $order->o_id );
    }

    public function offlineOrderUpdate( $o_id ) {

        Response::instance()->sendMessage( 'Offline order update is not possible right now.');

        if( ! $this->user->can( 'orderEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have order edit capabilities.');
        }
        if ( ! $o_id ) {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        if( $order = Order::getOrder( $o_id ) ){
            $prev_order = clone $order;
            $prev_o_data = (array)$prev_order->getMeta( 'o_data' );
            $cart_data = Functions::cartData( '', $_POST['medicineQty'], '', 0, true );

            $c_medicines = $cart_data['medicines'];
            unset( $cart_data['medicines'] );

            $o_data = $_POST;
            $o_data['o_subtotal'] = $cart_data['subtotal'];
            $o_data['o_addition'] = $cart_data['a_amount'];
            $o_data['o_deduction'] = $cart_data['d_amount'];
            $o_data['o_total'] = $cart_data['total'];

            $order->update( $o_data );
            $order->setMeta( 'o_data', $cart_data );
            Functions::ModifyOrderMedicines( $order, $c_medicines, $prev_order );

            $this->orderSingle( $order->o_id );
            
        } else {
            Response::instance()->sendMessage( 'No orders Found' );
        }

        Response::instance()->send();
    }

    public function orderMedicines() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $category = isset( $_GET['_category'] ) ? $_GET['_category'] : '';
        $c_id = isset( $_GET['_c_id'] ) ? $_GET['_c_id'] : 0;
        $ph_id = isset( $_GET['_ph_id'] ) ? $_GET['_ph_id'] : 0;
        $om_status = isset( $_GET['_om_status'] ) ? $_GET['_om_status'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $o_delivered = isset( $_GET['_o_delivered'] ) ? $_GET['_o_delivered'] : '';
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $i_status = isset( $_GET['_i_status'] ) ? $_GET['_i_status'] : '';

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS tom.*, (100 - (tom.s_price/tom.m_d_price*100)) as supplier_percent, tm.m_name, tm.m_form, tm.m_strength, tr.o_created, tr.o_delivered, tr.o_status, tr.o_i_status FROM t_o_medicines tom INNER JOIN t_medicines tm ON tom.m_id = tm.m_id INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE 1=1' );
        if ( $search ) {
            if( \is_numeric( $search ) ) {
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND tom.o_id LIKE ?', "{$search}%" );
            } else {
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND tm.m_name LIKE ?', "{$search}%" );
            }
        }
        if( $category ) {
            $db->add( ' AND tm.m_category = ?', $category );
        }
        if( $c_id ) {
            $db->add( ' AND tm.m_c_id = ?', $c_id );
        }
        if( $ph_id ) {
            $db->add( ' AND tr.o_ph_id = ?', $ph_id );
        }
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND tom.om_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }

        if( $status ) {
            $db->add( ' AND tr.o_status = ?', $status );
        }
        if( $i_status ) {
            $db->add( ' AND tr.o_i_status = ?', $i_status );
        }
        if( $om_status ) {
            $db->add( ' AND tom.om_status = ?', $om_status );
        }

        if( $o_delivered ) {
            $db->add( ' AND tr.o_delivered >= ? AND tr.o_delivered <= ?', $o_delivered . ' 00:00:00', $o_delivered . ' 23:59:59' );
        }

        if( $orderBy && \in_array( $orderBy, ['o_id', 'm_qty', 'm_unit', 'm_price', 'm_d_price', 's_price', 'om_status'] ) ) {
            $db->add( " ORDER BY tom.{$orderBy} $order" );
        } elseif( $orderBy && \in_array( $orderBy, ['m_name', 'm_form', 'm_strength'] ) ) {
            $db->add( " ORDER BY tm.{$orderBy} $order" );
        } elseif( $orderBy && \in_array( $orderBy, ['o_created', 'o_delivered', 'o_status'] ) ) {
            $db->add( " ORDER BY tr.{$orderBy} $order" );
        } elseif( $orderBy && \in_array( $orderBy, ['supplier_percent'] ) ) {
            $db->add( " ORDER BY supplier_percent $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['om_id'];
            $data['m_price_total'] = \round( $data['m_qty'] * $data['m_price'], 2 );
            $data['m_d_price_total'] = \round( $data['m_qty'] * $data['m_d_price'], 2 );
            $data['s_price_total'] = \round( $data['m_qty'] * $data['s_price'], 2 );
            $data['supplier_percent'] = \round( $data['supplier_percent'], 1 ) . '%';
            unset($data['o_delivered']);
            $data['attachedFiles'] = Functions::getPicUrlsAdmin( $data['m_id'] );

            Response::instance()->appendData( '', $data );
        }

        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No orders Found' );
        } else {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function orderMedicineSingle( $om_id ) {
        $query = DB::db()->prepare( 'SELECT tom.*, tm.m_name, tm.m_form, tm.m_strength FROM t_o_medicines tom INNER JOIN t_medicines tm ON tom.m_id = tm.m_id WHERE tom.om_id = ? LIMIT 1' );
        $query->execute( [ $om_id ] );
        if( $om = $query->fetch() ){
            $data = $om;
            $data['id'] = $om['om_id'];

            Response::instance()->setStatus( 'success' );
            Response::instance()->setData( $data );
        } else {
            Response::instance()->sendMessage( 'No order medicine Found' );
        }

        Response::instance()->send();
    }

    public function orderMedicineUpdate( $om_id ) {
        $s_price = isset( $_POST['s_price'] ) ? \round( $_POST['s_price'], 2 ) : 0.00;
        $om_status = isset( $_POST['om_status'] ) ? $_POST['om_status'] : '';

        DB::instance()->update( 't_o_medicines', [ 's_price' => $s_price, 'om_status' => $om_status], [ 'om_id' => $om_id ] );
        
        $this->orderMedicineSingle( $om_id );
    }

    public function orderMedicineDelete( $om_id ) {
        Response::instance()->sendMessage( 'Deleting not alowed. Delete from Order Edit page.');
    }

    public function inventory() {
        if( ! $this->user->can( 'inventoryView' ) ) {
            Response::instance()->sendMessage( 'Your account does not have inventory view capabilities.');
        }
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $category = isset( $_GET['_category'] ) ? $_GET['_category'] : '';
        $c_id = isset( $_GET['_c_id'] ) ? $_GET['_c_id'] : 0;
        $ph_id = isset( $_GET['_ph_id'] ) ? $_GET['_ph_id'] : 0;
        $qty = isset( $_GET['_qty'] ) ? $_GET['_qty'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS ti.*, (100 - (ti.i_price/tm.m_price*100)) as discount_percent, (100 - (ti.i_price/tm.m_d_price*100)) as profit_percent, (ti.i_qty*ti.i_price) as i_price_total, tm.m_id, tm.m_name, tm.m_form, tm.m_unit, tm.m_strength, tm.m_price, tm.m_d_price FROM t_inventory ti INNER JOIN t_medicines tm ON ti.i_m_id = tm.m_id WHERE 1=1' );
        if ( $search ) {
            $search = addcslashes( $search, '_%\\' );
            $db->add( ' AND tm.m_name LIKE ?', "{$search}%" );
        }
        if( $category ) {
            $db->add( ' AND tm.m_category = ?', $category );
        }
        if( $c_id ) {
            $db->add( ' AND tm.m_c_id = ?', $c_id );
        }
        if( $ph_id ) {
            $db->add( ' AND ti.i_ph_id = ?', $ph_id );
        }
        if( '<0' == $qty ) {
            $db->add( ' AND ti.i_qty < ?', 0 );
        } elseif( 'zero' == $qty ) {
            $db->add( ' AND ti.i_qty = ?', 0 );
        } elseif( '>0' == $qty ) {
            $db->add( ' AND ti.i_qty > ?', 0 );
        } elseif( '>100' == $qty ) {
            $db->add( ' AND ti.i_qty > ?', 100 );
        } elseif( '1-10' == $qty ) {
            $db->add( ' AND ti.i_qty BETWEEN ? AND ?', 1, 10 );
        } elseif( '11-100' == $qty ) {
            $db->add( ' AND ti.i_qty BETWEEN ? AND ?', 11, 100 );
        }
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND ti.i_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }

        if( $orderBy && \in_array( $orderBy, [ 'm_name', 'm_form', 'm_unit', 'm_strength', 'm_price', 'm_d_price'] ) ) {
            $db->add( " ORDER BY tm.{$orderBy} $order" );
        } elseif( $orderBy && \in_array($orderBy, ['discount_percent', 'profit_percent', 'i_price_total'] ) ) {
            $db->add( " ORDER BY $orderBy $order" );
        } elseif( $orderBy ) {
            $db->add( " ORDER BY ti.{$orderBy} $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['i_id'];
            $data['ph_name'] = User::getName( $data['i_ph_id'] );
            $data['i_price'] = \round( $data['i_price'], 2 );
            $data['i_price_total'] = \round( $data['i_price_total'] );
            $data['discount_percent'] = \round($data['discount_percent'], 1) . '%';
            $data['profit_percent'] = \round($data['profit_percent'], 1) . '%';

            $data['attachedFiles'] = Functions::getPicUrlsAdmin( $data['m_id'] );

            Response::instance()->appendData( '', $data );
        }

        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No inventory items Found' );
        } else {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function inventorySingle( $i_id ) {
        if( $inventory = Inventory::getInventory( $i_id ) ){
            $data = $inventory->toArray();
            $data['id'] = $inventory->i_id;
            $data['i_price'] = \round( $data['i_price'], 2 );

            if( $medicine = Medicine::getMedicine( $inventory->i_m_id ) ){
                $data['m_name'] = $medicine->m_name;
                $data['m_form'] = $medicine->m_form;
                $data['m_unit'] = $medicine->m_unit;
                $data['m_strength'] = $medicine->m_strength;
            }

            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'No inventory items Found' );
        }
    }

    public function inventoryUpdate( $i_id ) {
        if( ! $this->user->can( 'inventoryEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have inventory edit capabilities.');
        }
        $i_price = isset( $_POST['i_price'] ) ? \round( $_POST['i_price'], 4 ) : 0.0000;
        $i_qty = isset( $_POST['i_qty'] ) ? \intval( $_POST['i_qty'] ) : 0;

        if( $inventory = Inventory::getInventory( $i_id ) ){
            $inventory->i_price = $i_price;
            $inventory->i_qty = $i_qty;
            $inventory->update();
        }
        
        $this->inventorySingle( $i_id );
    }

    public function inventoryDelete( $i_id ) {
        if( ! $this->user->can( 'inventoryEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have inventory edit capabilities.');
        }
        if( $inventory = Inventory::getInventory( $i_id ) ){
            $inventory->delete();
            Response::instance()->sendData( ['id' => $i_id ], 'success');
        } else {
            Response::instance()->sendMessage( 'No inventory items Found' );
        }
    }

    public function inventoryBalance(){
        $query = DB::db()->query( 'SELECT SUM(i_price*i_qty) as totalBalance FROM t_inventory' );
        $balance = $query->fetchColumn();
        $data = [
            'totalBalance' => \round( $balance, 2 ),
        ];
        Response::instance()->sendData( $data, 'success');
    }

    public function purchases() {
        if( ! $this->user->can( 'purchasesView' ) ) {
            Response::instance()->sendMessage( 'Your account does not have purchases view capabilities.');
        }
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $category = isset( $_GET['_category'] ) ? $_GET['_category'] : '';
        $c_id = isset( $_GET['_c_id'] ) ? $_GET['_c_id'] : 0;
        $ph_id = isset( $_GET['_ph_id'] ) ? $_GET['_ph_id'] : 0;
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $expiry = isset( $_GET['_expiry'] ) ? $_GET['_expiry'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS tpu.*, (100 - (tpu.pu_price/tm.m_price*100)) as discount_percent, (100 - (tpu.pu_price/tm.m_d_price*100)) as profit_percent, (tpu.pu_qty*tpu.pu_price) as pu_price_total, tm.m_name, tm.m_form, tm.m_unit, tm.m_strength, tm.m_price, tm.m_d_price FROM t_purchases tpu INNER JOIN t_medicines tm ON tpu.pu_m_id = tm.m_id WHERE 1=1' );
        if ( $search ) {
            $search = addcslashes( $search, '_%\\' );
            if( 0 === \stripos( $search, 'i-' ) ){
                $search = substr( $search, 2 );
                if( $search )
                $db->add( ' AND tpu.pu_inv_id = ?', $search );
            } elseif( 0 === \stripos( $search, 'b-' ) ){
                $search = substr( $search, 2 );
                if( $search )
                $db->add( ' AND tpu.m_batch = ?', $search );
            } else {
                $db->add( ' AND tm.m_name LIKE ?', "{$search}%" );
            }
        }
        if( $category ) {
            $db->add( ' AND tm.m_category = ?', $category );
        }
        if( $c_id ) {
            $db->add( ' AND tm.m_c_id = ?', $c_id );
        }
        if( $ph_id ) {
            $db->add( ' AND tpu.pu_ph_id = ?', $ph_id );
        }
        if( $status ) {
            $db->add( ' AND tpu.pu_status = ?', $status );
        }
        if( 'expired' == $expiry ) {
            $db->add( ' AND tpu.m_expiry BETWEEN ? AND ?', '0000-00-00', \date( 'Y-m-d H:i:s' ) );
        } elseif( 'n3' == $expiry ){
            $db->add( ' AND tpu.m_expiry BETWEEN ? AND ?', \date( 'Y-m-d H:i:s' ), \date( 'Y-m-d H:i:s', strtotime("+3 months") ) );
        } elseif( 'n6' == $expiry ){
            $db->add( ' AND tpu.m_expiry BETWEEN ? AND ?', \date( 'Y-m-d H:i:s' ), \date( 'Y-m-d H:i:s', strtotime("+6 months") ) );
        }
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND tpu.pu_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }

        if( $orderBy && \in_array( $orderBy, [ 'm_name', 'm_form', 'm_unit', 'm_strength', 'm_price', 'm_d_price'] ) ) {
            $db->add( " ORDER BY tm.{$orderBy} $order" );
        } elseif( $orderBy && \in_array($orderBy, ['discount_percent', 'profit_percent', 'pu_price_total'] ) ) {
            $db->add( " ORDER BY $orderBy $order" );
        } elseif( $orderBy ) {
            $db->add( " ORDER BY tpu.{$orderBy} $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['pu_id'];
            $data['ph_name'] = User::getName( $data['pu_ph_id'] );
            $data['pu_price'] = \round( $data['pu_price'], 2 );
            $data['pu_price_total'] = \round( $data['pu_price_total'] );
            $data['discount_percent'] = \round($data['discount_percent'], 1) . '%';
            $data['profit_percent'] = \round($data['profit_percent'], 1) . '%';

            Response::instance()->appendData( '', $data );
        }

        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No purchases items Found' );
        }
    }

    public function purchaseCreate() {
        $items = isset( $_POST['items'] ) ? $_POST['items'] : '';
        if( !\is_array( $items ) ){
            Response::instance()->sendMessage( 'Items are malformed.');
        }

        $ph_id = isset( $_POST['pu_ph_id'] ) ? \intval( $_POST['pu_ph_id'] ) : 0;
        if( !$ph_id ){
            Response::instance()->sendMessage( 'No pharmacy selected.');
        }

        $pu_inv_id = isset( $_POST['pu_inv_id'] ) ? (int)$_POST['pu_inv_id'] : '';
        if( !$pu_inv_id ){
            $pu_inv_id = DB::db()->query('SELECT MAX(pu_inv_id) FROM t_purchases')->fetchColumn() + 1;
            //Response::instance()->sendMessage( 'No invoice number given.');
        }

        $d_percent = isset( $_POST['d_percent'] ) ? \round( $_POST['d_percent'], 8 ) : 0;
        
        $insert = [];
        foreach ( $items as $item ) {
            if( ! \is_array( $item ) ){
                continue;
            }
            $m_id = isset( $item['pu_m_id'] ) ? \intval( $item['pu_m_id'] ) : 0;
            $pu_price = isset( $item['pu_price'] ) ? \round( $item['pu_price'], 4) : 0.0000;
            $pu_qty = isset( $item['pu_qty'] ) ? \intval( $item['pu_qty'] ) : 0;
            $expMonth = isset( $item['expMonth'] ) ? \intval( $item['expMonth'] ) : 0;
            $expYear = isset( $item['expYear'] ) ? \intval( $item['expYear'] ) : 0;
            $batch = isset( $item['batch'] ) ? filter_var($item['batch'], FILTER_SANITIZE_STRING) : '';

            $exp = '0000-00-00';
            if( $expMonth && $expYear && checkdate( $expMonth, 1, $expYear ) ){
                $exp = $expYear . '-' . $expMonth . '-01';
            }

            if( ! $m_id ){
                continue;
            }

            if ( $pu_price && $d_percent ){
                $pu_price = $pu_price - (( $pu_price * $d_percent)/100);
            }

            if( $pu_qty ){
                $per_price = \round( $pu_price / $pu_qty, 4 );
            } else {
                $per_price = 0.0000;
            }

            $insert[] = [
                'pu_inv_id' => $pu_inv_id,
                'pu_ph_id' => $ph_id,
                'pu_m_id' => $m_id,
                'pu_price' => $per_price,
                'pu_qty' => $pu_qty,
                'pu_created' => \date( 'Y-m-d H:i:s' ),
                'm_expiry' => $exp,
                'm_batch' => $batch,
                //'pu_status' => 'pending', //default
            ];
        }
        $id = 0;
        if( $insert ){
            $id = DB::instance()->insertMultiple( 't_purchases', $insert );
        }
        
        Response::instance()->sendData( ['id' => $id], 'success' );
    }

    function purchaseSingle( $pu_id ) {
        if( ! $pu_id ){
            Response::instance()->sendMessage( 'No purchase item Found' );
        }
        $query = DB::db()->prepare( 'SELECT tpu.*, tm.m_name, tm.m_form, tm.m_unit, tm.m_strength, tm.m_price, tm.m_d_price FROM t_purchases tpu INNER JOIN t_medicines tm ON tpu.pu_m_id = tm.m_id WHERE tpu.pu_id = ? LIMIT 1' );
        $query->execute( [ $pu_id ] );
        $data = $query->fetch();
        if( $data ){
            $data['id'] = $data['pu_id'];
            $data['pu_price'] = \round( $data['pu_price'], 2 );
            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'No purchase item Found' );
        }
    }

    public function purchasesSync(){
        $ph_m_ids = [];
        $inv_ids = [];
        DB::db()->beginTransaction();
        try {
            $query = DB::db()->prepare( 'SELECT * FROM t_purchases WHERE pu_status = ? LIMIT 500' );
            $query->execute( [ 'pending' ] );
            $insert= [];
            while( $pu = $query->fetch() ) {
                $ph_m_ids[ $pu['pu_ph_id'] ][] = $pu['pu_m_id'];
                if ( ! isset( $inv_ids[ $pu['pu_inv_id'] ] ) ){
                    $inv_ids[ $pu['pu_inv_id'] ] = 0;
                }
                $inv_ids[ $pu['pu_inv_id'] ] += $pu['pu_price'] * $pu['pu_qty'];

                if( $inventory = Inventory::getByPhMid( $pu['pu_ph_id'], $pu['pu_m_id'] ) ){
                    if(($inventory->i_qty + $pu['pu_qty'])){
                        $inventory->i_price = ( ($inventory->i_price * $inventory->i_qty ) + ($pu['pu_price'] * $pu['pu_qty']) ) / ($inventory->i_qty + $pu['pu_qty']);
                    } else {
                        $inventory->i_price = '0.00';
                    }
                    $inventory->i_qty = $inventory->i_qty + $pu['pu_qty'];
                    $inventory->update();
                } else {
                    $insert[] = [
                        'i_ph_id' => $pu['pu_ph_id'],
                        'i_m_id' => $pu['pu_m_id'],
                        'i_price' => $pu['pu_price'],
                        'i_qty' => $pu['pu_qty'],
                    ];
                }
                $updated = DB::instance()->update( 't_purchases', ['pu_status' => 'sync'], [ 'pu_id' => $pu['pu_id'] ] );
            }
            if( $insert ){
                $i_id = DB::instance()->insertMultiple( 't_inventory', $insert );
            }
            if( $inv_ids ){
                foreach ( $inv_ids as $inv_id => $amount ) {
                    $reason = \sprintf( 'Payment for Invoice %s', $inv_id );
                    Functions::ledgerCreate( $reason, -$amount, 'purchase' );
                }
            }
            DB::db()->commit(); 
        } catch(\PDOException $e) {
            DB::db()->rollBack();
            \error_log( $e->getMessage() );
            Response::instance()->sendMessage( 'Something wrong, Please try again.' );
        }
        
        Functions::checkOrdersForInventory( $ph_m_ids );
        Functions::checkOrdersForPacking();

        Response::instance()->sendMessage( 'Successfully sync.', 'success' );
    }

    public function purchaseUpdate( $pu_id ) {
        if( ! $pu_id ){
            Response::instance()->sendMessage( 'No purchase items Found' );
        }

        $pu_price = isset( $_POST['pu_price'] ) ? \round( $_POST['pu_price'], 4 ) : 0.0000;
        $pu_qty = isset( $_POST['pu_qty'] ) ? \intval( $_POST['pu_qty'] ) : 0;
        $m_expiry = isset( $_POST['m_expiry'] ) ? filter_var($_POST['m_expiry'], FILTER_SANITIZE_STRING) : '0000-00-00';
        $m_batch = isset( $_POST['m_batch'] ) ? filter_var($_POST['m_batch'], FILTER_SANITIZE_STRING) : '';

        $updated = DB::instance()->update( 't_purchases', ['pu_price' => $pu_price, 'pu_qty' => $pu_qty, 'm_expiry' => $m_expiry, 'm_batch' => $m_batch], [ 'pu_id' => $pu_id ] );
        
        $this->purchaseSingle( $pu_id );
    }

    public function purchaseDelete( $pu_id ) {
        if( ! $pu_id ){
            Response::instance()->sendMessage( 'No purchase item Found' );
        }
        $query = DB::db()->prepare( 'SELECT pu_status FROM t_purchases WHERE pu_id = ? LIMIT 1' );
        $query->execute( [ $pu_id ] );
        $data = $query->fetch();

        if( $data && 'sync' == $data['pu_status'] ){
            Response::instance()->sendMessage( 'You can not delete this purchase anymore' );
        }

        $deleted = DB::instance()->delete( 't_purchases', [ 'pu_id' => $pu_id ] );
        
        if( $deleted ){
            Response::instance()->sendData( ['id' => $pu_id ], 'success');
        } else {
            Response::instance()->sendMessage( 'No purchase item Found' );
        }
    }

    public function purchasesPendingTotal(){
        $query = DB::db()->prepare( 'SELECT COUNT(*) as totalItems, SUM(pu_price*pu_qty) as totalAmount FROM t_purchases WHERE pu_status = ?' );
        $query->execute( [ 'pending' ] );
        $purchase = $query->fetch();
        $data = [
            'totalItems' => !empty( $purchase['totalItems'] ) ? (int)$purchase['totalItems'] : 0,
            'totalAmount' => !empty( $purchase['totalAmount'] ) ? \round($purchase['totalAmount'], 2) : 0.00,
        ];
        Response::instance()->sendData( $data, 'success');
    }

    public function collections() {
        if( ! $this->user->can( 'collectionsView' ) ) {
            Response::instance()->sendMessage( 'Your account does not have collections view capabilities.');
        }

        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $fm_id = isset( $_GET['_fm_id'] ) ? $_GET['_fm_id'] : 0;
        $to_id = isset( $_GET['_to_id'] ) ? $_GET['_to_id'] : 0;
        $status = isset( $_GET['_status'] ) ? $_GET['_status'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS *, (co_amount - co_s_amount) AS profit FROM t_collections WHERE 1=1' );
        if( $fm_id ) {
            $db->add( ' AND co_fid = ?', $fm_id );
        }
        if( $to_id ) {
            $db->add( ' AND co_tid = ?', $to_id );
        }
        if( $status ) {
            $db->add( ' AND co_status = ?', $status );
        }
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND co_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }

        if( $orderBy ) {
            $db->add( " ORDER BY {$orderBy} $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['co_id'];
            $data['fm_name'] = User::getName( $data['co_fid'] );
            $data['to_name'] = User::getName( $data['co_tid'] );
            $data['profit'] = \round( $data['profit'], 2);

            Response::instance()->appendData( '', $data );
        }

        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No collections Found' );
        }
    }

    function collectionSingle( $co_id ) {
        $query = DB::db()->prepare( 'SELECT * FROM t_collections WHERE co_id = ? LIMIT 1' );
        $query->execute( [ $co_id ] );
        if( $data = $query->fetch() ){
            $data['id'] = $data['co_id'];
            $data['fm_name'] = User::getName( $data['co_fid'] );
            $data['to_name'] = User::getName( $data['co_tid'] );
            $data['profit'] = \round( $data['co_amount'] - $data['co_s_amount'], 2);

            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'No items Found' );
        }

        Response::instance()->send();
    }

    public function ledger() {
        if( ! $this->user->can( 'ledgerView' ) ) {
            Response::instance()->sendMessage( 'Your account does not have ledger view capabilities.');
        }
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $u_id = isset( $_GET['_u_id'] ) ? $_GET['_u_id'] : 0;
        $created = isset( $_GET['_created'] ) ? $_GET['_created'] : '';
        $type = isset( $_GET['_type'] ) ? $_GET['_type'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_ledger WHERE 1=1' );
        if ( $search ) {
            $search = addcslashes( $search, '_%\\' );
            $db->add( ' AND l_reason LIKE ?', "%{$search}%" );
        }
        if( $u_id ) {
            $db->add( ' AND l_uid = ?', $u_id );
        }
        if( $created ) {
            $db->add( ' AND l_created >= ? AND l_created <= ?', $created . ' 00:00:00', $created . ' 23:59:59' );
        }
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND l_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if( $type ) {
            $db->add( ' AND l_type = ?', $type );
        }

        if( $orderBy ) {
            $db->add( " ORDER BY {$orderBy} $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['l_id'];
            $data['u_name'] = User::getName( $data['l_uid'] );

            foreach ( Functions::getLedgerFiles( $data['l_id'], 'path' ) as $fullPath ) {
                $data['attachedFiles'][] = [
                    'src' => \str_replace( STATIC_DIR, STATIC_URL, $fullPath ),
                    'title' => $this->ledgerFileNameFromPath( $fullPath ),
                ];
            }

            Response::instance()->appendData( '', $data );
        }

        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No items Found' );
        }
    }

    function ledgerSingle( $l_id ) {
        $query = DB::db()->prepare( 'SELECT * FROM t_ledger WHERE l_id = ? LIMIT 1' );
        $query->execute( [ $l_id ] );
        if( $data = $query->fetch() ){
            $data['id'] = $data['l_id'];
            $data['u_name'] = User::getName( $data['l_uid'] );

            foreach ( Functions::getLedgerFiles( $data['l_id'], 'path' ) as $fullPath ) {
                $data['attachedFiles'][] = [
                    'src' => \str_replace( STATIC_DIR, STATIC_URL, $fullPath ),
                    'title' => $this->ledgerFileNameFromPath( $fullPath ),
                ];
            }
            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'No items Found' );
        }

        Response::instance()->send();
    }

    private function ledgerFileNameFromPath( $path ){
        $name = basename( $path );
        list( $name, $ext ) = explode('.', $name );
        $name = substr($name, 0, strrpos($name, '-'));
        $name = substr($name, strpos($name, '-')+1 );
        return "{$name}.{$ext}";
    }

    public function ledgerCreate() {
        if( ! $this->user->can( 'ledgerCreate' ) ) {
            Response::instance()->sendMessage( 'Your account does not have ledger create capabilities.');
        }
        if(empty($_POST['l_reason']) || ! isset($_POST['l_amount']) || empty($_POST['l_type']) ) {
            Response::instance()->sendMessage( 'All Fields Required' );
        }
        
        $l_id = Functions::ledgerCreate( $_POST['l_reason'], $_POST['l_amount'], $_POST['l_type'] );

        $attachedFiles = isset( $_POST['attachedFiles'] ) ? $_POST['attachedFiles'] : [];
        Functions::modifyLedgerFiles( $l_id, $attachedFiles );

        $this->ledgerSingle( $l_id );
    }

    public function ledgerUpdate( $l_id ) {
        if( ! $this->user->can( 'ledgerEdit' ) ) {
            Response::instance()->sendMessage( 'Your account does not have ledger edit capabilities.');
        }

        if( ! $l_id ){
            Response::instance()->sendMessage( 'No items Found' );
        }

        if(empty($_POST['l_reason']) || ! isset($_POST['l_amount']) || empty($_POST['l_type']) ) {
            Response::instance()->sendMessage( 'All Fields Required' );
        }
        DB::instance()->update( 't_ledger', ['l_reason' => $_POST['l_reason'], 'l_type' => $_POST['l_type'], 'l_amount' => \round($_POST['l_amount'], 2)], [ 'l_id' => $l_id ] );

        $attachedFiles = isset( $_POST['attachedFiles'] ) ? $_POST['attachedFiles'] : [];
        Functions::modifyLedgerFiles( $l_id, $attachedFiles );

        $this->ledgerSingle( $l_id );
    }

    public function ledgerDelete( $l_id ) {
        Response::instance()->sendMessage( 'Deleting ledger item is not permitted' );

        if( ! $l_id ){
            Response::instance()->sendMessage( 'No items Found' );
        }
        $deleted = DB::instance()->delete( 't_ledger', [ 'l_id' => $l_id ] );
        
        if( $deleted ){
            Response::instance()->sendData( ['id' => $l_id ], 'success');
        } else {
            Response::instance()->sendMessage( 'No items Found' );
        }
    }

    public function ledgerBalance(){
        $query = DB::db()->query( 'SELECT SUM(l_amount) as totalBalance FROM t_ledger' );
        $balance = $query->fetchColumn();
        $data = [
            'totalBalance' => \round( $balance, 2 ),
        ];
        Response::instance()->sendData( $data, 'success');
    }

    public function companies() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_companies WHERE 1=1' );
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND c_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if ( $search ) {
            $search = addcslashes( $search, '_%\\' );
            $db->add( ' AND c_name LIKE ?', "{$search}%" );
        }

        if( $orderBy ) {
            $db->add( " ORDER BY {$orderBy} $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['c_id'];

            Response::instance()->appendData( '', $data );
        }

        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No companies Found' );
        }
    }

    function companySingle( $c_id ) {
        $query = DB::db()->prepare( 'SELECT * FROM t_companies WHERE c_id = ? LIMIT 1' );
        $query->execute( [ $c_id ] );
        if( $data = $query->fetch() ){
            $data['id'] = $data['c_id'];

            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'No items Found' );
        }

        Response::instance()->send();
    }

    public function generics() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_generics_v2 WHERE 1=1' );
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND g_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }
        if ( $search ) {
            $search = addcslashes( $search, '_%\\' );
            $db->add( ' AND g_name LIKE ?', "{$search}%" );
        }

        if( $orderBy ) {
            $db->add( " ORDER BY {$orderBy} $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();

        while( $data = $query->fetch() ) {
            $data['id'] = $data['g_id'];

            Response::instance()->appendData( '', $data );
        }

        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No companies Found' );
        }
    }

    function genericSingle( $g_id ) {
        $query = DB::db()->prepare( 'SELECT * FROM t_generics_v2 WHERE g_id = ? LIMIT 1' );
        $query->execute( [ $g_id ] );
        if( $data = $query->fetch() ){
            $data['id'] = $data['g_id'];

            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'No items Found' );
        }

        Response::instance()->send();
    }

    public function laterMedicines(){
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $search = isset( $_GET['_search'] ) ? $_GET['_search'] : '';
        $c_id = isset( $_GET['_c_id'] ) ? $_GET['_c_id'] : 0;
        $ph_id = isset( $_GET['_ph_id'] ) ? $_GET['_ph_id'] : 0;
        $orderBy = isset( $_GET['_orderBy'] ) ? $_GET['_orderBy'] : '';
        $order = ( isset( $_GET['_order'] ) && 'DESC' == $_GET['_order'] ) ? 'DESC' : 'ASC';
        $page = isset( $_GET['_page'] ) ? (int)$_GET['_page'] : 1;
        $perPage = isset( $_GET['_perPage'] ) ? (int)$_GET['_perPage'] : 20;

        if( ! $ph_id && 'pharmacy' == $this->user->u_role ){
            $ph_id = Auth::id();
        }

        $db = new DB;
        $db->add( 'SELECT SQL_CALC_FOUND_ROWS tom.om_id, tom.m_id, tom.m_unit, tm.m_name, tm.m_form, tm.m_strength, tm.m_company, tm.m_generic, SUM(tom.m_qty) as total_qty FROM t_o_medicines tom INNER JOIN t_medicines tm ON tom.m_id = tm.m_id INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE 1=1' );
        $db->add( ' AND tr.o_status IN (?,?)', 'processing', 'confirmed' );
        $db->add( ' AND tr.o_i_status IN (?,?,?)', 'ph_fb', 'later', 'confirmed' );
        $db->add( ' AND tom.om_status = ?', 'later' );

        if ( $search ) {
            if( \is_numeric( $search ) ) {
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND tom.o_id LIKE ?', "{$search}%" );
            } else {
                $search = addcslashes( $search, '_%\\' );
                $db->add( ' AND tm.m_name LIKE ?', "{$search}%" );
            }
        }
        if( $c_id ) {
            $db->add( ' AND tm.m_c_id = ?', $c_id );
        }
        if( $ph_id ) {
            $db->add( ' AND tr.o_ph_id = ?', $ph_id );
        }
        if( $ids ) {
            $ids = \array_filter( \array_map( 'intval', \array_map( 'trim', \explode( ',', $ids ) ) ) );
            $in  = str_repeat('?,', count($ids) - 1) . '?';
            $db->add( " AND tom.om_id IN ($in)", ...$ids );

            $perPage = count($ids);
        }

        $db->add( ' GROUP BY tom.m_id' );

        if( $orderBy && \in_array( $orderBy, ['om_id', 'o_id', 'm_unit', 'm_price', 'm_d_price', 's_price', 'om_status'] ) ) {
            $db->add( " ORDER BY tom.{$orderBy} $order" );
        } elseif( $orderBy && \in_array( $orderBy, ['m_name', 'm_form', 'm_c_id', 'm_company', 'm_strength'] ) ) {
            $db->add( " ORDER BY tm.{$orderBy} $order" );
        } elseif( $orderBy && \in_array( $orderBy, ['o_created', 'o_delivered', 'o_status'] ) ) {
            $db->add( " ORDER BY tr.{$orderBy} $order" );
        } elseif( $orderBy && \in_array( $orderBy, ['total_qty'] ) ) {
            $db->add( " ORDER BY total_qty $order" );
        }

        $limit    = $perPage * ( $page - 1 );
        $db->add( ' LIMIT ?, ?', $limit, $perPage );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        while( $data = $query->fetch() ) {
            $data['id'] = $data['om_id'];
            Response::instance()->appendData( '', $data );
        }

        if ( Response::instance()->getData() ) {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            Response::instance()->sendMessage( 'No Medicines Found' );
        }
    }

    function allLocations(){
        $locations = Functions::getLocations();
        foreach( $locations as &$v1 ){
            foreach( $v1 as &$v2 ){
                foreach( $v2 as &$v3 ){
                    //remove postcode, deliveryman id
                    $v3 = [];
                }
            }
        }
        unset( $v1, $v2, $v3 );

        Response::instance()->sendData( $locations, 'success' );
    }

}