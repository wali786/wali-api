<?php

namespace OA\Factory;
use OA\{DB, Cache, Functions};

/**
 * 
 */
class Option {
    
    function __construct()
    {
    }
    
    public function __get( $key ){
		return $this->get( $key );
	}
	
	public static function get( $key, $array = false ){
        $found = false;
        $value = Cache::instance()->get( $key, 'option', false, $found );
        if ( $found ){
            return $value;
        }
        $query = DB::db()->prepare( "SELECT * FROM t_options WHERE option_name = ? LIMIT 1" );
        $query->execute( [ $key ] );
        if( $option = $query->fetch() ){
            $value = Functions::maybeJsonDecode( $option['option_value'] );
            Cache::instance()->set( $key, $value, 'option' );
            return $value;
        } else {
            if( $array ) {
                Cache::instance()->set( $key, [], 'option' );
                return [];
            } else {
                Cache::instance()->set( $key, false, 'option' );
                return false;
            }
        }
	}
    public function __set( $key, $value ){
		return $this->set( $key, $value );
	}
    public static function set( $key, $value ){
        $dbValue = Functions::maybeJsonEncode( $value );
        $query = DB::db()->prepare( "SELECT * FROM t_options WHERE option_name = ? LIMIT 1" );
        $query->execute( [ $key ] );
        if( $option = $query->fetch() ){
            $query2 = DB::db()->prepare( "UPDATE t_options SET option_value = ? WHERE option_name = ?" );
            $query2->execute( [ $dbValue, $key ] );
            if( $query2->rowCount() ) {
                Cache::instance()->set( $key, $value, 'option' );
                return true;
            }
        } else {
            $query2 = DB::db()->prepare( "INSERT INTO t_options (option_name, option_value) VALUES (?, ?)" );
            $query2->execute( [ $key, $dbValue ] );
            Cache::instance()->set( $key, $value, 'option' );
            return true;
        }
        return false;
    }

    public static function delete( $key ) {
        $deleted = DB::instance()->delete( 't_options', [ 'option_name' => $key ] );
        
        if( $deleted ){
            Cache::instance()->delete( $key, 'option' );
        }

        return $deleted;
    }
}
