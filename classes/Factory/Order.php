<?php

namespace OA\Factory;
use OA\{DB, Cache, Functions};

/**
 * 
 */
class Order {
	
	private $o_id = 0;
    private $u_id = 0;
    private $u_name = '';
    private $u_mobile = '';
    private $o_subtotal = '0.00';
    private $o_addition = '0.00';
    private $o_deduction = '0.00';
    private $o_total = '0.00';
    private $o_created = '0000-00-00 00:00:00';
    private $o_updated = '0000-00-00 00:00:00';
    private $o_delivered = '0000-00-00 00:00:00';
    private $o_status = 'processing';
    private $o_i_status = 'dncall';
    private $o_address = '';
    private $o_lat = '';
    private $o_long = '';
    private $o_gps_address = '';
    private $o_payment_method = 'cod';
    private $o_de_id = 0;
    private $o_ph_id = 0;
    
    function __construct( $id = 0 )
    {
        if( $id instanceof self ){
            foreach( $id->toArray() as $k => $v ){
                $this->$k = $v;
            }
        } elseif( is_numeric( $id ) && $id && $order = static::getOrder( $id ) ){
            foreach( $order->toArray() as $k => $v ){
                $this->$k = $v;
            }
        }
    }
    
    public static function getOrder( $id ) {
        if ( ! \is_numeric( $id ) ){
            return false;
        }
        $id = \intval( $id );
        if ( $id < 1 ){
            return false;
        }

        if ( $order = Cache::instance()->get( $id, 'order' ) ){
            return $order;
        }
        
        $query = DB::db()->prepare( 'SELECT * FROM t_orders WHERE o_id = ? LIMIT 1' );
        $query->execute( [ $id ] );
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Order');
        if( $order = $query->fetch() ){     
            Cache::instance()->add( $order->o_id, $order, 'order' );
            return $order;
        } else {
            return false;
        }
    }
    
    public function toArray(){
        $array = [];
        foreach ( \array_keys( \get_object_vars( $this ) ) as $key ) {
            $array[ $key ] = $this->get( $key );
        };
        return $array;
    }
    
    public function exist(){
		return ! empty( $this->o_id );
	}
    
    public function __get( $key ){
		return $this->get( $key );
	}
	
	public function get( $key, $filter = false ){
		if( property_exists( $this, $key ) ) {
			$value = $this->$key;
		} else {
			$value = false;
        }
        switch ( $key ) {
            case 'o_id':
            case 'u_id':
            case 'o_de_id':
            case 'o_ph_id':
                $value = (int) $value;
            break;
            case 'o_subtotal':
            case 'o_addition':
            case 'o_deduction':
            case 'o_total':
                $value = \round( $value, 2 );
            break;
            case 'prescriptions':
                $prescription_dir = STATIC_DIR . '/orders/' . \floor( $this->o_id / 1000 );
                $p_array = glob( $prescription_dir . '/' . $this->o_id . "-*.*", GLOB_NOSORT );
                $value = [];
                foreach ( $p_array as $name ) {
                    $value[] = \str_replace( STATIC_DIR, STATIC_URL, $name );
                }
            break;
            case 'medicineQty':
                $value = [];
                $query = DB::instance()->select( 't_o_medicines', ['o_id' => $this->o_id ], 'm_id, m_qty, om_status, s_price' );
                while ( $om = $query->fetch() ) {
                    $value[] = [
                        'm_id' => $om['m_id'],
                        'qty'  => $om['m_qty'],
                        'om_status' => $om['om_status'],
                        's_price' => $om['s_price'],
                    ];
                }
            break;
            case 'medicines':
                $value = [];
                $query = DB::db()->prepare( 'SELECT tom.om_id, tom.m_id, tom.m_unit, tom.m_price, tom.m_d_price, tom.m_qty, tom.s_price, tom.om_status, tm.m_name, tm.m_form, tm.m_strength, tm.m_company FROM t_o_medicines tom INNER JOIN t_medicines tm ON tom.m_id = tm.m_id WHERE tom.o_id = ? LIMIT 100' );
                $query->execute( [ $this->o_id ] );
                while( $om = $query->fetch() ){
                    $value[ $om['m_id'] ] = [
                        'om_id' => $om['om_id'],
                        'qty' => (int)$om['m_qty'],
                        'name' => $om['m_name'],
                        'form' => $om['m_form'],
                        'company' => $om['m_company'],
                        'strength' => $om['m_strength'],
                        'unit' => $om['m_unit'],
                        'price' => \round( $om['m_price'] * $om['m_qty'], 2),
                        'd_price' => \round( $om['m_d_price'] * $om['m_qty'], 2),
                        's_price' => \round( $om['s_price'] * $om['m_qty'], 2),
                        'm_id' => $om['m_id'],
                        'om_status' => $om['om_status'],
                    ];
                }
            break;
            default:
                break;
        }
		return $value;
	}
    public function __set( $key, $value ){
		return $this->set( $key, $value );
	}
    public function set( $key, $value ){

        switch( $key ){
            case 'o_id':
                return false;
            break;
            case 'u_id':
            case 'o_de_id':
            case 'o_ph_id':
                $value = (int) $value;
            break;
            case 'o_lat':
            case 'o_long':
                $value = \round( $value, 6 );
            break;
            default:
            break;
        }
        
        $return = false;
        
        if( property_exists( $this, $key ) ) {
            $old_value = $this->$key;
            
            if( $old_value != $value ){
                $this->$key = $value;
                $return = true;
            }
        }
        return $return;
    }

    public function cashBackAmount(){
        $cash_back = 0.00;
        if( 'call' == $this->o_i_status ) {
            return $cash_back;
        }
        $o_data = $this->getMeta( 'o_data' );
        if( \is_array( $o_data ) && ! empty( $o_data['cash_back'] ) ){
            $cash_back = \round( $o_data['cash_back'], 2 );
        }
        return $cash_back;
    }

    public function getMeta( $key ) {
        return Meta::get( 'order', $this->o_id, $key );
    }

    public function setMeta( $key, $value ) {
        return Meta::set( 'order', $this->o_id, $key, $value );
    }

    public function insertMetas( $keyValues ) {
        return Meta::insert( 'order', $this->o_id, $keyValues );
    }
    
    public function insert( $data = array() ){
        if( $this->exist() ){
            return false;
        }
        if( is_array( $data ) && $data ){
            foreach( $data as $k => $v ){
                if( property_exists( $this, $k ) ) {
                    $this->set( $k, $v );
                }
            }
        }
        $data_array = $this->toArray();
        $this->o_created = $data_array['o_created'] = \date( 'Y-m-d H:i:s' );
        $this->o_updated = $data_array['o_updated'] = \date( 'Y-m-d H:i:s' );
        
        unset( $data_array['o_id'] );

        $this->o_id = DB::instance()->insert( 't_orders', $data_array );
        
        if( $this->o_id ){
            Cache::instance()->add( $this->o_id, $this, 'order' );

            Functions::orderCreated( $this );
        }

        return $this->o_id;
    }
    public function update( $data = array() ){
        if( ! $this->exist() ){
            return false;
        }
        $order = static::getOrder( $this->o_id );
        if( ! $order ) {
            return false;
        }
        if( 'delivered' == $order->o_status && $order->o_ph_id != $order->o_de_id ) {
            //return false;
        }
        
        if( is_array( $data ) && $data ){
            foreach( $data as $k => $v ){
                if( property_exists( $this, $k ) ) {
                    $this->set( $k, $v );
                }
            }
        }
        $data_array = [];
        foreach ( $this->toArray() as $key => $value) {
            if ( $order->$key != $value ) {
                $data_array[ $key ] = $value;
            }
        }
        unset( $data_array['o_id'] );

        if ( ! $data_array ) {
            return false;
        }
        $this->o_updated = $data_array['o_updated'] = \date( 'Y-m-d H:i:s' );
        if( !empty( $data_array['o_status'] ) && 'delivered' === $data_array['o_status'] ) {
            $this->o_delivered = $data_array['o_delivered'] = \date( 'Y-m-d H:i:s' );
        }
        
        $updated = DB::instance()->update( 't_orders', $data_array, [ 'o_id' => $this->o_id ] );
        
        if( $updated ){
            Cache::instance()->set( $this->o_id, $this, 'order' );
            Functions::sendOrderStatusChangeNotification( $order, $this );
            Functions::calculateCash( $order, $this );
            Functions::referralCash( $order, $this );
            Functions::sendInternalOrderStatusChangeNotification( $order, $this );

            Functions::miscOrderUpdate( $order, $this );
        }

        return $updated;
    }

    public function delete(){
        if( ! $this->exist() ){
            return false;
        }
        
        $deleted = DB::instance()->delete( 't_orders', [ 'o_id' => $this->o_id ] );
        
        if( $deleted ){
            Functions::orderDeleted( $this );
            
            DB::instance()->delete( 't_o_medicines', [ 'o_id' => $this->o_id ] );
            Meta::delete( 'order', $this->o_id );
            Cache::instance()->delete( $this->o_id, 'order' );
        }

        return $deleted;
    }
}
