<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate, PDF, Log};
use GuzzleHttp\Client;

class RouteResponse {

    function __construct() {
    }

    public function dataInitial( $version, $table, $page ) {
        $per_page = 300;
        $limit    = $per_page * ( $page - 1 );
        Response::instance()->addData( 'current', "/$version/data/initial/$table/$page/" );

        switch ( $table ) {
            case 'companies':
                $query = DB::db()->prepare( 'SELECT c_id, c_name FROM t_companies ORDER BY c_id LIMIT ?, ?' );
                $query->execute( [ $limit, $per_page ] );
                $companies = $query->fetchAll();
                if ( count( $companies ) < $per_page ) {
                    Response::instance()->addData( 'next', "/$version/data/initial/generics/1/" );
                } else {
                    Response::instance()->addData( 'next', sprintf('/%s/data/initial/companies/%d/', $version, ++$page ) );
                }
                Response::instance()->addData( 'table', 't_companies' );
                Response::instance()->addData( 'insert', $companies );
                break;
            case 'generics':
                if( 'v1' == $version ){
                    $query = DB::db()->prepare( 'SELECT * FROM t_generics ORDER BY g_id LIMIT ?, ?' );
                } elseif ( 'v2' == $version ) {
                    $query = DB::db()->prepare( 'SELECT * FROM t_generics_v2 ORDER BY g_id LIMIT ?, ?' );
                } else {
                    Response::instance()->sendMessage( 'Something wrong. Plase try again.' );
                }
                $query->execute( [ $limit, $per_page ] );
                $generics = $query->fetchAll();
                if ( count( $generics ) < $per_page ) {
                    Response::instance()->addData( 'next', "/$version/data/initial/medicines/1/" );
                } else {
                    Response::instance()->addData( 'next', sprintf('/%s/data/initial/generics/%d/', $version, ++$page ) );
                }
                if( 'v2' == $version ){
                    foreach ( $generics as &$generic ) {
                        foreach ( $generic as &$value ) {
                            $value = Functions::maybeJsonDecode($value);
                        }
                        unset( $value );
                    }
                    unset( $generic );
                }
                Response::instance()->addData( 'table', 't_generics' );
                Response::instance()->addData( 'insert', $generics );
            break;
            case 'medicines':
                $count = 0;
                $query = DB::db()->prepare( 'SELECT m_id, m_name, m_form, m_strength, m_unit, m_g_id, m_c_id, m_category FROM t_medicines WHERE m_status = ? ORDER BY m_name, m_form DESC, m_strength, m_unit LIMIT ?, ?' );
                $query->execute( [ 'active', $limit, $per_page ] );
                //$query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');

                while( $medicine = $query->fetch() ){
                    //$data = $medicine->toArray();
                    //$data['id'] = $medicine->m_id;
                    Response::instance()->appendData( 'insert', $medicine );
                    $count++;
                }
                if ( $count < $per_page ) {
                    Response::instance()->addData( 'dbVersion', Option::get('dbVersion') );
                } else {
                    Response::instance()->addData( 'next', sprintf('/%s/data/initial/medicines/%d/', $version, ++$page ) );
                }
                Response::instance()->addData( 'table', 't_medicines' );
            break;
            
            default:
                Response::instance()->addData( 'next', "/$version/data/initial/companies/1/" );
                break;
        }
        Response::instance()->setStatus( 'success' );
        Response::instance()->send();
        
    }

    public function dataInitial_v1( $table = 'companies', $page = 1 ) {
        $this->dataInitial( 'v1', $table, $page );
    }
    public function dataInitial_v2( $table = 'companies', $page = 1 ) {
        $this->dataInitial( 'v2', $table, $page );
    }
    public function dataCheck( $dbVersion ) {
        $dbVersion = \intval( $dbVersion );
        $current_dbVersion = \intval( Option::get('dbVersion') );
        
        if( $dbVersion === $current_dbVersion ) {
            Response::instance()->sendMessage( 'No data update available' );
        }
        $v = min( $dbVersion+10, $current_dbVersion ); //Send maximum 10 updates in one call
        $data = [];
        for ($i=$dbVersion+1; $i <= $v; $i++) {
            $db_data = Option::get( "dbData_{$i}", true );
            if( $db_data ) {
                $data = \array_merge_recursive( $data, $db_data );
            }
        }
        Response::instance()->setResponse( 'dbVersion', $v );
        Response::instance()->setData( $data );
        Response::instance()->setStatus( 'success' );
        Response::instance()->send();
    }

    public function home() {
        $carousel = [];
        foreach ( glob( STATIC_DIR . '/images/carousel/*.{jpg,jpeg,png,gif}', GLOB_BRACE ) as $value ) {
            $carousel[] = \str_replace( STATIC_DIR, STATIC_URL, $value ) . '?v=' . @\filemtime($value) ?: 1;
        }
        $deals = [];
        foreach ( [ 27948,27933,27621,27932,27931,27716,27620,27619,28163,28165,28164,27930 ] as $m_id ) {
            if( $medicine = Medicine::getMedicine( $m_id ) ){
                $deals[] = [
                    'id' => $medicine->m_id,
                    'name' => $medicine->m_name,
                    'price' => $medicine->m_price,
                    'd_price' => $medicine->m_d_price,
                    'pic_url' => $medicine->m_pic_url,
                ];
            }
        }
        
        $feature = [];
        foreach ( [ 27931,27932,27360,27336,27369,27525,27716,27619,27458,27374 ] as $m_id ) {
            if( $medicine = Medicine::getMedicine( $m_id ) ){
                $feature[] = [
                    'id' => $medicine->m_id,
                    'name' => $medicine->m_name,
                    'price' => $medicine->m_price,
                    'd_price' => $medicine->m_d_price,
                    'pic_url' => $medicine->m_pic_url,
                ];
            }
        }
        
        Response::instance()->addData( 'carousel', $carousel );
        Response::instance()->addData( 'feature', $feature );
        Response::instance()->addData( 'deals', $deals );
        Response::instance()->setStatus( 'success' );
        Response::instance()->send();
    }

    public function home_v2() {
        Response::instance()->setResponse( 'checkoutNote', '* Estimated Delivery Time: 18-48 hours');
        /*
        Response::instance()->appendData( '', [
            'type' => 'notice',
            'bgColor' => '#FFA07A',
            'color' => '#FF0000',
            'title' => "Due to Covid-19 pandmic some of our delivery is getting delyaed.\nPlease have paitence if your order is not delivered yet.\nWe will reach you ASAP.",
            'data' => [],
        ]);
        */

        $data = [];

        foreach ( glob( STATIC_DIR . '/images/carousel/*.{jpg,jpeg,png,gif}', GLOB_BRACE ) as $value ) {
            $data[] = \str_replace( STATIC_DIR, STATIC_URL, $value ) . '?v=' . @\filemtime($value) ?: 1;
        }

        if( $data ){
            Response::instance()->appendData( '', [
                'type' => 'carousel',
                'title' => '',
                'data' => $data,
            ]);
        }
        Response::instance()->appendData( '', [
            'type' => 'actions',
            'title' => '',
            'data' => [
                //discount percents
                'order' => 10,
                'call' => 10,
                'healthcare' => 30,
                //heading text
                'callTime' => '10am To 10pm',
            ],
        ]);
        $m_ids = [
            [27948,27933,27621,27932,27931,27716,27620,28176,28177,28163,28165,28164,27619,28149,27597,27930],
            [27335,27336,27655,27345,27549,27349,27536,27403,27690,27707,26793,27342,27343,27374],
            [27360,27525,27361,27363,27526,27357,27921,27604,27362,27530],
            [27369,27350,27926,27798,27703,27622,27923,27368,27927,27796,27367],
            [27486,27977,27316,27653,27880,27970,27541,27644,27665],
            [24884,14873,21833,13206,14,26599,26718,26815,6394],
            [27458,27928,27454,27639,27452,27862,27612,27662,27431],

        ];
        $titles = [
            'Covid-19 Special',
            'Diabetes Care',
            'Napkin',
            'Safe Products',
            'Herbal and others',
            "Women's Care",
            'Miscellaneous',
        ];

        CacheUpdate::instance()->add_to_queue( \call_user_func_array('array_merge', $m_ids ), 'medicine' );
        CacheUpdate::instance()->update_cache( [], 'medicine' );

        foreach ( $m_ids as $key => $ids ) {
            $data = [];
            foreach ($ids as $m_id ) {
                if( $medicine = Medicine::getMedicine( $m_id ) ){
                    $data[] = [
                        'id' => $medicine->m_id,
                        'name' => $medicine->m_name,
                        'price' => $medicine->m_price,
                        'd_price' => $medicine->m_d_price,
                        'pic_url' => $medicine->m_pic_url,
                    ];
                }
            }
            if( $data ){
                Response::instance()->appendData( '', [
                    'type' => "sideScroll-{$key}",
                    'title' => isset($titles[$key]) ? $titles[$key] : '',
                    'data' => $data,
                ]);
            }
        }
        Response::instance()->setStatus( 'success' );
        Response::instance()->send();

    }

    public function medicines( $search = '', $page = 0 ) {
        if( ! $search ){
            $search = isset( $_GET['search'] ) ? $_GET['search'] : '';
        }
        if( ! $page ){
            $page = !empty( $_GET['page'] ) ? (int)$_GET['page'] : 1;
        }
        $category = isset( $_GET['category'] ) ? $_GET['category'] : '';

        if( 'healthcare' == $category ){
            $per_page = 12;
        } else {
            $per_page = 10;
        }
        $limit    = $per_page * ( $page - 1 );
        $db = new DB;

        $db->add( 'SELECT * FROM t_medicines WHERE 1=1' );
        if ( $search ) {
            $search = preg_replace('/[^a-z0-9\040\.\-]+/i', ' ', $search);

            //$search = \rtrim( addcslashes( $search, '_%\\' ), '-');
            $org_search = $search = \rtrim( \trim(preg_replace('/\s\s+/', ' ', $search ) ), '-' );

            //$db->add( ' AND ( m_name LIKE ? OR m_generic LIKE ? )', "{$search}%", "{$search}%" );
            //$db->add( ' AND m_name LIKE ?', "{$search}%" );
            if( false === \strpos( $search, ' ' ) ){
                $search .= '*';
            } else {
                $search = '+' . \str_replace( ' ', ' +', $search) . '*';
            }
            if( \strlen( $org_search ) > 2 ){
                $db->add( " AND (MATCH(m_name) AGAINST (? IN BOOLEAN MODE) OR m_name LIKE ?)", $search, "{$org_search}%" );
                //$db->add( " AND MATCH(m_name) AGAINST (? IN BOOLEAN MODE)", $search );
            } elseif( $org_search ) {
                $db->add( ' AND m_name LIKE ?', "{$org_search}%" );
            }
        }
        if( $category ) {
            $db->add( ' AND m_category = ?', $category );
        }
        $db->add( ' AND m_status = ?', 'active' );
        $db->add( ' ORDER BY m_rob DESC, m_category, m_name, m_form DESC, m_strength, m_unit LIMIT ?, ?', $limit, $per_page );
        
        $cache_key = \md5( $db->getSql() . \json_encode($db->getParams()) );
        
        if ( $cache_data = Cache::instance()->get( $cache_key, 'userMedicines' ) ){
            Response::instance()->setData( $cache_data['data'] );
            //Response::instance()->setResponse( 'total', $cache_data['total'] );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }

        $query = $db->execute();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');

        while( $medicine = $query->fetch() ){
            $data = [
                'id' => $medicine->m_id,
                'name' => $medicine->m_name,
                'generic' => $medicine->m_generic,
                'strength' => $medicine->m_strength,
                'form' => $medicine->m_form,
                'company' => $medicine->m_company,
                'unit' => $medicine->m_unit,
                'pic_url' => $medicine->m_pic_url,
                'rx_req' => $medicine->m_rx_req,
                'comment' => $medicine->m_comment,
                'price' => $medicine->m_price,
                'd_price' => $medicine->m_d_price,
            ];
            Response::instance()->appendData( '', $data );
        }
        if ( $all_data = Response::instance()->getData() ) {
            $cache_data = [
                'data' => $all_data,
                //'total' => $total,
            ];
            //pic_url may change. So cache for sort period of time
            Cache::instance()->set( $cache_key, $cache_data, 'userMedicines', 60 * 60 );

            //Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        } else {
            if( $page > 1 ){
                Response::instance()->sendMessage( 'No more medicines Found' );
            } else {
                Response::instance()->sendMessage( 'No medicines Found' );
            }
        }
    }

    public function sameGeneric( $g_id, $page = 1 ){
        $per_page = 10;
        $limit    = $per_page * ( $page - 1 );

        $db = new DB;
        $db->add( 'SELECT m_id, m_name, m_strength, m_form, m_unit, m_generic, m_company  FROM t_medicines WHERE 1=1' );
        $db->add( ' AND m_g_id = ?', $g_id );
        $db->add( ' ORDER BY m_rob DESC, m_name, m_form DESC, m_strength, m_unit LIMIT ?, ?', $limit, $per_page );
        $query = $db->execute();

        while( $medicine = $query->fetch() ){
            $data = [
                'id' => $medicine['m_id'],
                'name' => $medicine['m_name'],
                'generic' => $medicine['m_generic'],
                'strength' => $medicine['m_strength'],
                'form' => $medicine['m_form'],
                'company' => $medicine['m_company'],
                'unit' => $medicine['m_unit'],
                'pic_url' => Functions::getPicUrl( $medicine['m_id'] ),
            ];

            Response::instance()->appendData( '', $data );
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No medicines Found' );
        } else {
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    public function medicineSingle( $version, $m_id ) {

        $m_id = (int)$m_id;
        if ( ! $m_id ) {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        if( $medicine = Medicine::getMedicine( $m_id ) ){
            $medicine->incrCount( 'Viewed' );

            Response::instance()->setStatus( 'success' );
            //$price = $medicine->m_price * (intval($medicine->m_unit));
            //$d_price = ( ( $price * 90 ) / 100 );
            $data = [
                'id' => $medicine->m_id,
                'name' => $medicine->m_name,
                'g_id' => $medicine->m_g_id,
                'generic' => $medicine->m_generic,
                'strength' => $medicine->m_strength,
                'form' => $medicine->m_form,
                'c_id' => $medicine->m_c_id,
                'company' => $medicine->m_company,
                'unit' => $medicine->m_unit,
                'price' => $medicine->m_price,
                'd_price' => $medicine->m_d_price,
                'pic_url' => $medicine->m_pic_url,
                'pic_urls' => $medicine->m_pic_urls,
                'rob' => $medicine->m_rob,
                'rx_req' => $medicine->m_rx_req,
                'r_bought' => $medicine->m_rob ? \round( $medicine->m_id % 70 + \floor(\time()/3600) - 440000 ) : 0,
                'comment' => $medicine->m_comment,
                'category' => $medicine->m_category,
                //'note' => 'Use coupon code "arogga11" at checkout to get 11% cashback',
            ];
            if( 'v1' == $version ){
                $data['description'] = $medicine->m_description;
            } elseif ( 'v2' == $version ) {
                if( 'allopathic' == $medicine->m_category ){
                    $data['description'] = $medicine->m_description_v2;
                    if( ! empty( $data['description']['g_quick_tips'] ) ){
                        $data['description']['brief_description'] = $medicine->m_description_dims;
                    }
                } else {
                    $data['description'] = [ 'html' => (string)$medicine->getMeta( 'description' ) ];
                }
            }
            //$data['sideScroll'] = $this->getSingleMedicineSideScroll( $medicine );

            Response::instance()->addData( 'medicine', $data );
            Response::instance()->addData( 'same_generic', $this->getSingleMedicineSameGeneric( $medicine ) );
            
        } else {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        Response::instance()->send();
    }

    private function getSingleMedicineSideScroll( $medicine ){
        if( ! $medicine || 'allopathic' == $medicine->m_category ){
            return [];
        }
        if ( $cache_data = Cache::instance()->get( 'singleMedicineSideScroll', 'userSameGeneric' ) ){
            return $cache_data;
        }
        $data = [];
        $query = DB::db()->prepare( 'SELECT tm.m_id, tm.m_name, tm.m_price, tm.m_d_price, COUNT(*) c FROM t_medicines tm INNER JOIN t_o_medicines tom ON tm.m_id = tom.m_id  WHERE tm.m_status = ? AND tm.m_category = ? AND tm.m_rob = ? AND tom.om_status = ? GROUP BY tom.m_id ORDER BY c DESC LIMIT 10' );
        $query->execute( [ 'active', 'healthcare', 1, 'available' ] );
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');
        while( $ss_medicine = $query->fetch() ){
            $data[] = [
                'id' => $ss_medicine->m_id,
                'name' => $ss_medicine->m_name,
                'price' => $ss_medicine->m_price,
                'd_price' => $ss_medicine->m_d_price,
                'pic_url' => $ss_medicine->m_pic_url,
            ];
        }
        if( $data ){
            Cache::instance()->set( 'singleMedicineSideScroll', $data, 'userSameGeneric', 60 * 60 );
        }

        return $data;
    }

    private function getSingleMedicineSameGeneric( $medicine ){
        if( ! $medicine || ! $medicine->m_g_id ){
            return [];
        }
        if ( $cache_data = Cache::instance()->get( $medicine->m_id, 'userSameGeneric' ) ){
            return $cache_data;
        }
        $data = [];
        $query = DB::db()->prepare( 'SELECT * FROM t_medicines WHERE m_strength = ? AND m_g_id = ? AND m_form = ? AND m_unit = ? AND m_id <> ? AND m_status = ? ORDER BY m_rob DESC, m_name LIMIT 50' );
        $query->execute( [ $medicine->m_strength, $medicine->m_g_id, $medicine->m_form, $medicine->m_unit, $medicine->m_id, 'active' ] );
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');
        while( $sg_medicine = $query->fetch() ){
            $data[] = [
                'id' => $sg_medicine->m_id,
                'name' => $sg_medicine->m_name,
                'unit' => $sg_medicine->m_unit,
                'company' => $sg_medicine->m_company,
                'price' => $sg_medicine->m_price,
                'd_price' => $sg_medicine->m_d_price,
                'rob' => $sg_medicine->m_rob,
                //'pic_url' => $sg_medicine->m_pic_url,
            ];
        }
        if( $data ){
            Cache::instance()->set( $medicine->m_id, $data, 'userSameGeneric', 60 * 60 * 24 );
        }

        return $data;
    }

    public function medicineSingle_v1( $m_id ) {
        $this->medicineSingle( 'v1', $m_id );
     }

    public function medicineSingle_v2( $m_id ) {
       $this->medicineSingle( 'v2', $m_id );
    }

    public function medicinePrice( $m_ids ) {
        $m_ids = \array_filter( \array_map( 'trim', \explode( ',', $m_ids ) ) );
        $in  = str_repeat('?,', count($m_ids) - 1) . '?';
        if ( ! $m_ids ) {
            Response::instance()->sendMessage( 'No medicines Found' );
        }

        $query = DB::db()->prepare( "SELECT m_id, m_price, m_d_price, m_rob, m_rx_req, m_comment FROM t_medicines WHERE m_id IN ($in)" );
        $query->execute( $m_ids );
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');
        while( $medicine = $query->fetch() ){
            Response::instance()->setStatus( 'success' );
            //$price = $medicine->m_price * (intval($medicine->m_unit));
            //$d_price = ( ( $price * 90 ) / 100 );
            $data = [
                'price' => $medicine->m_price,
                'd_price' => $medicine->m_d_price,
                //'d_price' => \round( $medicine->m_d_price, 2 ),
                'pic_url' => $medicine->m_pic_url,
                'rx_req' => $medicine->m_rx_req,
                //'r_bought' => \rand( 100,800),
                'r_bought' => $medicine->m_rob ? \round( $medicine->m_id % 70 + \floor(\time()/3600) - 439625 ) : 0,
                'rob' => $medicine->m_rob,
                'comment' => $medicine->m_comment,
            ];
            Response::instance()->addData( $medicine->m_id, $data );
        }
        Response::instance()->send();
    }

    public function medicineSuggest() {
        $name = isset( $_POST['name'] ) ? \strip_tags( $_POST['name'] ) : '';
        $generic = isset( $_POST['generic'] ) ? \strip_tags( $_POST['generic'] ) : '';
        $strength = isset( $_POST['strength'] ) ? \strip_tags( $_POST['strength'] ) : '';
        $form = isset( $_POST['form'] ) ? \strip_tags( $_POST['form'] ) : '';
        $company = isset( $_POST['company'] ) ? \strip_tags( $_POST['company'] ) : '';

        if ( ! $name || ! $form ){
            Response::instance()->sendMessage( 'Name and Form are required.');
        }
        if ( ! Auth::id() ){
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Login required.');
        }
        $medicine = new Medicine;
        $medicine->m_name = $name;
        //$medicine->m_generic = $generic;
        $medicine->m_strength = $strength;
        $medicine->m_form = $form;
        //$medicine->m_company = $company;
        $medicine->m_status = 'suggested';
        $medicine->m_u_id = Auth::id();
        $medicine->insert();
        Response::instance()->sendMessage( 'Thank you. we will manually review this medicine and update.', 'success' );
    }

    function dicountCheck() {
        $d_code = isset( $_POST['d_code'] ) ?  $_POST['d_code'] : '';

        if ( ! $d_code ){
            Response::instance()->sendMessage( 'd_code is required.');
        }
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        $discount = Discount::getDiscount( $d_code );
        if ( $discount && $discount->canUserUse( $user->u_id ) ) {
            $data = [
                'code' => $d_code,
                'type' => $discount->d_type,
                'amount' => $discount->d_amount,
                'max'    => $discount->d_max,
            ];
            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'wrong discount code.');
        }
    }

    function cartDetails() {
        $medicines = isset( $_POST['medicines'] ) ?  $_POST['medicines'] : '';
        $d_code = isset( $_POST['d_code'] ) ?  $_POST['d_code'] : '';
        $s_address = ( isset( $_POST['s_address'] ) && is_array($_POST['s_address']) )? $_POST['s_address']: [];

        if ( ! $medicines ){
            Response::instance()->sendMessage( 'medicines are required.');
        }
        if ( ! is_array( $medicines ) ){
            Response::instance()->sendMessage( 'medicines need to be an array with m_id as key and quantity as value.');
        }

        Response::instance()->sendData( Functions::cartData( User::getUser( Auth::id() ), $medicines, $d_code, 0, false, ['s_address' => $s_address] ), 'success' );
    }

    function closest( $role, $lat, $long ) {
        if( ! $role || ! $lat || ! $long ) {
            return 0;
        }
        $query = DB::db()->prepare( "SELECT u_id FROM t_users WHERE u_role = ? AND u_status = ? ORDER BY (ABS(u_lat - ?) + ABS(u_long - ?)) ASC LIMIT 1" );
        $query->execute( [ $role, 'active', $lat, $long ] );
        if( $u_id = $query->fetchColumn() ){
            return $u_id;
        } else {
            return 0;
        }
    }

    function getDeIdByLocation( $division, $district, $area ){
        if( !$division || !$district || !$area ){
            return 0;
        }

        $locations = Functions::getLocations();
        if( ! isset( $locations[ $division ] ) || ! isset( $locations[ $division ][ $district ] ) || ! isset( $locations[ $division ][ $district ][ $area ] ) ){
            return 0;
        }
        return (int) $locations[ $division ][ $district ][ $area ]['l_de_id'];
    }

    function orderAdd() {
        //Response::instance()->sendMessage( "Dear valued clients.\nOur Dhaka city operation will resume from 29th November 2020.\nThanks for being with Arogga.");
        //Response::instance()->sendMessage( "Due to some unavoidable circumstances we cannot take orders now. We will send you a notification once we start taking orders.\nSorry for this inconvenience.");
        //Response::instance()->sendMessage( "Due to covid19 outbreak, there is a severe short supply of medicine.\nUntil regular supply of medicine resumes, we may not take anymore orders.\nSorry for this inconvenience.");
        //Response::instance()->sendMessage( "Due to Software maintainance we cannot receive orders now.\nPls try after 24 hours. We will be back!!");
        //Response::instance()->sendMessage( "Due to Software maintainance we cannot receive orders now.\nPlease try again after 2nd Jun, 11PM. We will be back!!");
        //Response::instance()->sendMessage( "Due to recent coronavirus outbreak, we are facing delivery man shortage.\nOnce our delivery channel is optimised, we may resume taking your orders.\nThanks for your understanding.");
        //Response::instance()->sendMessage( "Due to EID holiday we cannot receive orders now.\nPlease try again after EID. We will be back!!");
        //Response::instance()->sendMessage( "Due to EID holiday we cannot receive orders now.\nPlease try again after 28th May, 10PM. We will be back!!");

        $medicines = ( isset( $_POST['medicines'] ) && is_array( $_POST['medicines'] ) ) ?  $_POST['medicines'] : [];
        $d_code = isset( $_POST['d_code'] ) ?  $_POST['d_code'] : '';
        $prescriptions = isset( $_FILES['prescriptions'] ) ? $_FILES['prescriptions'] : [];

        $name = isset( $_POST['name'] ) ?  filter_var($_POST['name'], FILTER_SANITIZE_STRING) : '';
        $mobile = isset( $_POST['mobile'] ) ?  filter_var($_POST['mobile'], FILTER_SANITIZE_STRING) : '';
        $address = isset( $_POST['address'] ) ?  filter_var($_POST['address'], FILTER_SANITIZE_STRING) : '';
        $lat = isset( $_POST['lat'] ) ?  filter_var($_POST['lat'], FILTER_SANITIZE_STRING) : '';
        $long = isset( $_POST['long'] ) ?  filter_var($_POST['long'], FILTER_SANITIZE_STRING) : '';
        $gps_address = isset( $_POST['gps_address'] ) ?  filter_var($_POST['gps_address'], FILTER_SANITIZE_STRING) : '';
        $s_address = ( isset( $_POST['s_address'] ) && is_array($_POST['s_address']) )? $_POST['s_address']: [];
        $monthly = !empty( $_POST['monthly'] ) ?  1 : 0;
        $med_pres = ( isset( $_POST['med_pres'] ) && in_array($_POST['med_pres'], ['call', 'dncall']) ) ? $_POST['med_pres'] : 'dncall';
        $payment_method = ( isset( $_POST['payment_method'] ) && in_array($_POST['payment_method'], ['cod', 'online']) ) ? $_POST['payment_method'] : 'cod';

        if ( ! $medicines ) {
            //$med_pres = 'call';
        }

        if ( ! $name || ! $mobile ){
            Response::instance()->sendMessage( 'name and mobile are required.');
        }

        if ( ! $address && ( ! $lat || ! $long ) ){
            Response::instance()->sendMessage( 'Address is required.');
        }

        /*
        if ( $lat && $long ){
            if( Functions::isInside( $lat, $long, 'chittagong' ) ){
                Response::instance()->sendMessage( "Our Chattogram operation temporarily off due to some unavoidable circumstances. We will send you a notification once our Chattogram operation resumes.\nSorry for this inconvenience.");
            }
            if( ! Functions::isInside( $lat, $long ) ){
                Response::instance()->sendMessage( "Our delivery service comming to this area very soon, please stay with us.");
            }
        }
        */
        if ( ! $medicines && ! $prescriptions ){
            Response::instance()->sendMessage( 'medicines or prescription are required.');
        }
        if ( $medicines && ! is_array( $medicines ) ){
            Response::instance()->sendMessage( 'medicines need to be an array with m_id as key and quantity as value.');
        }
        if ( $prescriptions && ! is_array( $prescriptions ) ){
            Response::instance()->sendMessage( 'prescription need to be an file array.');
        }
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        if ( 'blocked' == $user->u_status ){
            Response::instance()->sendMessage( 'You are blocked. Please contact customer care.');
        }
        /*
        $order_check = DB::db()->prepare( "SELECT o_id FROM t_orders WHERE u_id = ? AND o_status = ? LIMIT 1" );
        $order_check->execute( [ Auth::id(), 'processing' ] );
        if( $order_check->fetch() ){
            Response::instance()->sendMessage( 'Please wait until your current order is confirmed. After that you can submit another order OR call customer care if you need further assistance.');
        }
        */
        $discount = Discount::getDiscount( $d_code );

        if( ! $discount || ! $discount->canUserUse( $user->u_id ) ) {
            $d_code = '';
        }
        if ( $name && $user->u_name !== $name ) {
            $user->u_name = $name;
        }
        if ( ! $user->u_mobile && $mobile ) {
            $m_user = User::getBy( 'u_mobile', $mobile );
            if ( $m_user ) {
                Response::instance()->sendMessage( 'Sorry for this but this number is already registered with another account. Please sign in with that account if it is you or login with your own phone number.');
            } else {
                $user->u_mobile = $mobile;
            }
        }
        if ( $lat && $user->u_lat != $lat ) {
            $user->u_lat = $lat;
        }
        if ( $long &&  $user->u_long != $long ) {
            $user->u_long = $long;
        }

        $files_to_save = [];
        if ( $prescriptions ) {
            if ( empty( $prescriptions['tmp_name'] ) || ! is_array( $prescriptions['tmp_name'] ) ) {
                Response::instance()->sendMessage( 'prescription need to be an file array.');
            }
            if ( count( $prescriptions['tmp_name'] ) > 5 ) {
                Response::instance()->sendMessage( 'Maximum 5 prescription pictures allowed.');
            }
            $i = 1;
            foreach( $prescriptions['tmp_name'] as $key => $tmp_name ) {
                if( ! $tmp_name ) {
                    continue;
                }
                if ( UPLOAD_ERR_OK !== $prescriptions['error'][$key] ) {
                    Response::instance()->sendMessage( \sprintf('Upload error occured when upload %s. Please try again', \strip_tags( $prescriptions['name'][$key] ) ) );
                }
                $size = \filesize( $tmp_name );
                if( $size < 12 ) {
                    Response::instance()->sendMessage( \sprintf('File %s is too small.', \strip_tags( $prescriptions['name'][$key] ) ) );
                } elseif ( $size > 10 * 1024 * 1024 ) {
                    Response::instance()->sendMessage( \sprintf('File %s is too big. Maximum size is 10MB.', \strip_tags( $prescriptions['name'][$key] ) ) );
                }
                $imagetype = exif_imagetype( $tmp_name );
                $mime      = ( $imagetype ) ? image_type_to_mime_type( $imagetype ) : false;
                $ext       = ( $imagetype ) ? image_type_to_extension( $imagetype ) : false;
                if( ! $ext || ! $mime ) {
                    Response::instance()->sendMessage( 'Only prescription pictures are allowed.');
                }
                $files_to_save[ $tmp_name ] = $i++ . Functions::randToken( 'alnumlc', 12 ) . $ext;
            }
        }

        $cart_data = Functions::cartData( $user, $medicines, $d_code, 0, false, ['s_address' => $s_address] );
        if ( ! empty( $cart_data['rx_req'] ) && ! $files_to_save ) {
            Response::instance()->sendMessage( 'Rx required.');
        }
        if( isset($cart_data['deductions']['cash']) && !empty($cart_data['deductions']['cash']['info'])) {
            $cart_data['deductions']['cash']['info'] = "Didn't apply because the order value was less than ৳499.";
        }
        if( isset($cart_data['additions']['delivery']) && !empty($cart_data['additions']['delivery']['info'])) {
            $cart_data['additions']['delivery']['info'] = str_replace('To get free delivery order more than', 'Because the order value was less than', $cart_data['additions']['delivery']['info']);
        }
        $c_medicines = $cart_data['medicines'];
        unset( $cart_data['medicines'] );

        $order = new Order;
        $order->u_id = $user->u_id;
        $order->u_name = $user->u_name;
        $order->u_mobile = $user->u_mobile;
        $order->o_subtotal = $cart_data['subtotal'];
        $order->o_addition = $cart_data['a_amount'];
        $order->o_deduction = $cart_data['d_amount'];
        $order->o_total = $cart_data['total'];
        $order->o_status = 'processing';
        $order->o_i_status = $med_pres;
        $order->o_address = $address;
        $order->o_gps_address = $gps_address;
        $order->o_lat = $lat;
        $order->o_long = $long;
        $order->o_payment_method = $payment_method;

        /*
        if( $p_id = $this->closest( 'pharmacy', $lat, $long ) ) {
            $order->o_ph_id = $p_id;
        }
        */
        //Currently we have only one pharmacy
        $order->o_ph_id = 6139;

        if( !isset( $s_address['district'] ) ){
            if( $d_id = $this->closest( 'delivery', $lat, $long ) ){
                $order->o_de_id = $d_id;
            }
        } elseif( $s_address['district'] != 'Dhaka City' ){
            //Outside Dhaka delivery ID
            $order->o_de_id = 9236;
        } elseif( $d_id = $this->getDeIdByLocation( $s_address['division'], $s_address['district'], $s_address['area'] ) ) {
            $order->o_de_id = $d_id;
        }
        $user->update();
        $order->insert();
        Functions::ModifyOrderMedicines( $order, $c_medicines );
        $meta = [
            'o_data' => $cart_data,
            'o_secret' => Functions::randToken( 'alnumlc', 16 ),
            's_address' => $s_address,
        ];
        if( $d_code ) {
            $meta['d_code'] = $d_code;
        }
        if( $monthly ) {
            $meta['subscriptionFreq'] = 'monthly';
        }
        $order->insertMetas( $meta );

        $cash_back = $order->cashBackAmount();

        if ( $files_to_save ) {
            $upload_folder = STATIC_DIR . '/orders/' . \floor( $order->o_id / 1000 );

            if ( ! is_dir($upload_folder)) {
                @mkdir($upload_folder, 0755, true);
            }
            foreach ( $files_to_save as $tmp_name => $new_file ) {
                $new_file = \sprintf( '%s/%s-%s', $upload_folder, $order->o_id, $new_file );
                if( @ move_uploaded_file( $tmp_name, $new_file ) ) {
                    // Set correct file permissions.
                    $stat  = stat( dirname( $new_file ) );
                    $perms = $stat['mode'] & 0000666;
                    @ chmod( $new_file, $perms );
                }  
            }
        }
        //Get user again, User data may changed
        $user = User::getUser( Auth::id() );

        $message = 'Order added successfully.';
        if ( 'call' === $med_pres && $files_to_save ) {
            $message = "Thank you for submitting prescription. You will receive a call shortly from our representatives.\nNote: Depending on the order value,  you may receive cashback from arogga.";
        } else {
            if ( $cash_back ) {
                $user->u_p_cash = $user->u_p_cash + $cash_back;
                $message = "Congratulations!!! You have received a cashback of ৳{$cash_back} from arogga. The cashback will be automatically applied at your next order.";
                Functions::sendNotification( $user->fcm_token, 'Cashback Received.', $message );
            }
        }
        
        if( isset($cart_data['deductions']['cash']) ){
            $user->u_cash = $user->u_cash - $cart_data['deductions']['cash']['amount'];
        }

        $user->update();
        $data = $user->toArray();
        $data['authToken'] = $user->authToken();

        Response::instance()->setStatus( 'success' );
        Response::instance()->setMessage( $message );
        Response::instance()->addData( 'user', $data );
        Response::instance()->send();
    }

    function orderSingle( $o_id ) {
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }

        if( ! \in_array( Auth::id(), [ $order->u_id, $order->o_de_id, $order->o_ph_id ] ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }
        
        $data = $order->toArray();
        $data['prescriptions'] = $order->prescriptions;
        $data['o_data'] = (array)$order->getMeta( 'o_data' );
        $data['o_data']['medicines'] = $order->medicines;
        $data['s_address'] = $order->getMeta('s_address')?:[];
        
        $secret = $order->getMeta( 'o_secret' );
        if( ! $secret ) {
            $secret = Functions::randToken( 'alnumlc', 16 );
            $order->setMeta( 'o_secret', $secret );
        }
        $data['invoiceUrl'] = \sprintf( SITE_URL . '/v1/invoice/%d/%s/', $order->o_id, $secret );
        $data['paymentStatus'] = ( 'paid' === $order->o_i_status || 'paid' === $order->getMeta( 'paymentStatus' ) ) ? 'paid' : '';
        if( \in_array( $order->o_status, [ 'confirmed', 'delivering', 'delivered' ] ) && 'confirmed' == $order->o_i_status && 'paid' !== $order->getMeta( 'paymentStatus' ) ){
            $data['paymentUrl'] = \sprintf( SITE_URL . '/payment/v1/%d/%s/', $order->o_id, $secret );
        }

        if( 'user' !== $user->u_role ) {
            $data['o_i_note'] = (string)$order->getMeta('o_i_note');
        }

        if( 'pharmacy' == $user->u_role ) {
            $query = DB::db()->prepare( 'SELECT SUM(s_price*m_qty) FROM t_o_medicines WHERE o_id = ? AND om_status = ?' );
            $query->execute( [ $order->o_id, 'available' ] );
            $sum = $query->fetchColumn();
            $data['o_data']['a_message'] = "Pharmacy Total = $sum";
        }
        Response::instance()->sendData( $data, 'success' );
    }

    public function invoice( $o_id, $secret ) {
        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }

        if( $order->getMeta( 'o_secret' ) != $secret ){
            Response::instance()->sendMessage( 'No orders found.' );
        }

        if ( ! ( $user = User::getUser( $order->u_id ) ) ) {
            Response::instance()->sendMessage( 'No order user found.' );
        }

        $o_data = (array)$order->getMeta( 'o_data' );
        $deductions = isset( $o_data['deductions'] ) ? $o_data['deductions'] : [];
        $additions = isset( $o_data['additions'] ) ? $o_data['additions'] : [];
        $address = $order->o_gps_address;
        if( $order->o_gps_address && $order->o_address ){
            $address .= "\n";
        }
        $address .= $order->o_address;

        $pdf = new PDF();
        $pdf->SetTitle( 'Invoice-' . $order->o_id . '.pdf' );
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetFont('Times','B',10);

        $pdf->Cell(63,5,'Bill From', 0, 0, 'L');
        $pdf->Cell(23,5,'', 0, 0, 'L');
        $pdf->Cell(41,5,'', 0, 0, 'L');
        $pdf->Cell(63,5,'Bill To', 0, 1, 'L');

        $pdf->SetFont('Times','',10);

        $pdf->Cell(63,5,'Arogga Limited', 0, 0, 'L');
        $pdf->Cell(23,5,'Order ID:', 0, 0, 'L');
        $pdf->Cell(41,5,$order->o_id, 0, 0, 'L');
        $pdf->Cell(63,5,$user->u_name, 0, 1, 'L');

        $pdf->Cell(63,5,'+8801810117100', 0, 0, 'L');
        $pdf->Cell(23,5,'Order Date:', 0, 0, 'L');
        $pdf->Cell(41,5, \date('d/m/Y', \strtotime($order->o_created) ), 0, 0, 'L');
        $pdf->Cell(63,5,$user->u_mobile, 0, 1, 'L');
        
        $pdf->Cell(63,5,'www.arogga.com', 0, 0, 'L');
        $pdf->Cell(23,5,'Invoice Date:', 0, 0, 'L');
        $pdf->Cell(41,5,\date('d/m/Y'), 0, 0, 'L');
        $pdf->MultiCell(63,5,$address, 0, 'L');

        $pdf->Ln(10);

        $pdf->SetFont('Times','B',9);

        $pdf->Cell(20,5,'SL No.', 1, 0, 'C');
        $pdf->Cell(65,5,'Medicine', 1, 0, 'C');
        $pdf->Cell(30,5,'Quantity', 1, 0, 'C');
        $pdf->Cell(25,5,'MRP', 1, 0, 'C');
        $pdf->Cell(25,5,'Discount', 1, 0, 'C');
        $pdf->Cell(25,5,'Amount', 1, 1, 'C');

        $pdf->SetFont('Times','',9);

        $pdf->SetWidths([20,65,30,25,25,25]);
        $pdf->SetAligns(['C','L','L','R','R','R']);
        $i = 1;
        foreach ( $order->medicines as $medicine ) {
            $pdf->Row( [
                $i++,
                $medicine['name'] . '-' . $medicine['strength'],
                Functions::qtyText( $medicine['qty'], $medicine),
                $medicine['price'],
                \round( $medicine['price']-$medicine['d_price'], 2),
                $medicine['d_price'],
            ]);
        }

        $pdf->Ln(10);

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $pdf->Image( STATIC_DIR . '/play_app_store.png',null,null,45);
        $pdf->SetXY($x,$y);

        $pdf->SetWidths([60,30]);
        $pdf->SetAligns(['L','R']);

        $pdf->Cell(100);
        $pdf->Row( [ 'Subtotal', $order->o_subtotal ] );

        foreach ( $deductions as $deduction ) {
            $pdf->Cell(100);
            $pdf->Row( [ $deduction['text'] ."\n". str_replace( '৳', '', $deduction['info'] ), '-' . $deduction['amount'] ] );
        }
        if( $additions ) {
            $pdf->Cell(100);
            $pdf->Row( [ 'Total order value', $order->o_total - $order->o_addition ] );
        }

        foreach ($additions as $addition ) {
            $pdf->Cell(100);
            $pdf->Row( [ $addition['text'] ."\n". str_replace( '৳', '', $addition['info'] ), $addition['amount'] ] );
        }
        $pdf->SetFont('Times','B',8);
        $pdf->Cell(100);
        $pdf->Row( [ 'Amount Payable', $order->o_total ] );

        // cashback
        if( isset($o_data['cash_back']) && $o_data['cash_back'] ){
            $pdf->Ln(40);
            $pdf->SetFont('Arial', 'B', 20);
            $pdf->Cell(190,5, $o_data['cash_back'].' Taka Cashback Rewarded For This Order', 0, 1, 'C');
            $pdf->Ln(3);
            $pdf->SetFont('Arial', 'I', 10);
            $pdf->Cell(190,5,'* N.B: Cashback will be applicable at your next Order', 0, 1, 'C');
        }

        $pdf->Output( 'I', 'Invoice-' . $order->o_id . '.pdf' );

        Log::instance()->insert([
            'log_response_code' => 200,
            'log_response' => 'Invoice-' . $order->o_id . '.pdf',
        ]);
        exit;
    }

    function orders( $status = 'all', $page = 1 ) {
        $per_page = 10;
        $page     = (int) $page;
        $limit    = $per_page * ( $page - 1 );

        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }

        $db = new DB;

        $db->add( 'SELECT * FROM t_orders WHERE u_id = ?', Auth::id() );
        if ( 'all' !== $status ) {
            $db->add( ' AND o_status = ?', $status );
        }
        $db->add( ' AND o_id >= ?', 4287 );
        $db->add( ' ORDER BY o_id DESC' );
        $db->add( ' LIMIT ?, ?', $limit, $per_page );
        
        $query = $db->execute();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Order');

        while( $order = $query->fetch() ){
            $data = $order->toArray();
            if( 'processing' == $order->o_status && ( \strtotime( $order->o_created ) + ( 10 * 60 ) ) < \strtotime( \date( 'Y-m-d H:i:s' ) ) ){
                $data['o_status'] = 'awaiting feedback';
            }
            //TO-DO: Later queue all ids then get meta so that it will use one query
            $data['o_note'] = (string)$order->getMeta('o_note');
            $data['paymentStatus'] = ( 'paid' === $order->o_i_status || 'paid' === $order->getMeta( 'paymentStatus' ) ) ? 'paid' : '';

            if( \in_array( $order->o_status, [ 'confirmed', 'delivering', 'delivered' ] ) && 'confirmed' == $order->o_i_status && 'paid' !== $order->getMeta( 'paymentStatus' ) ){
                $secret = $order->getMeta( 'o_secret' );
                if( ! $secret ) {
                    $secret = Functions::randToken( 'alnumlc', 16 );
                    $order->setMeta( 'o_secret', $secret );
                }
                $data['paymentUrl'] = \sprintf( SITE_URL . '/payment/v1/%d/%s/', $order->o_id, $secret );
            }
            Response::instance()->appendData( '', $data );
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Orders Found' );
        } else {
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function cashBalance(){
        $user = User::getUser( Auth::id() );
        if( ! $user ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'No users Found' );
        }
        
        $data = [
            'u_cash' => \round( $user->u_cash, 2 ),
            'u_p_cash' => \round( $user->u_p_cash, 2 ),
        ];
        Response::instance()->sendData( $data, 'success' );
    }

    function collections() {

        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }

        $page = isset( $_GET['page'] ) ? (int)$_GET['page'] : '';
        $per_page = 10;
        $limit    = $per_page * ( $page - 1 );

        $query = DB::db()->prepare( 'SELECT * FROM t_collections WHERE co_fid = ? OR co_tid = ? ORDER BY co_created DESC LIMIT ?,?' );
        $query->execute( [ Auth::id(), Auth::id(), $limit, $per_page ] );

        while( $data = $query->fetch() ){
            if ( !empty($data['co_fid']) && ( $user = User::getUser( $data['co_fid'] ) ) ) {
                $data['co_fname'] = $user->u_name;
            }
            if ( !empty($data['co_tid']) && ( $user = User::getUser( $data['co_tid'] ) ) ) {
                $data['co_tname'] = $user->u_name;
            }
            unset( $data['o_ids'] );

            Response::instance()->appendData( '', $data );
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Collections Found' );
        } else {
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function location(){
        $lat = isset( $_GET['lat'] ) ? $_GET['lat'] : 0;
        $long = isset( $_GET['long'] ) ? $_GET['long'] : 0;
        if( ! $lat || ! $lat ){
            Response::instance()->sendMessage( 'Invalid location.' );
        }
        /*
        if( Functions::isInside( $lat, $long, 'chittagong' ) ){
            Response::instance()->sendMessage( "Our Chattogram operation temporarily off due to some unavoidable circumstances. We will send you a notification once our Chattogram operation resumes.\nSorry for this inconvenience.");
        }
        if( ! Functions::isInside( $lat, $long ) ){
            Response::instance()->sendMessage( "Our delivery service comming to this area very soon, please stay with us.");
        }
        */
        $client = new Client();
        $res = $client->get( \sprintf( 'https://barikoi.xyz/v1/api/search/reverse/geocode/server/%s/place', BARIKOI_API_KEY ), [
            'query' => [
                'latitude' => $lat,
                'longitude' => $long,
                'post_code' => 'true',
            ],
        ]);
        if( 200 !== $res->getStatusCode() ){
            Response::instance()->sendMessage( 'Something went wrong, Please try again.' );
        }
        $body = Functions::maybeJsonDecode( $res->getBody()->getContents() );
        if( ! $body || ! \is_array( $body ) || 200 !== $body['status'] ){
            Response::instance()->sendMessage( 'Something went wrong, Please try again' );
        }
        $location = [];
        if( ! empty( $body['place']['address'] ) ){
            $location[] = trim($body['place']['address']);
        }
        if( ! empty( $body['place']['area'] ) ){
            $location[] = trim($body['place']['area']);
        }
        if( ! empty( $body['place']['city'] ) ){
            $location[] = trim($body['place']['city']);
        }
        $postCode = ! empty( $body['place']['postCode'] ) ? trim($body['place']['postCode']) : '';
        $data = Functions::getAddressByPostcode( $postCode, trim($body['place']['area']) );

        $location = \array_unique( \array_filter( $location ) );
        if( ! $location ){
            Response::instance()->sendMessage( 'No address found, Please try again.' );
        }
        $data['homeAddress'] = ! empty( $body['place']['address'] ) ? trim($body['place']['address']) : '';
        $data['location'] = \implode( ', ', $location );
        $data['place'] = $body['place'];
        
        Response::instance()->sendData( $data, 'success' );
    }

    function profile() {
        $user = User::getUser( Auth::id() );
        if( ! $user ){
            Response::instance()->sendMessage( 'You are not logged in' );
        }
        $data = $user->toArray();
        $data['authToken'] = $user->authToken();
        $data['u_pic_url'] =  Functions::getProfilePicUrl( Auth::id() );
        
        Response::instance()->sendData( ['user' =>  $data], 'success' );
    }

    function profileUpdate() {
        $file_to_save = isset( $_FILES['u_profile_pic'] ) ? $_FILES['u_profile_pic'] : [];
        
        $user = User::getUser( Auth::id() );
        if( ! $user ){
            Response::instance()->sendMessage( 'You are not logged in' );
        }
        $user->update( $_POST );
        $data = $user->toArray();
        $data['authToken'] = $user->authToken();
                

        if ( $file_to_save ) {
            $upload_folder = STATIC_DIR . '/users/' . \floor( Auth::id() / 1000 );

            if ( ! is_dir($upload_folder)) {
                @mkdir($upload_folder, 0755, true);
            }
            
            // var_dump($file_to_save);
            $tmp_name = $file_to_save['tmp_name'];

            $imagetype = exif_imagetype( $tmp_name );
            $mime      = ( $imagetype ) ? image_type_to_mime_type( $imagetype ) : false;
            $ext       = ( $imagetype ) ? image_type_to_extension( $imagetype ) : false;
            if( ! $ext || ! $mime ) {
                Response::instance()->sendMessage( 'Only profile picture are allowed.');
            }
            //Delete previous pic here
            $prev_image = \str_replace( STATIC_URL, STATIC_DIR, Functions::getProfilePicUrl( Auth::id() ) );
            if( $prev_image && file_exists( $prev_image ) ){
                @unlink( $prev_image );
            }

            $new_file = Functions::randToken( 'alnumlc', 12 ) . $ext;

            $new_file = \sprintf( '%s/%s-%s', $upload_folder, Auth::id(), $new_file );
            if( @ move_uploaded_file( $tmp_name, $new_file ) ) {
                // Set correct file permissions.
                $stat  = stat( dirname( $new_file ) );
                $perms = $stat['mode'] & 0000666;
                @ chmod( $new_file, $perms );
            }
        
        }
        $data['u_pic_url'] =  Functions::getProfilePicUrl( Auth::id() );
        Response::instance()->sendData( ['user' => $data], 'success' );
    }

    function offers(){
        $data = [
            [
                'heading' => 'Cashback ৳100',
                'desc' => 'Get ৳100 cash back on purchase of more than ৳5000',
            ],
            [
                'heading' => 'Cashback ৳80',
                'desc' => 'Get ৳80 cash back on purchase of ৳4000 to ৳4999',
            ],
            [
                'heading' => 'Cashback ৳60',
                'desc' => 'Get ৳60 cash back on purchase of ৳3000 to ৳3999',
            ],
            [
                'heading' => 'Cashback ৳40',
                'desc' => 'Get ৳40 cash back on purchase of ৳2000 to ৳2999',
            ],
            [
                'heading' => 'Cashback ৳20',
                'desc' => 'Get ৳20 cash back on purchase of ৳1000 to ৳1999',
            ],

        ];
        Response::instance()->sendData( $data, 'success' );
    }

    private function FAQs(){
        $return = [];
        $return[] = [
            'title' => 'Medicine and Healthcare Orders',
            'slug' => 'medicineAndHealthcareOrders',
            'data' => [
                [
                    'q' => 'When will I receive my order?',
                    'a' => 'Your order will be delivered within 12-24 hours.'
                ],
                [
                    'q' => 'I have received damaged items.',
                    'a' => 'We are sorry you had to experience this. Please do not accept the delivery of that order and let us know what happened'
                ],
                [
                    'q' => 'Items are different from what I ordered.',
                    'a' => 'We are sorry you have had to experience this. Please do not accept it from delivery man. Reject the order straightaway and call to arogga customer care'
                ],
                [
                    'q' =>'What if Items are missing from my order.',
                    'a' => 'In no circumstances, you should receive an order that is incomplete. Once delivery man reaches your destination, be sure to check expiry date of medicines and your all ordered items was delivered.',
                ],
                [
                    'q' => 'How do I cancel my order?',
                    'a' => 'Please call us with your order ID and we will cancel it for you.'
                ],
                [
                    'q' => 'I want to modify my order.',
                    'a' => 'Sorry, once your order is confirmed, it cannot be modified. Please place a fresh order with any modifications.'
                ],
                [
                    'q' => 'What is the shelf life of medicines being provided?',
                    'a' => 'We ensure that the shelf life of the medicines being supplied by our partner retailers is, at least, a minimum of 3 months from the date of delivery.'
                ]
            ]
        ];
        $return[] = [
            'title' => 'Delivery',
            'slug' => 'delivery',
            'data' => [
                [
                    'q' => 'When will I receive my order?',
                    'a' => 'Your order will be delivered within the Estimated Delivery Date.'
                ],
                [
                    'q' => 'Order status showing delivered but I have not received my order.',
                    'a' => 'Sorry that you are experiencing this. Please call to connect with us immediately.'
                ],
                [
                    'q' => 'Which cities do you operate in?',
                    'a' => 'We provide healthcare services in Dhaka and Chittagong and expanding for whole Bangladesh very soon.'
                ],
                [
                    'q' => 'How can I get my order delivered faster?',
                    'a' => 'Sorry, we currently do not have a feature available to expedite the order delivery. We surely have a plan to introduce 2 hour delivery soon'
                ],
                [
                    'q' => 'Can I modify my address after Order placement?',
                    'a' => 'Sorry, once the order is placed, we are unable to modify the address.'
                ],
            ]
        ];

        $return[] = [
            'title' => 'Payments',
            'slug' => 'payments',
            'data' => [
                [
                    'q' => 'How do customers get discounts.',
                    'a' => 'We deduct the value from every medicines and show it to you before order, so that you can see what you are really paying for each medicines  '
                ],
                [
                    'q' => 'When will I get my refund?',
                    'a' => 'Refund will be in credited in arogga cash (3-5 business days)'
                ],
                [
                    'q' => 'I did not receive cashback for my order.',
                    'a' => 'Please read the T&C of the offer carefully for the eligibility of cashback.'
                ],
                [
                    'q' => 'What are the payment modes at arogga?',
                    'a' => 'Cash on Delivery (COD).'
                ]
            ]
        ];

        $return[] = [
            'title' => 'Referrals',
            'slug' => 'referrals',
            'data' => [
                [
                    'q' => 'How does your referral program work?',
                    'a' => 'Invite your friend and family members by sharing your referral code. If a new user places their first order using your referral code, they will get an extra 20 taka OFF over the base discount. You will get an extra 20 taka OFF on your next order; once their order is delivered.'
                ],
                [
                    'q' => 'Why did I not get the referral benefit?',
                    'a' => 'If you are not notified about your referral benefit, it is likely that one or more of the following things happened: \n 1. The referred member did not apply your referral code while placing the order \n 2. The user clicked on your link but did not create an account or complete their first purchase. \n 3. The referred member placed an eligible order, but the order was not fulfilled. \n 4. The person who used the code has already placed an order on arogga. \n 5. Your referral benefit has expired'
                ],
                [
                    'q' => 'Is there an expiry date to my referral benefit?',
                    'a' => 'No, there is no expiry date. Once you are eligible for the additional benefit, you will surely get it.'
                ],
            ]
        ];

        $return[] = [
            'title' => 'Arogga Cash',
            'slug' => 'AroggaCash',
            'data' => [
                [
                    'q' => 'What is arogga cash?',
                    'a' => 'This is a virtual wallet to store arogga Cash in your account..'
                ],
                [
                    'q' => 'How do I check my arogga cash balance?',
                    'a' => 'You can check your arogga cash in Account screen.'
                ],
                [
                    'q' => 'When will the arogga money expire?',
                    'a' => 'Any arogga Cash deposited in your arogga wallet through returns will never expire. At times, our marketing team may deposit promotional cash which will have an expiry that is communicated to you via an SMS.'
                ],
                [
                    'q' => 'Can I add money to my arogga cash?',
                    'a' => 'No, you are unable to transfer or add money to your arogga cash.'
                ],
                [
                    'q' => 'How can I redeem my arogga cash?',
                    'a' => 'If you have any money in your arogga cash, it will be automatically deducted from your next order amount and you will only have to pay for the balance amount (if any).'
                ],
                [
                    'q' => 'Can I transfer money from my arogga cash to the bank account?',
                    'a' => 'No, you are unable to transfer money from your arogga cash to the bank account.'
                ],
                [
                    'q' => 'How much arogga money can I redeem in an order?',
                    'a' => 'There is no limit for redemption of arogga cash  '
                ]
            ]
        ];

        $return[] = [
            'title' => 'Promotions',
            'slug' => 'promotions',
            'data' => [
                [
                    'q' =>'How do I apply a coupon code on my order?',
                    'a' =>'You can apply a coupon on the cart screen while placing an order. If you are getting a message that the coupon code has failed to apply, it may be because you are not eligible for the offer.'
                ],
                [
                    'q' => 'Does arogga offers return of the medicine?',
                    'a' => 'No, Arogga does not accept returns of the medicine from customer. Thats why customers are requested to thoroughly check all the medicine before accepting the order from delivery man. If for any reason you want to return the product, simply reject the order to delivery man. Do not receive it, your order will be automatically cancelled'
                ]
            ]
        ];
        return $return;
    }

    function FAQsHeaders(){
        $FAQs = $this->FAQs();
        $data = array_map( function( $FAQ ){
            return [ 'title' => $FAQ['title'], 'slug' => $FAQ['slug'] ];
        }, $FAQs);
        $data = array_filter( $data );
        if( ! is_array( $data ) ){
            $data = [];
        }

        Response::instance()->sendData( $data, 'success' );
    }

    function FAQsReturn( $slug ){
        $FAQs = $this->FAQs();
        $data = array_map( function( $FAQ ) use ( $slug ){
            if( $FAQ['slug'] == $slug ){
                return $FAQ['data'];
            } else {
                return null;
            }
        }, $FAQs);
        $data = array_filter( $data );
        $return = reset( $data );
        if( ! is_array( $return ) ){
            $return = [];
        }

        Response::instance()->sendData( $return, 'success' );
    }

    function saveInternalNote( $o_id ){
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }

        if( ! \in_array( Auth::id(), [ $order->o_de_id, $order->o_ph_id ] ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }
        $o_i_note = isset($_POST['o_i_note']) ? filter_var($_POST['o_i_note'], FILTER_SANITIZE_STRING) : '';
        $order->setMeta( 'o_i_note', $o_i_note );

        $this->orderSingle( $o_id );
    }

    function locationData(){
        $get      = isset($_GET['get']) ? $_GET['get'] : '';
        $division = isset($_GET['division']) ? $_GET['division'] : '';
        $district = isset($_GET['district']) ? $_GET['district'] : '';

        $data = [];
        if( in_array( $get, [ 'all', 'divisions'] ) ){
            $data['divisions'] = Functions::getDivisions();
        }
        if( in_array( $get, [ 'all', 'districts'] ) ){
            $data['districts'] = Functions::getDistricts( $division );
        }
        if( in_array( $get, [ 'all', 'areas'] ) ){
            $data['areas'] = Functions::getAreas( $division, $district );
        }

        Response::instance()->sendData( $data, 'success' );
    }

}