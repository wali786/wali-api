<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate};
use GuzzleHttp\Client;

class CronResponse {

    function __construct() {
    }

    function daily(){
        $dbVersion = \intval( Option::get('dbVersion') );
        $data = Option::get( 'dbNext', true );
        $i = $dbVersion + 1;
        if( $data ) {
            Option::set( "dbData_{$i}", $data );
            Option::set( "dbVersion", $i );
            Option::set( "dbNext", [] ); 
        }
    }

    function dumpLog(){
        DB::db()->beginTransaction();

        $last_id = DB::db()->query('SELECT MAX(log_id) FROM t_logs')->fetchColumn();
        if( !$last_id ){
            return null;
        }

        $query = DB::db()->prepare( 'INSERT INTO t_logs_backup (SELECT * FROM t_logs WHERE log_id <= ?)' );
        $query->execute( [ $last_id ] );

        $query2 = DB::db()->prepare( 'DELETE FROM t_logs WHERE log_id <= ?' );
        $query2->execute( [ $last_id ] );

        DB::db()->commit();
        die;
    }

}