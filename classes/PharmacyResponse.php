<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate};
use GuzzleHttp\Client;

class PharmacyResponse {
    private $user;

    function __construct() {
        if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
            Response::instance()->loginRequired( true );
            Response::instance()->sendMessage( 'Invalid id token' );
        }
        if ( 'pharmacy' !== $user->u_role ) {
            Response::instance()->sendMessage( 'You are not a pharmacy.' );
        }
        $this->user = $user;
    }

    function orders() {
        $status = isset( $_GET['status'] ) ? $_GET['status'] : '';
        $page = isset( $_GET['page'] ) ? (int)$_GET['page'] : 1;
        $per_page = 10;
        $limit    = $per_page * ( $page - 1 );

        $db = new DB;

        $db->add( 'SELECT SQL_CALC_FOUND_ROWS * FROM t_orders WHERE o_ph_id = ?', Auth::id() );

        if( \in_array( $status, [ 'feedback_requested', 'feedback_given', 'later'] ) ) {
            $db->add( ' AND o_status IN (?,?)', 'processing', 'confirmed' );
            if ( 'feedback_requested' == $status ) {
                $db->add( ' AND o_i_status = ?', 'ph_fb' );
            } elseif ( 'feedback_given' == $status ) {
                $db->add( ' AND o_i_status IN (?,?,?)', 'later', 'never', 'confirmed' );
            } elseif ( 'later' == $status ) {
                $db->add( ' AND o_i_status = ?', 'later' );
            }
        } elseif ( 'packing' == $status ) {
            $db->add( ' AND o_status = ? AND o_i_status = ?', 'confirmed', $status );
        } elseif ( $status ) {
            $db->add( ' AND o_status = ?', $status );
            $db->add( ' AND o_i_status IN (?,?)', 'confirmed', 'paid' );
        }
        if( \in_array( $status, ['delivered', 'cancelled']) ){
            $db->add( ' ORDER BY o_id DESC' );
        } else {
            $db->add( ' ORDER BY o_id ASC' );
        }

        $db->add( ' LIMIT ?, ?', $limit, $per_page );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Order');

        while( $order = $query->fetch() ){
            $data = $order->toArray();
            if ( !empty($data['o_de_id']) && ( $user = User::getUser( $data['o_de_id'] ) ) ) {
                $data['o_de_name'] = $user->u_name;
            }
            $data['paymentStatus'] = ( 'paid' === $order->o_i_status || 'paid' === $order->getMeta( 'paymentStatus' ) ) ? 'paid' : '';
            $data['o_i_note'] = (string)$order->getMeta('o_i_note');
            
            Response::instance()->appendData( '', $data );
        }
        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Orders Found' );
        } else {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function pharmacyLater(){
        $page = isset( $_GET['page'] ) ? (int)$_GET['page'] : 1;
        $status = isset( $_GET['status'] ) ? $_GET['status'] : 'pending';
        $per_page = 20;
        $limit    = $per_page * ( $page - 1 );

        $db = new DB;
        $db->add( 'SELECT SQL_CALC_FOUND_ROWS tom.*, tom.m_unit as unit, tm.m_name as name, tm.m_form as form, tm.m_strength as strength, tm.m_company as company, tr.o_ph_id, SUM(tom.m_qty) as total_qty FROM t_o_medicines tom INNER JOIN t_medicines tm ON tom.m_id = tm.m_id INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE 1=1' );
        $db->add( ' AND tr.o_ph_id = ?', Auth::id() );
        $db->add( ' AND tr.o_status IN (?,?)', 'processing', 'confirmed' );
        $db->add( ' AND tr.o_i_status IN (?,?,?)', 'ph_fb', 'later', 'confirmed' );
        $db->add( ' AND tom.om_status = ?', $status );

        $db->add( ' GROUP BY tom.m_id ORDER BY tm.m_company' );
        $db->add( ' LIMIT ?, ?', $limit, $per_page );
        
        $query = $db->execute();
        $total = DB::db()->query('SELECT FOUND_ROWS()')->fetchColumn();
        $data = $query->fetchAll();

        Response::instance()->setData( $data );

        if ( ! Response::instance()->getData() ) {
            Response::instance()->sendMessage( 'No Medicines Found' );
        } else {
            Response::instance()->setResponse( 'total', $total );
            Response::instance()->setStatus( 'success' );
            Response::instance()->send();
        }
    }

    function supplierPrice() {
        $medicines = isset( $_POST['medicines'] ) ?  $_POST['medicines'] : [];
        $o_id = isset( $_POST['o_id'] ) ? $_POST['o_id'] : '';

        Response::instance()->sendMessage( 'Price input is not allowed.' );

        if ( ! $medicines ){
            //Response::instance()->sendMessage( 'medicines are required.');
        }
        if ( ! is_array( $medicines ) ){
            Response::instance()->sendMessage( 'medicines are malformed.');
        }
        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }
        if( $order->o_ph_id != Auth::id() ){
            Response::instance()->sendMessage( 'You are not pharmacy assign to this order.' );
        }
        if( ! \in_array( $order->o_status, ['processing', 'confirmed'] ) ){
            Response::instance()->sendMessage( 'You cannot edit it anymore.' );
        }
        if( \in_array( $order->o_i_status, ['confirmed'] ) ){
            //Response::instance()->sendMessage( 'You cannot edit it anymore.' );
        }
        $prev_medicines = $order->medicines;

        foreach ( $medicines as $m_id => $price ) {
            $m_id = (int) $m_id;
            $price = \round( $price, 2 );

            $medicine = isset( $prev_medicines[$m_id] ) ? $prev_medicines[$m_id] : [];
            if( ! $medicine ) {
                continue;
            }

            DB::instance()->update( 't_o_medicines', [ 's_price' => $medicine['qty'] ? \round( $price/$medicine['qty'], 2 ) : 0.00, 'om_status' => 'available'], [ 'om_id' => $medicine['om_id'] ] );
        }
        $query = DB::db()->prepare( 'SELECT om_id FROM t_o_medicines WHERE o_id = ? AND om_status = ? LIMIT 1' );
        $query->execute( [ $o_id, 'pending'] );

        if( $query->fetch() ){
            Response::instance()->sendMessage( 'All medicines feedback are required.');
        }

        $query = DB::db()->prepare( 'SELECT DISTINCT om_status FROM t_o_medicines WHERE o_id = ? AND m_qty != ?' );
        $query->execute( [ $o_id, 0 ] );

        $update = [
            'o_status' => 'confirmed',
            'o_i_status' => 'confirmed',
        ];

        while( $om = $query->fetch() ){
            if( 'never' == $om['om_status'] ){
                $update = [
                    'o_i_status' => 'never',
                ];
                break;
            }
            if( 'later' == $om['om_status'] ){
                $update = [
                    'o_status' => 'confirmed',
                    'o_i_status' => 'later',
                ];
            }
        }
        if( $update ) {
            $order->update( $update );
        }

        Response::instance()->sendMessage( 'Prices successfully updated.', 'success' );
    }

    function receivedCollection( $co_id ) {
        if( ! $co_id ) {
            Response::instance()->sendMessage( 'No collection found.' );
        }
        $query = DB::db()->prepare( 'SELECT * FROM t_collections WHERE co_id = ? AND co_tid = ? AND co_status = ? LIMIT 1' );
        $query->execute( [ $co_id, Auth::id(), 'pending' ] );

        if( $collection = $query->fetch() ){
            $updated = DB::instance()->update( 't_collections', ['co_status' => 'confirmed'], [ 'co_id' => $co_id ] );
            if( $updated ){
                $user = User::getUser( Auth::id() );
                $user->cashUpdate( -$collection['co_amount'] );

                $fm_name = User::getName( $collection['co_fid'] );
                $to_name = User::getName( $collection['co_tid'] );
                $reason = \sprintf( 'Collected by %s from %s', $to_name, $fm_name );
                Functions::ledgerCreate( $reason, $collection['co_amount'], 'collection' );

                Response::instance()->sendMessage( 'Confirmed received collection.', 'success' );
            }
        } else {
            Response::instance()->sendMessage( 'No collection found to confirm.' );
        }
    }

    function setOMStatus(){
        $o_id = isset( $_POST['o_id'] ) ? (int)$_POST['o_id'] : '';
        $om_id = isset( $_POST['om_id'] ) ? (int)$_POST['om_id'] : '';
        $status = isset( $_POST['status'] ) ? filter_var($_POST['status'], FILTER_SANITIZE_STRING) : '';

        Response::instance()->sendMessage( 'Changing status is not allowed.' );

        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }
        if( Auth::id() != $order->o_ph_id ){
            Response::instance()->sendMessage( 'You are not pharmacy assign to this order.' );
        }

        $updated = DB::instance()->update( 't_o_medicines', ['om_status' => $status], [ 'om_id' => $om_id, 'o_id' => $o_id ] );
        if( $updated ) {
            $data = $order->toArray();
            $data['prescriptions'] = $order->prescriptions;
            $data['o_data'] = (array)$order->getMeta( 'o_data' );
            $data['o_data']['medicines'] = $order->medicines;
            Response::instance()->sendData( $data, 'success' );
        } else {
            Response::instance()->sendMessage( 'Something wrong. Please try again.' );
        }

    }

    function setCombineStatus(){
        $m_id = isset( $_POST['m_id'] ) ? (int)$_POST['m_id'] : '';
        $status = isset( $_POST['status'] ) ? filter_var($_POST['status'], FILTER_SANITIZE_STRING) : '';

        Response::instance()->sendMessage( 'Changing status is not allowed.' );

        $db = new DB;
        $db->add( 'UPDATE t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id' );
        $db->add( ' SET om_status = ?', $status );
        $db->add( ' WHERE tom.m_id = ? AND tr.o_ph_id = ?', $m_id, Auth::id() );
        $db->add( ' AND tr.o_status IN (?,?)', 'processing', 'confirmed' );
        $db->add( ' AND tr.o_i_status IN (?,?,?)', 'ph_fb', 'later', 'confirmed' );
        
        $db->execute();

        Response::instance()->sendMessage( 'Status changed successfully.', 'success' );
    }

    function setCombinePrice() {
        $medicines = isset( $_POST['medicines'] ) ?  $_POST['medicines'] : [];

        Response::instance()->sendMessage( 'Price input is not allowed.' );

        if ( ! $medicines ){
            //Response::instance()->sendMessage( 'medicines are required.');
        }
        if ( ! is_array( $medicines ) ){
            Response::instance()->sendMessage( 'medicines are malformed.');
        }

        $query = DB::db()->prepare( 'UPDATE t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id SET om_status = ?, s_price = ? WHERE tom.m_id = ? AND tr.o_ph_id = ? AND tr.o_status IN (?,?) AND tr.o_i_status IN (?,?,?)');

        foreach ( $medicines as $m_id => $price ) {
            $m_id = (int) $m_id;
            $price = \round( $price, 2 );

            $query->execute( [ 'packed', $price, $m_id, Auth::id(), 'processing', 'confirmed', 'ph_fb', 'later', 'confirmed' ] );
        }

        Response::instance()->sendMessage( 'Prices successfully updated.', 'success' );
    }

    function combineClearPacked(){
        Response::instance()->sendMessage( 'Price input is not allowed.' );

        $inv_id = DB::db()->query('SELECT MAX(pu_inv_id) FROM t_purchases')->fetchColumn() + 1;

        $db = new DB;
        $db->add( ' AND tr.o_ph_id = ?', Auth::id() );
        $db->add( ' AND tr.o_status IN (?,?)', 'processing', 'confirmed' );
        $db->add( ' AND tr.o_i_status IN (?,?,?)', 'ph_fb', 'later', 'confirmed' );
        $db->add( ' AND tom.om_status = ?', 'packed' );

        $sql = $db->getSql();
        $params = $db->getParams();

        $db1 = new DB;
        $db1->add( 'SELECT tom.m_id, tom.s_price, SUM(tom.m_qty) as total_qty FROM t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id WHERE 1=1' );
        $db1->add( $sql, ...$params );
        $db1->add( ' GROUP BY tom.m_id' );
        $query = $db1->execute();

        $insert = [];
        while ( $om = $query->fetch() ) {
            $insert[] = [
                'pu_inv_id' => $inv_id,
                'pu_ph_id' => Auth::id(),
                'pu_m_id' => $om['m_id'],
                'pu_price' => $om['s_price'],
                'pu_qty' => $om['total_qty'],
                'pu_created' => \date( 'Y-m-d H:i:s' ),
                //'pu_status' => 'pending', //default
            ];
        }
        if( $insert ){
            DB::instance()->insertMultiple( 't_purchases', $insert );
        }

        $db2 = new DB;
        $db2->add( 'UPDATE t_o_medicines tom INNER JOIN t_orders tr ON tom.o_id = tr.o_id' );
        $db2->add( ' SET tom.om_status = ?', 'pending' );
        $db2->add( ' WHERE 1=1');
        $db2->add( $sql, ...$params );
        $db2->execute();

        Response::instance()->sendMessage( 'Cleared successfully.', 'success' );
    }

    function internalStatusTo( $o_id, $status ){
        if ( !$o_id || !$status || !\in_array( $status, ['confirmed'] ) ) {
            Response::instance()->sendMessage( 'No orders found.' );
        }
        if( ! ( $order = Order::getOrder( $o_id ) ) ){
            Response::instance()->sendMessage( 'No orders found.' );
        }
        if( Auth::id() != $order->o_ph_id ){
            Response::instance()->sendMessage( 'You are not pharmacy for this order.' );
        }

        if( $order->update( [ 'o_i_status' => $status ] ) ){
            Response::instance()->sendMessage( 'Successfully changed status.', 'success' );
        }
        Response::instance()->sendMessage( 'Something wrong. Please try again.' );
    }

}