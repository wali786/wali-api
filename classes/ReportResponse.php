<?php

namespace OA;
use OA\Factory\{User, Medicine, Discount, Order, Option, CacheUpdate, Inventory};
use GuzzleHttp\Client;

class ReportResponse {
    private $user;

    function __construct() {
        \header("Access-Control-Allow-Origin: *");
        // \header("Access-Control-Request-Headers: *");
        \define( 'ADMIN', true );

        // if ( ! ( $user = User::getUser( Auth::id() ) ) ) {
        //     Response::instance()->setCode( 403 );
        //     Response::instance()->loginRequired( true );
        //     Response::instance()->sendMessage( 'You are not logged in' );
        // }
        // if( ! $user->can( 'backendAccess' ) ) {
        //     Response::instance()->setCode( 401 );
        //     Response::instance()->sendMessage( 'Your account does not have admin access.');
        // }
        // $httpMethod = $_SERVER['REQUEST_METHOD'];
        // if( $user->can( 'onlyGET' ) && $httpMethod != 'GET' ) {
        //     Response::instance()->sendMessage( 'Your account does not have permission to do this.');
        // }

        // $this->user = $user;
    }

    private function validateDate( $date, $format = 'Y-m-d' ){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function orders(){
        $dateFrom = (isset( $_GET['dateFrom'] ) && $this->validateDate( $_GET['dateFrom'] ) ) ? $_GET['dateFrom'] : '';
        $dateTo = (isset( $_GET['dateTo'] ) && $this->validateDate( $_GET['dateTo'] ) ) ? $_GET['dateTo'] : '';

        if( ! $dateFrom || ! $dateTo ){
            Response::instance()->sendMessage( 'Invalid date', 'error' );
        }

        $begin = new \DateTime($dateFrom);
        $end = new \DateTime($dateTo);
        $checkend = new \DateTime($dateTo);
        $checkend->add(\DateInterval::createFromDateString('1 day'));
        $difference = $begin->diff($end);
        $months = $difference->y * 12 + $difference->m + floor($difference->d / 30);

        if( $months > 12 ){
            $interval = \DateInterval::createFromDateString('1 year');
            $dayFormat = 'Y';
            $dateFormat = '%Y';
        } else if( $months > 1 ){
            $interval = \DateInterval::createFromDateString('1 month');
            $dayFormat = 'Y-m';
            $dateFormat = '%Y-%m';
        } else {
            $interval = \DateInterval::createFromDateString('1 day');
            $dayFormat = 'Y-m-d';
            $dateFormat = '%Y-%m-%d';
        }
        $period = new \DatePeriod($begin, $interval, $checkend);

        $query = DB::db()->prepare("SELECT DATE_FORMAT(o_created, ?) as orderDate, count(o_id) as orderCount, sum(o_total) as orderValue, `o_status` as orderStatus FROM `t_orders` WHERE `o_created` BETWEEN ? AND ? GROUP BY DATE_FORMAT(o_created, ?) , `o_status`");
        $query->execute([ $dateFormat, $dateFrom, $checkend->format('Y-m-d'), $dateFormat ]);
        $orders = $query->fetchAll();
        // Response::instance()->sendData( $orders, 'success');
        $orderReport = [];
        $orderReport['orderCount'] = [];
        $orderReport['orderValue'] = [];

        foreach ( $period as $date ) {
            $totalCount = $totalValue = 0;
            $orderCount = $orderValue = [
                'date' => '',
                'total' => 0,
                'delivered' => 0,
                'delivering' => 0,
                'confirmed' => 0,
                'processing' => 0,
                'cancelled' => 0,
            ];
            $orderCount['date'] = $orderValue['date'] = $date->format($dayFormat);

            foreach ( $orders as $order ) {
                if ( $date->format($dayFormat) == $order['orderDate'] ) {
                    $orderCount[ $order['orderStatus'] ] = $order['orderCount'];
                    $orderValue[ $order['orderStatus'] ] = $order['orderValue'];
                    $totalCount += $order['orderCount'];
                    $totalValue += $order['orderValue'];
                }
            }
            $orderCount['total'] = $totalCount;
            $orderValue['total'] = $totalValue;

            array_push($orderReport['orderCount'], $orderCount);
            array_push($orderReport['orderValue'], $orderValue);
        }

        Response::instance()->sendData( $orderReport, 'success');
    }

    public function users(){
        $dateFrom = (isset( $_GET['dateFrom'] ) && $this->validateDate( $_GET['dateFrom'] ) ) ? $_GET['dateFrom'] : '';
        $dateTo = (isset( $_GET['dateTo'] ) && $this->validateDate( $_GET['dateTo'] ) ) ? $_GET['dateTo'] : '';

        if( ! $dateFrom || ! $dateTo ){
            Response::instance()->sendMessage( 'Invalid date', 'error' );
        }

        $begin = new \DateTime($dateFrom);
        $end = new \DateTime($dateTo);
        $checkend = new \DateTime($dateTo);
        $checkend->add(\DateInterval::createFromDateString('1 day'));
        $difference = $begin->diff($end);
        // return floor($difference->d / 30);
        $months = $difference->y * 12 + $difference->m + floor($difference->d / 30);
        if( $months > 12 ){
            $interval = \DateInterval::createFromDateString('1 year');
            $dayFormat = 'Y';
            $dateFormat = '%Y';
        } else if( $months > 1 ){
            $interval = \DateInterval::createFromDateString('1 month');
            $dayFormat = 'Y-m';
            $dateFormat = '%Y-%m';
        } else {
            $interval = \DateInterval::createFromDateString('1 day');
            $dayFormat = 'Y-m-d';
            $dateFormat = '%Y-%m-%d';
        }
        $period = new \DatePeriod($begin, $interval, $checkend);

        $registeredUsersQuery = DB::db()->prepare('SELECT DATE_FORMAT(u_created, ?), count(u_id) FROM `t_users` WHERE `u_created` BETWEEN ? AND ? GROUP BY DATE_FORMAT(u_created, ?)');
        $registeredUsersQuery->execute([ $dateFormat, $dateFrom, $checkend->format('Y-m-d'), $dateFormat ]);
        $registeredUsers = $registeredUsersQuery->fetchAll( \PDO::FETCH_KEY_PAIR );

        $orderedUsersQuery = DB::db()->prepare('SELECT DATE_FORMAT(u_created, ?), count(u_id) FROM `t_users` WHERE `u_o_count` > ?  AND `u_created` BETWEEN ? AND ? GROUP BY DATE_FORMAT(u_created, ?)');
        $orderedUsersQuery->execute([ $dateFormat, 0, $dateFrom, $checkend->format('Y-m-d'), $dateFormat ]);
        $orderedUsers = $orderedUsersQuery->fetchAll( \PDO::FETCH_KEY_PAIR );

        $repeatedUsersQuery = DB::db()->prepare('SELECT DATE_FORMAT(u_created, ?), count(u_id) FROM `t_users` WHERE `u_o_count` > ?  AND `u_created` BETWEEN ? AND ? GROUP BY DATE_FORMAT(u_created, ?)');
        $repeatedUsersQuery->execute([ $dateFormat, 1, $dateFrom, $checkend->format('Y-m-d'), $dateFormat ]);
        $repeatedUsers = $repeatedUsersQuery->fetchAll( \PDO::FETCH_KEY_PAIR );

        $userReport = [];
        foreach ($period as $date) {
            $userCount = [
                'date' => '',
                'total' => 0,
                'ordered' => 0,
                'repeated' => 0,
            ];
            $userCount['date'] = $date->format($dayFormat);
            if( isset( $registeredUsers[ $userCount['date'] ] ) ){
                $userCount['total'] = $registeredUsers[ $userCount['date'] ];
            }
            if( isset( $orderedUsers[ $userCount['date'] ] ) ){
                $userCount['ordered'] = $orderedUsers[ $userCount['date'] ];
            }
            if( isset( $repeatedUsers[ $userCount['date'] ] ) ){
                $userCount['repeated'] = $repeatedUsers[ $userCount['date'] ];
            }

            array_push($userReport, $userCount);
        }

        Response::instance()->sendData( $userReport, 'success');
    }

    public function summary(){
        $dateFrom = (isset( $_GET['dateFrom'] ) && $this->validateDate( $_GET['dateFrom'] ) ) ? $_GET['dateFrom'] : '';
        $dateTo = (isset( $_GET['dateTo'] ) && $this->validateDate( $_GET['dateTo'] ) ) ? $_GET['dateTo'] : '';

        if( ! $dateFrom || ! $dateTo ){
            Response::instance()->sendMessage( 'Invalid date', 'error' );
        }

        //summary calculation
        $begin = new \DateTime($dateFrom);
        $end = new \DateTime($dateTo);
        $checkend = new \DateTime($dateTo);
        $checkend->add(\DateInterval::createFromDateString('1 day'));
        $difference = $begin->diff($end);

        $query = DB::db()->prepare('SELECT count(*) FROM `t_users` WHERE `u_created` BETWEEN ? AND ?');
        $query->execute([ $dateFrom, $checkend->format('Y-m-d') ]);
        $usersCount = $query->fetchColumn();

        $query = DB::db()->prepare('SELECT count(*) FROM `t_orders` WHERE `o_created` BETWEEN ? AND ?');
        $query->execute([ $dateFrom, $checkend->format('Y-m-d') ]);
        $ordersCount = $query->fetchColumn();

        $query = DB::db()->prepare('SELECT count(*) as total, sum(tr.o_total) as reveneue, sum(tm1.meta_value) as price, sum(tm2.meta_value) as fee FROM `t_orders` as tr LEFT JOIN `t_order_meta` as tm1 on tr.o_id = tm1.o_id AND tm1.meta_key = ? LEFT JOIN `t_order_meta` as tm2 on tr.o_id = tm2.o_id AND tm2.meta_key = ? WHERE tr.o_status = ? AND tr.o_created BETWEEN ? AND ?');
        $query->execute([ 'supplierPrice', 'paymentGatewayFee', 'delivered', $dateFrom, $checkend->format('Y-m-d')]);
        $result = $query->fetch() ?? [];

        Response::instance()->sendData( $result, 'success');

        $total = $result['total'] ?? 0;
        $price = $result['price'] ?? 0;
        $fee = $result['fee'] ?? 0;

        $summary = [];
        $summary['users'] = $usersCount;
        $summary['orders'] = $ordersCount;
        $summary['revenue'] = $result['reveneue'] ?? 0;
        $summary['profit'] = $summary['revenue'] - $price - $fee;
        $summary['avg_basket_size'] = $total ? round($summary['revenue']/$total, 2) : 0;

        //previous summary calculation
        $prevStartingDate = new \DateTime($dateFrom);
        $days = $difference->y * 12 + $difference->m * 30 + $difference->d;
        $prevStartingDate->sub(\DateInterval::createFromDateString("{$days} days"));

        $query = DB::db()->prepare('SELECT count(*) FROM `t_users` WHERE `u_created` BETWEEN ? AND ?');
        $query->execute([ $prevStartingDate->format('Y-m-d'), $dateFrom ]);
        $usersCount = $query->fetchColumn();

        $query = DB::db()->prepare('SELECT count(*) FROM `t_orders` WHERE `o_created` BETWEEN ? AND ?');
        $query->execute([ $dateFrom, $checkend->format('Y-m-d') ]);
        $ordersCount = $query->fetchColumn();

        $query = DB::db()->prepare('SELECT count(*) as total, sum(tr.o_total) as reveneue, sum(tm1.meta_value) as price, sum(tm2.meta_value) as fee FROM `t_orders` as tr LEFT JOIN `t_order_meta` as tm1 on tr.o_id = tm1.o_id AND tm1.meta_key = ? LEFT JOIN `t_order_meta` as tm2 on tr.o_id = tm2.o_id AND tm2.meta_key = ? WHERE tr.o_status = ? AND tr.o_created BETWEEN ? AND ?');
        $query->execute([ 'supplierPrice', 'paymentGatewayFee', 'delivered', $prevStartingDate->format('Y-m-d'), $dateFrom ]);
        $result = $query->fetch() ?? [];

        $total = $result['total'] ?? 0;
        $price = $result['price'] ?? 0;
        $fee = $result['fee'] ?? 0;

        $summary['prev_users'] = $usersCount;
        $summary['prev_orders'] = $ordersCount;
        $summary['prev_revenue'] = $result['reveneue'] ?? 0;
        $summary['prev_profit'] = $summary['prev_revenue'] - $price - $fee;
        $summary['prev_avg_basket_size'] = $total ? round($summary['prev_revenue']/$total, 2) : 0;

        Response::instance()->sendData( $summary, 'success');
    }

    public function popularMedicines(){

        $dateFrom = (isset( $_GET['dateFrom'] ) && $this->validateDate( $_GET['dateFrom'] ) ) ? $_GET['dateFrom'] : '';
        $dateTo = (isset( $_GET['dateTo'] ) && $this->validateDate( $_GET['dateTo'] ) ) ? $_GET['dateTo'] : '';
        $limit = (isset( $_GET['limit'] ) && (int)$_GET['limit'] <= 1000 ) ? (int)$_GET['limit'] : 10;

        if( ! $dateFrom || ! $dateTo ){
            Response::instance()->sendMessage( 'Invalid date', 'error' );
        }
        $begin = new \DateTime($dateFrom);
        $end = new \DateTime($dateTo);
        $checkend = new \DateTime($dateTo);
        $checkend->add(\DateInterval::createFromDateString('1 day'));
        $difference = $begin->diff($end);
        $query = DB::db()->prepare('SELECT sum(t_o_medicines.m_qty) as total_qty, t_o_medicines.m_id, t_medicines.* FROM `t_o_medicines` JOIN `t_orders` ON t_orders.o_id = t_o_medicines.o_id JOIN `t_medicines` ON t_medicines.m_id = t_o_medicines.m_id WHERE t_orders.o_created BETWEEN ? AND ? GROUP BY t_o_medicines.m_id ORDER BY total_qty desc limit ?');
        $query->execute([ $dateFrom, $checkend->format('Y-m-d'), $limit ]);
        $popular_medicines_quantity_wise = $query->fetchAll();

        $query = DB::db()->prepare('SELECT sum(t_o_medicines.m_qty * t_o_medicines.m_d_price) as revenue, t_o_medicines.m_id, t_medicines.* FROM `t_o_medicines` JOIN `t_orders` ON t_orders.o_id = t_o_medicines.o_id JOIN `t_medicines` ON t_medicines.m_id = t_o_medicines.m_id WHERE t_orders.o_created BETWEEN ? AND ? GROUP BY t_o_medicines.m_id ORDER BY revenue desc limit ?');
        $query->execute([ $dateFrom, $checkend->format('Y-m-d'), $limit ]);
        $popular_medicines_revenue_wise = $query->fetchAll();

        $data = [
            'popular_medicines_quantity_wise' => $popular_medicines_quantity_wise,
            'popular_medicines_revenue_wise' => $popular_medicines_revenue_wise,
        ];

        Response::instance()->sendData( $data, 'success');
    }


}
