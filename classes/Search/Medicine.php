<?php

namespace OA\Search;
use OA\{DB, Cache, Functions, Response};
use Elasticsearch\ClientBuilder;
use ONGR\ElasticsearchDSL\Query\Compound\BoolQuery;
use ONGR\ElasticsearchDSL\Query\Compound\FunctionScoreQuery;
use ONGR\ElasticsearchDSL\Query\FullText\MultiMatchQuery;
use ONGR\ElasticsearchDSL\Query\TermLevel\FuzzyQuery;
use ONGR\ElasticsearchDSL\Query\TermLevel\PrefixQuery;
use ONGR\ElasticsearchDSL\Query\TermLevel\TermQuery;
use ONGR\ElasticsearchDSL\Search;

/**
 * 
 */
class Medicine {
    private static $instance;

    private $client;
    private $index = 'arogga';
    private $params;

    public static function init() {
		if(!self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}

    function __construct(){
        $this->client = ClientBuilder::create()->build();
        if( ! MAIN ){
            $this->index = $this->index . '_staging';
        }
        $this->params = [
            'index' => $this->index, //like database
            //'type' => 'medicine', //like DB table
        ];
    }

    public function createIndex(){
        $this->client->indices()->delete( [ 'index' => $this->index ] );

        $params = $this->params;
        $params['body']['mappings'] = [
            'properties' => [
                'm_name' => [
                    'type' => 'text',
                    'index_prefixes' => [
                        'min_chars' => 2,
                        'max_chars' => 5,
                    ],
                    'copy_to' => 'shamim_search_field',
                ],
                'm_form' => [
                    'type' => 'text',
                    'index' => false,
                    'copy_to' => 'shamim_search_field',
                ],
                'm_strength' => [
                    'type' => 'text',
                    'index' => false,
                    'copy_to' => 'shamim_search_field',
                ],
                'm_unit' => [
                    'type' => 'text',
                    'index' => false,
                    'copy_to' => 'shamim_search_field',
                ],
                'm_generic' => [
                    'type' => 'text',
                    'index' => false,
                    'copy_to' => 'shamim_search_field',
                ],
                'm_company' => [
                    'type' => 'text',
                    'index' => false,
                    'copy_to' => 'shamim_search_field',
                ],
                'shamim_search_field' => [
                    'type' => 'search_as_you_type',
                ],
                //https://www.elastic.co/guide/en/elasticsearch/reference/master/keyword.html#keyword-field-type
                'm_g_id' => [
                    'type' => 'keyword',
                ],
                'm_c_id' => [
                    'type' => 'keyword',
                ],
                'm_status' => [
                    'type' => 'keyword',
                ],
                'm_category' => [
                    'type' => 'keyword',
                ],
                'm_rob' => [
                    'type' => 'boolean',
                ],
                'm_rx_req' => [
                    'type' => 'boolean',
                ],
                'm_price' => [
                    'type' => 'scaled_float',
                    'scaling_factor' => 100,
                ],
                'm_d_price' => [
                    'type' => 'scaled_float',
                    'scaling_factor' => 100,
                ],
                'medicineCountViewed' => [
                    'type' => 'integer',
                ],
                'medicineCountPurchased' => [
                    'type' => 'integer',
                ],
            ],
        ];

        //$this->client->indices()->delete( [ 'index' => 'arogga_test1' ] );
        //$this->client->indices()->delete( [ 'index' => 'arogga_test2' ] );

        // Create the index with mappings now
        $response = $this->client->indices()->create( $params );

        return $response;
    }

    public function bulkIndex(){
        $this->createIndex();

        $params = ['body' => []];

        $db = new DB;
        $db->add( 'SELECT * FROM t_medicines WHERE 1=1' );
        $query = $db->execute();
        $query->setFetchMode( \PDO::FETCH_CLASS, '\OA\Factory\Medicine');
        $i = 1;

        while( $medicine = $query->fetch() ){
            $params['body'][] = [
                'index' => [
                    '_index' => $this->index,
                    '_id'    => $medicine->m_id
                ]
            ];

            $params['body'][] = $medicine->toArray();

            // Every 1000 documents stop and send the bulk request
            if ($i % 1000 == 0) {
                $responses = $this->client->bulk($params);

                // erase the old bulk request
                $params = ['body' => []];

                // unset the bulk response when you are done to save memory
                unset($responses);
            }
            $i++;
        }

        // Send the last batch if it exists
        if (!empty($params['body'])) {
            $responses = $this->client->bulk($params);
        }
        Response::instance()->sendMessage( 'Done.', 'success' );
    }

    /**
     * Undocumented function
     *
     * @author Shamim Hasan <shamim@arogga.com>
     * @since 1.0.0
     *
     * @param [type] $id
     * @param [type] $body
     *
     * @return void
     */
    public function index( $id, $body ){
        $params = $this->params;
        $params['id'] = $id;
        $params['body'] = $body;

        $this->client->index( $params );
        return true;
    }

    public function update( $id, $body ){
        $params = $this->params;
        $params['id'] = $id;
        $params['body']['doc'] = $body;

        $this->client->update( $params );
        return true;
    }

    public function delete( $id ){
        $params = $this->params;
        $params['id'] = $id;

        $this->client->delete( $params );
        return true;
    }

    public function search( $args = [] ) {
        $q = '';
        if( ! empty( $args['search'] ) ){
            $q = mb_strtolower( trim( $args['search'] ) );
        }

        $search = new Search();
        if( ! empty( $args['per_page'] ) ){
            $search->setSize( $args['per_page'] );
        }
        if( ! empty( $args['limit'] ) ){
            $search->setFrom( $args['limit'] );
        }
        
        $boolQuery = new BoolQuery();

        if( ! empty( $args['m_status'] ) ){
            $boolQuery->add( new TermQuery( 'm_status', $args['m_status'] ), BoolQuery::FILTER );
        }
        if( ! empty( $args['m_category'] ) ){
            $boolQuery->add( new TermQuery( 'm_status', $args['m_category'] ), BoolQuery::FILTER );
        }

        if( \strlen( $q ) >= 2 && false === \strpos( $q, ' ' ) ){
            $boolQuery->add( new PrefixQuery( 'm_name', $q ), BoolQuery::SHOULD );
            /*
            $boolQuery->add(
                new FuzzyQuery(
                    'm_name',
                    $q,
                    [
                        'fuzziness'     => 'AUTO',
                        'prefix_length' => 2,
                    ]
                ),
                BoolQuery::SHOULD
            );
            */
        }

        $boolQuery->add(
            new MultiMatchQuery(
                [
                    'shamim_search_field',
                    'shamim_search_field._2gram',
                    'shamim_search_field._3gram',
                    //'shamim_search_field._index_prefix',
                ],
                $q,
                [
                    'type'          => 'bool_prefix',
                    'fuzziness'     => 'AUTO',
                    'prefix_length' => 2,
                ]
            ),
            BoolQuery::SHOULD
        );

        $functionScoreQuery = new FunctionScoreQuery( $boolQuery, [
            'score_mode' => 'sum',
            'boost_mode' => 'sum',
            'max_boost'  => 10,
        ] );
        $functionScoreQuery->addFieldValueFactorFunction( 'medicineCountViewed', 0.0001, 'log1p', null, 0.0001 );
        $functionScoreQuery->addFieldValueFactorFunction( 'medicineCountPurchased', 0.01, 'log1p', null, 0.0001 );

        $search->addQuery( $functionScoreQuery );
        //$queryArray = $search->toArray();

        $client = ClientBuilder::create()->build();

        $params = $this->params;
        $params['body'] = $search->toArray();

        $data = [];
        $docs = $client->search($params);
        //return $docs;
        foreach( $docs['hits']['hits'] as $doc ){
            $medicine = new \OA\Factory\Medicine( $doc['_source'] );

            $data[] = [
                'id' => $medicine->m_id,
                'name' => $medicine->m_name,
                'generic' => $medicine->m_generic,
                'strength' => $medicine->m_strength,
                'form' => $medicine->m_form,
                'company' => $medicine->m_company,
                'unit' => $medicine->m_unit,
                'pic_url' => $medicine->m_pic_url,
                'rx_req' => $medicine->m_rx_req,
                'comment' => $medicine->m_comment,
                'price' => $medicine->m_price,
                'd_price' => $medicine->m_d_price,
            ];
        }

        return $data;
    }

    public function search2( $q, $from = 0, $size = 20, $args = [] ){
        $q = trim( \rawurldecode($q) );

        $params = $this->params;
        $query = [];
        $params['body']['from'] = $from;
        $params['body']['size'] = $size;

        if( !empty($args['orderBy']) && !empty($args['order']) ){
            $params['body']['sort'] = [
                $args['orderBy'] => $args['order'],
            ];
        }
        $query['bool']['filter'][] = [
            'term' => [ 'm_status' => 'active' ],
        ];

        if( \strlen( $q ) >= 2 && false === \strpos( $q, ' ' ) ){
            $query['bool']['should'][]['prefix']['m_name'] = [
                //'shamim_search_field._index_prefix' => $q,
                'value' => $q,
            ];
            $query['bool']['should'][]['fuzzy']['m_name'] = [
                'value' => $q,
                'fuzziness' => 'AUTO',
                'prefix_length' => 2,
            ];
        }

        $query['bool']['should'][]['multi_match'] = [
            'query' => $q,
            'type' => 'bool_prefix',
            'fuzziness' => 'AUTO',
            'prefix_length' => 2,
            'fields' => [
                'shamim_search_field',
                'shamim_search_field._2gram',
                'shamim_search_field._3gram',
                'shamim_search_field._index_prefix',
            ],
        ];
        $params['body']['query']['function_score'] = [
            "query" => $query,
            'functions' => [
                [
                    'field_value_factor' => [
                        'field' => 'm_view_count',
                        'factor' => 0.0001,
                        'missing' => 0.0001,
                        'modifier' => 'log1p',
                    ],
                ],
                [
                    'field_value_factor' => [
                        'field' => 'm_purchased_count',
                        'factor' => 0.01,
                        'missing' => 0.0001,
                        'modifier' => 'log1p',
                    ],
                ],
            ],
            'score_mode' => 'sum',
            'boost_mode' => 'sum',
            'max_boost' => 10,
            
        ];

       $result = array();
       $i = 0;
       $query = $this->client->search( $params );
       $hits = sizeof($query['hits']['hits']);
       $hit = $query['hits']['hits'];
       $result['searchfound'] = $hits;
       $result['q'] = $q;
       while ($i < $hits) {
           $result['result'][$i] = $query['hits']['hits'][$i]['_source'];
           $i++;
       }
       Response::instance()->sendData( $query, 'success' );
       return $result;
    }
}
